#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <utime.h>



int filter(const struct dirent *listfile){
    
    char dot = '.', end = '\0', c = 'c', o = 'o', first, second, third;
    int len = strlen(listfile->d_name), i = 0;
    
    while(i <= len+1){
        
        if(first == '.' && (second == c || second == o) && third == end)
            return 1;
        
        first = second;
        second = third;
        third = listfile->d_name[i];
        i++;
    }
    return 0;
}

int searchobj(struct dirent **listfile, int i, int n){
    
    if((n-(i+1))){
        char *name1 = listfile[i]->d_name;
        char *name2 = listfile[i+1]->d_name;

    int len1 = strlen(name1);
    int len2 = strlen(name2);
    int i = 0;
    
    
        if((len1 != len2))
            return 0;
        else{
            
            while(i < len1-2){
                if(name1[i] != name2[i])
                    return 0;
                i++;
            }
        }
        return 1;
    }
    
    return 0;
}

int isc(char *name){
    return (name[strlen(name)-1] == 'c');
}

long long last_change(char *name){
    char *path = NULL;
    struct stat buf;
    asprintf(&path, "./%s", name);
    
    stat(path, &buf);
    
    return buf.st_mtime;
}

int main(void){
    
    struct dirent **listfile;
    int n = 0, i ;
    long long isolder;
    n = scandir(".", &listfile, filter, alphasort);
    
    for(i=0; i < n; i++){
        
        if(isc(listfile[i]->d_name)){
            printf("%s\n", listfile[i]->d_name);
            if(searchobj(listfile, i, n)){
                isolder = (last_change(listfile[i]->d_name) - last_change(listfile[i+1]->d_name));
                if(isolder >= 0){
                    if(!fork())
                        execlp("gcc", "gcc", "-I.", "-ggdb", "-c", listfile[i]->d_name, (char*)NULL);
                }
            }
            else{
                if(!fork())
                    execlp("gcc", "gcc", "-I.", "-ggdb", "-c", listfile[i]->d_name,(char*)NULL);
            }
            
        }
        
    }
    
    return 0;
}
