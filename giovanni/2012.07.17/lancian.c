#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>


int main(int argc, char *argv[]){
    
    int myargc = argc-2, i;
    int ncopie = atoi(argv[1]);
    char *myargv[myargc];
    char *ncopiaenv;
    
    char *envp[] = {ncopiaenv, NULL};
    
    for(i = 0; i < myargc; i++){
        asprintf(&myargv[i], "%s", argv[i+2]);
    }
    myargv[i] = NULL;
 
    
    for(i=0; i < ncopie; i++){
        char cp = i-'0';
        char *NCOPIA = getenv(&cp);
        asprintf(&ncopiaenv, "NCOPIA=%s", NCOPIA);
        
        if(!fork()){
            if(execvpe(myargv[0], myargv, envp)==-1)
            	perror("Error execvp: ");
            break;
        }
    }
    
    for(i = 0; i < ncopie; i++){
        waitpid(-1, NULL, 0);
    }
    
    return 0;
}
