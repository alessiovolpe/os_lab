#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>
#include <signal.h>
#include <string.h>



int main(int argc, char *argv[]){
	
	sigset_t sig_set;
	sigemptyset(&sig_set);
	sigaddset(&sig_set, SIGUSR1);
	sigprocmask(SIG_BLOCK, &sig_set, NULL);
	
	int signal;
	int fdshm = shm_open("shared_memory",O_RDWR , 0777);
	char buffer[1024];
	pid_t pid;
	char vuoto[1024];
	int i;
	for(i=0; i<1024; i++){
		vuoto[i]='\0';
		}
	
	
	read(fdshm, &buffer, 1024);
	pid = atoi(buffer);
	printf("%d\n", pid);
	
	sprintf(buffer, "%d", getpid());
	lseek(fdshm, 0, SEEK_SET);
	if(write(fdshm, &buffer, strlen(buffer)) < 0)
		perror("write error ");
	kill(pid, SIGUSR1);
	sigwait(&sig_set, &signal);
	/*-----------------------------------------------------------------*/
	
    i = 1;
	while(i < argc){
		lseek(fdshm, 0, SEEK_SET);
		sprintf(buffer, "%s", argv[i]);
		write(fdshm, &vuoto, 1024);
		lseek(fdshm, 0, SEEK_SET);
		if(write(fdshm, &buffer, strlen(buffer)) < 0)
		perror("write error ");
		kill(pid, SIGUSR1);
		sigwait(&sig_set, &signal);
		i++;		
}
	
	kill(pid, SIGKILL);
	close(fdshm);
	shm_unlink("shared_memory");



	return 0;	
}
