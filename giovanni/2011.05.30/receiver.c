#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>
#include <string.h>
#include <signal.h>


int main(void){
	
	sigset_t sig_set;
	sigemptyset(&sig_set);
	sigaddset(&sig_set, SIGUSR1);
	sigprocmask(SIG_BLOCK, &sig_set, NULL);
	
	
	
	shm_unlink("shared_memory");
	int signal;
	int fdshm = shm_open("shared_memory",O_RDWR | O_CREAT, 0777);
	char buffer[1024];
		
	int pid = getpid();
	sprintf(buffer, "%d", pid);

	write(fdshm, &buffer, strlen(buffer));
	
	/*----------------------------------------------------------------*/
	sigwait(&sig_set, &signal);

	
	lseek(fdshm, 0, SEEK_SET);
	read(fdshm, &buffer, 1024);
	pid = atoi(buffer);
	printf("%d\n", pid);
	while(1){
		kill(pid, SIGUSR1);
		sigwait(&sig_set, &signal);
		lseek(fdshm, 0, SEEK_SET);
		read(fdshm, &buffer, 1024);
		printf("%s\n", buffer);
	}

	close(fdshm);





	return 0;	
}
