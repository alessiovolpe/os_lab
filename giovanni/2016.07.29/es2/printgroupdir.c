#include <printgroupdir.h>


char **printgroupdir(const char *dirp, const char *group){
    
    struct dirent **list = NULL;
    char *filepath;
    int i, j, n = scandir(dirp, &list, NULL, alphasort);
    struct stat buff_stat;
    struct group *grp;
    char **vector = malloc(n+1* sizeof(char *));
    
    for(i = 0, j = 0; i < n; i++){
        
        asprintf(&filepath, "%s/%s", dirp, list[i]->d_name);
        stat(filepath, &buff_stat);
        if(((buff_stat.st_mode & S_IFMT) != S_IFDIR) ){
            grp = getgrgid(buff_stat.st_gid);
            if(strcmp(grp->gr_name, group) == 0){
                asprintf((&vector[j]), "%s", filepath);
                printf("%d -> %s\n", j, vector[j]);
                j++;
            }

        }
    }
    
    if (j == 0){
        asprintf(&vector[j], "%c", '\0');
    }else{
        asprintf(&vector[j+1], "%c", '\0');
    }
    return vector;
    
}

void freestringarray(char **v){
    int i = 0;
    while(v[i] != NULL){
        free(v[i]);
        i++;
    }
    free(v);
}
