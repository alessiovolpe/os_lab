#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <grp.h>
#include <uuid/uuid.h>
#include <string.h>
#include <stdlib.h>

char **printgroupdir(const char *dirp, const char *group);

void freestringarray(char **v);
