
#include <printgroupdir.h>


int main(int argc, char *argv[]){
    
    char **list = printgroupdir(argv[1],argv[2]);
    int i = 0;
    while(list[i] != NULL){
        printf("%s\n", list[i]);
        i++;
    }
    
    freestringarray(list);
    
    return 0;
}
