#!/usr/bin/python

import sys, os, grp
from os import listdir
from os.path import join, isdir

listfile = [f for f in listdir(sys.argv[1]) if not isdir(join(sys.argv[1],f))]


for file in listfile:
    stat_info = os.stat(join(sys.argv[1], file))
    gid = stat_info.st_gid
    group = grp.getgrgid(gid)[0]
    if(group == sys.argv[2]):
        print join(sys.argv[1], file)
