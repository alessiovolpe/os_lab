#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(void){

	int fd = open("file" , O_CREAT | O_RDWR, 0777);
	ftruncate(fd, 1024*1024*1024);

	close(fd);	

	return 0;
}
