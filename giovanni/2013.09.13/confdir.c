#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <fcntl.h>
#include <string.h>

int filter(const struct dirent *listfile){
    char *path;
    asprintf(&path, "./%s", listfile->d_name);
    struct stat buf;
    
    stat(path, &buf);
    
    return S_ISREG(buf.st_mode);
}

int comparebyte(char *file1, char *file2){
    
    char *path1, *path2;
    asprintf(&path1, "./%s", file1);
    asprintf(&path2, "./%s", file2);
    
    int fd1 = open(path1, O_RDONLY);
    int fd2 = open(path2, O_RDONLY);
    
    char c1, c2;
    
    while(1){
        if(read(fd1, &c1, 1) == read(fd2, &c2, 1)){
            close(fd1);
            close(fd2);
            return 1;
        }
        else if(c1 != c2){
            close(fd1);
            close(fd2);
            return 0;
        }
    }
}

int comparesize(char *file1, char *file2){
    
    char *path1, *path2;
    asprintf(&path1, "./%s", file1);
    asprintf(&path2, "./%s", file2);
    
    struct stat st1, st2;
    
    stat(path1, &st1);
    stat(path2, &st2);
    if(st1.st_size == st2.st_size){
        if(st1.st_ino != st2.st_ino){
            return 1;
        }
    }
    
    return 0;
}

int main(int argc, char *argv[]){
    
    
    struct dirent **listfile;
    chdir(argv[1]);
    int n = scandir(".", &listfile, filter, alphasort), i ,j;
    char *path1, *path2;
    
    for(i = 0; i<n; i++){
        for(j = 0; j < n; j++){
            if(comparesize(listfile[i]->d_name, listfile[j]->d_name)){
                if(comparebyte(listfile[i]->d_name, listfile[j]->d_name) ){
                    asprintf(&path1, "./%s", listfile[i]->d_name);
                    asprintf(&path2, "./%s", listfile[j]->d_name);
                    unlink(path2);
                    link(path1,path2);
                    strcpy(listfile[j]->d_name, "\0");
                }
            }
        }
    }
    
    
    
    
    return 0;
}
