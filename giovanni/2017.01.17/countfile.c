#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>

struct filelement {
    dev_t dev;
    ino_t ino;
    struct filelement *next;
};


int addfile(struct filelement **head, dev_t dev, ino_t ino){
    if ((*head) == NULL){
        (*head) = malloc(sizeof(struct filelement));
        (*head)->dev = dev;
        (*head)->ino = ino;
        (*head)->next = NULL;
        return 1;
    }else if ((*head)->ino == ino && (*head)->dev == dev){
        return 0;
    }else {
        return addfile(&(*head)->next, dev, ino);
    }
}

int filt(const struct dirent *elem) {
    if (strcmp(elem->d_name,".") == 0 ||
        strcmp(elem->d_name,"..") == 0)
        return 0;
    else {
        return 1;
    }
}

int recscan(char *path, struct filelement **head){
    struct dirent **list;
    int count = 0;
    int i, n = scandir(path, &list, filt, alphasort);
    
    for (i = 0; i < n; i++) {
        struct stat buf;
        char *filepath;
        asprintf(&filepath, "%s/%s", path, list[i]->d_name);
        stat(filepath, &buf);
        switch (buf.st_mode & S_IFMT) {
            case S_IFREG:
                printf("file %s\n", filepath);
                count += addfile(head, buf.st_dev, buf.st_ino);
                break;
            case S_IFDIR:
                count +=recscan(filepath, head) ;
                printf("dir %s\n", filepath);
                break;
        }
        free(filepath);
        free(list[i]);
    }
    free(list);
    return count;
}


int main(int argc, char *argv[]) {
    
    if (argc > 1)
        chdir(argv[1]);
    struct filelement *head = NULL;
    printf("%d\n",recscan(".", &head));
    
}

