#!/usr/bin/python

from os import listdir
from os.path import join, isfile, getmtime
import os, sys, shutil

listA = [f for f in listdir(sys.argv[1]) if isfile(join(sys.argv[1], f)) ]
listB = [f for f in listdir(sys.argv[2]) if isfile(join(sys.argv[2], f)) ]

listC = {}

for file in listA:
    listC[file] = []
    listC[file].append(join(sys.argv[1], file))

for file in listB:
    try:
        if (os.path.getmtime(listC[file][0]) < os.path.getmtime(join(sys.argv[2],file))):
            listC[file][0] = join(sys.argv[2], file)
    except KeyError:
        listC[file] = []
        listC[file] = join(sys.argv[2], file)

if not os.path.exists(sys.argv[3]):
    os.makedirs(sys.argv[3])

for file in listC:
    shutil.move(listC[file][0], join(sys.argv[3], file))

