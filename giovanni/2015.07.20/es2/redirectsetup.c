#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>

void redirectsetup(char *s){
    
    int output = dup(STDOUT_FILENO);
    sigset_t set;
    sigemptyset(&set);
    sigaddset(&set, SIGUSR1);
    
    int i = 0;
    
    int fd = open(s, O_RDWR | O_APPEND);
    
    pid_t pid;
    
    while(1){
        pid = fork();
        if (pid == 0)
            return;
        sigwait(&set, NULL);
        printf("Signal received.\n");
        kill(pid, 9);
        if((i%2) == 0){
            dup2(fd,1);
        }
        else{
            dup2(output, 1);
        }
        i++;
    }
}



int main(int argc, char *argv[]) {
    
    redirectsetup(argv[1]);
    
    while (1) {
        time_t now = time(NULL);
        printf("%s",ctime(&now));
        sleep(1);
    }
}
