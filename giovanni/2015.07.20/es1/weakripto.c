#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>

#define rev(number, reversed) int i = 0;\
for(i; i < 8; i++){\
reversed = reversed << 1;\
reversed = reversed | ((number >> i) & 1);\
}


int main(int argc, char *argv[]){
    
    if (argc != 2){
        printf("Numero di argomenti errato.\n");
        return -1;
    }
    
    unsigned char reverse_buff;
    
    int fd = open(argv[1], O_RDWR), ppointer = 0;
    
    
    unsigned char buff = 130 , reversed = 0;
    
    while(read(fd, &buff, 1)){
        rev(buff,reversed);
        lseek(fd, ppointer, SEEK_SET);
        write(fd, &reversed, 1);
        ppointer++;
    }
    
    
    close(fd);
    
    
    
    
    
    return 0;
}
