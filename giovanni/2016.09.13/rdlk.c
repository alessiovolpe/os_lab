#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>


int main(int argc, char *argv[]){
    
    struct flock lock;
    memset(&lock, 0, sizeof(lock));
    lock.l_type = F_RDLCK;
    srand(time(NULL));
    
    int fd = open(argv[1], O_RDONLY);
    
    fcntl(fd, F_SETLKW, &lock);
    
    int timeSleep = rand()%10;
    printf("Sleeping for %d seconds.\n", timeSleep);
    sleep(timeSleep);
    
    lock.l_type = F_UNLCK;
    fcntl(fd, F_SETLKW, &lock);
    
    
    return 0;
}
