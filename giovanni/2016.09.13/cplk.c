#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <time.h>



int main(int argc, char *argv[]){
    
    struct flock lock;
    char buffer[1024];
    int fd1 = open(argv[1], O_RDONLY);
    int fd2 = open(argv[2], O_RDWR | O_CREAT | O_APPEND, S_IRWXU | S_IRWXG | S_IRWXO);
    
    srand(time(NULL));
    
    memset(&lock, 0, sizeof(lock));
    lock.l_type = F_WRLCK;
    
    fcntl(fd2, F_SETLKW, &lock);
    
    
    int readElement = read(fd1, buffer, 1024);
    while(readElement ){
        write(fd2, buffer, readElement);
        readElement = read(fd1, buffer, 1024);
    }

    int timeSleep = rand()%10;
    printf("Sleep for 15 seconds before unlock.\n");
    sleep(15);
    
    lock.l_type = F_UNLCK;
    fcntl(fd2, F_SETLKW, &lock);
    
    //timeSleep = rand()%10;
    //printf("Sleep for %d seconds after unlock.\n", timeSleep);
    //sleep(timeSleep);
    
    
    close(fd1);
    close(fd2);
    
    
    
    return 0;
}
