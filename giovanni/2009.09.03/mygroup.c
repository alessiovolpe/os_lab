#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <grp.h>

int main(void){

	gid_t list[20];
	
	int n_group = getgroups(20, list);
	struct group *group_info;
	
	if(n_group == -1)
		perror("Error getgroups");
		
	int i;
	for(i=0; i < n_group; i++){
		group_info = getgrgid(list[i]);
		printf("%s ", group_info->gr_name);
	}
	printf("\n");

}

