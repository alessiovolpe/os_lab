#!/usr/bin/python

from sys import argv
from os.path import join, isfile, splitext
from os import listdir
from collections import OrderedDict
from subprocess import call
from time import sleep
from operator import itemgetter

listexe = [f for f in listdir(argv[1]) if isfile(join(argv[1],f))]
checkedlist = {}
ocheckedlist = {}
for file in listexe:
	_, ext = splitext(file)
	ext1 = ext.replace('.', '')
	if ext1.isdigit():
		try:
			checkedlist[ext1].append(file)
		except KeyError:
			checkedlist[ext1] = []
			checkedlist[ext1].append(file)
			
			
listkey = sorted(checkedlist, key=int)
time = 0

for exe in listkey:
	time = float(exe) - time
	sleep(float(time)*0.001)
	for file in checkedlist[exe]:
		exepath = "./"+argv[1]+file
		call(exepath)
