#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

struct file{
    char *name;
    int value;
    struct file *next;
};


int filter(const struct dirent *listfile){
    
    int len = strlen(listfile->d_name), i, j = 0;
    char prev = '\0' , current = '\0';
    char *value = malloc(sizeof(char)*1024);
    
    for(i = 0; i < len; i++){
        current = listfile->d_name[i];
        
        if(prev == '.' && isdigit(current)){
            while(i < len){
                value[j] = listfile->d_name[i];
                i++; j++;
            }
            return 1;
        }
        prev = current;
    }
    
    return 0;
}

int getvalue(const struct dirent *listfile){
    
    int len = strlen(listfile->d_name), i, j = 0;
    char prev = '\0' , current = '\0';
    int value = 0;
    
    for(i = 0; i < len; i++){
        current = listfile->d_name[i];
        
        if(prev == '.' && isdigit(current)){
            while(i < len){
                value *= 10;
                value += listfile->d_name[i] - '0';
                i++; j++;
            }
            return value;
        }
        prev = current;
    }

    return -1;
}

int recinsert(struct file **prev,struct file **head, const char *name, int value){
    struct file *tmp = NULL;
    if((*head) == NULL){
        (*head)=malloc(sizeof(struct file));
        (*head)->value = value;
        asprintf(&((*head)->name), "%s", name);
        (*head)->next = NULL;
        return 0;
    }
    else if(value <= (*head)->value){
        tmp = malloc(sizeof(struct file));
        tmp->next = (*head);
        asprintf(&tmp->name, "%s", name);
        tmp->value = value;
        if(prev != NULL)
            (*prev)->next = tmp;
        else{
            tmp->next = (*head);
            (*head) = tmp;
        }
        return 0;
    }
    else{
        return recinsert(head, &((*head)->next), name, value);
    }
}

int main(int argc, char *argv[]){
    
    
    
    struct dirent **listfile;
    struct file *file_to_print = NULL;
    
    int n, i;
    
    n = scandir(argv[1], &listfile, filter, alphasort);
    
    for(i = 0; i < n; i++){
        recinsert(NULL, &file_to_print, listfile[i]->d_name, getvalue(listfile[i]));
    }
    for(i = 0; i < n; i++){
        printf("%s\n", file_to_print->name);
        file_to_print = file_to_print->next;
    }
    return 0;
}
