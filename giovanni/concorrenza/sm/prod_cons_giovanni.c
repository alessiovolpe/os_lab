#include <semaphore.h>
#include <stdio.h>
#include <pthread.h>

semaphore mutex;
volatile int buffer = 0;
volatile int who_is_in = 0;

void *producer(void *arg){
    while(1){
        int value;
        printf("-");
        value = rand()%1000;
        semaphore_P(mutex);
        if(buffer != 0) buffer = value;
        semaphore_V(mutex);
        printf("Prodotto ---> %d\n", buffer);
        usleep(rand()%1000000);
        }
}

void *consumer(void *arg){
    int last = 0;
    while(1){
        printf(".");
        if(0 != buffer){
            semaphore_P(mutex);
            printf("Consumato ---> %d\n", buffer);
            buffer = 0;
            semaphore_V(mutex);
            usleep(rand() % 1000000);
        }
    }
}

int main(void){
    
    srand(time(NULL));
    pthread_t prod_t;
    pthread_t cons_t;
    mutex = semaphore_create(1);
    pthread_create(&prod_t, NULL, producer, NULL);
    pthread_create(&cons_t, NULL, consumer, NULL);
    
    pthread_join(prod_t,NULL);
    pthread_join(cons_t, NULL);
    
    
    return 0;
}
