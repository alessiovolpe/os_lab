#include <stdio.h>
#include <semaphore.h>
#include <pthread.h>

#define NUMBERS_OF_WRITERS 3
#define NUMBERS_OF_READERS 3


int nr = 0;
int nw = 0;

semaphore mutex;
semaphore semw;
semaphore semr;
int waitingr = 0;
int waitingw = 0;
int wlast = 0;

int buffer;

void *reader(void *arg){
    
    while(1){
        
        semaphore_P(mutex);
        if(nw > 0 || waitingw > 0){
            waitingr++;
            semaphore_V(mutex);
            semaphore_P(semr);
        }
        nr++;
        if(waitingr > 0){
            waitingr--;
            semaphore_V(semr);
        }else{
            wlast = 0;
            semaphore_V(mutex);
        }
        printf("|NR %d|NW %d|WR %d|WW %d\n",nr,nw,waitingr,waitingw);
        semaphore_P(mutex);
        usleep(rand()%2000000);
        nr--;
        if(nr == 0 && waitingw > 0){
            waitingw--; semaphore_V(semw); }
        else { semaphore_V(mutex); }
    }
}

void *writer(void *arg){
    
    while(1){
        
        semaphore_P(mutex);
        if(nr > 0 || nw > 0){
            waitingw++;
            semaphore_V(mutex);
            semaphore_P(semw);
        }
        nw++;
        semaphore_V(mutex);
        wlast = 1;
        printf("|NR %d|NW %d|WR %d|WW %d\n",nr,nw,waitingr,waitingw);
        semaphore_P(mutex);
        usleep(rand()%2000000);
        nw--;
        if(waitingr > 0 && (waitingw == 0 || wlast )){
            waitingr--;
            semaphore_V(semr);
        }else if(waitingw > 0 && (waitingw == 0 || wlast )){
            waitingw--;
            semaphore_V(semw);
        }else{
            semaphore_V(mutex);
        }
        
    }
}

int main(void){
    
    pthread_t n_writer[NUMBERS_OF_WRITERS], n_reader[NUMBERS_OF_READERS];
    srand(time(NULL));
    int i;
    
    semw = semaphore_create(0);
    semr = semaphore_create(0);
    mutex = semaphore_create(1);
    
    for(i=0; i<NUMBERS_OF_WRITERS; i++){
        pthread_create(&n_writer[i], NULL, writer, NULL);
    }
    
    for(i=0; i<NUMBERS_OF_READERS; i++){
        pthread_create(&n_reader[i], NULL, reader, NULL);
    }
    
    for(i=0; i<NUMBERS_OF_READERS; i++){
        pthread_join(n_reader[i], NULL);
    }
    
    for(i=0; i<NUMBERS_OF_WRITERS; i++){
        pthread_join(n_writer[i], NULL);
    }
    
    
    
    
    
    
    
    
    
    
    return 0;
}
