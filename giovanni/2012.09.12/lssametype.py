#!/usr/bin/python

from sys import argv
from os.path import splitext
from os import walk

_, ext = splitext(argv[1])

for root, _, files in walk(argv[2]):
	for file in files:
		_, ext1 = splitext(file)
		if (ext == ext1):
			print argv[1], file

