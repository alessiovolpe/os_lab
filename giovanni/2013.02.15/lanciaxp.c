#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>


int ispal(const char *filename){
    int i = 0, j, middle, len;
    len = strlen(filename);
    j = len-1;
    if((len % 2) == 0)
        middle = (len/2);
    else
        middle = (len/2)+1;
    
    while(i < middle){
        if(filename[i] != filename[j])
            return 0;
        i++; j--;
    }
    
    return 1;
}

int filter(const struct dirent *listfile){
    struct stat st;
    char *path;
    asprintf(&path, "./%s", listfile->d_name);
    stat(path, &st);
    
    if((st.st_mode & S_IXUSR) && !(S_ISDIR(st.st_mode))){
        if(ispal(listfile->d_name))
            return 1;
    }
    return 0;
}

int main(void){
    
    struct dirent **listexe;
    
    int n = scandir(".",&listexe,filter,alphasort), i;
    
    for(i=0; i < n; i++){
        
        if(!fork()){
            execl(listexe[i]->d_name, listexe[i]->d_name, (char *)NULL);
        }
    }
    sleep(1);
    
    return 0;
}
