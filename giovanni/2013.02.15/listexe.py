#!/usr/bin/python

from os import listdir
from os.path import realpath, join

listproc = [f for f in listdir("/proc") if f.isdigit()]

for exe in listproc:
	try:
		rpath = realpath(join("/proc/"+str(exe),"exe"))
		print exe," ",rpath
	except OSError:
		continue
