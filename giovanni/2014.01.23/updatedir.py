#!/usr/bin/python

from sys import argv
from os import listdir
from os.path import isfile, join
from filecmp import cmp
from shutil import copyfile

listdira = [f for f in listdir(argv[1]) if isfile(join(argv[1],f))]

listdirb = [f for f in listdir(argv[2]) if isfile(join(argv[2],f))]
for file in listdira:
	if file in listdirb:
		index = listdirb.index(file)
		if not cmp(join(argv[2],listdirb[index]), join(argv[1],file)):
			copyfile(join(argv[1],file),join(argv[2],file+'1'))
	else:	
		copyfile(join(argv[1],file), join(argv[2],file))

