#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>

int compar(const void * a, const void *b){
	const char *pa = *(const char **) a;
	const char *pb = *(const char **) b;
	
	return strcmp(pa, pb)*(-1);
}

int main(void){
	
	DIR *directory;
	directory = opendir(".");
	struct dirent *entry;
	char *listdir[100];
	int i = 0;
	
	for(i; i < 100; i++){
		listdir[i] = malloc(sizeof(char)*256);
	}
	
	entry = readdir(directory);
	i = 0;
	while(entry){
		if(!((strcmp(entry->d_name, ".") == 0) || (strcmp(entry->d_name, "..") == 0))){
			strcpy(listdir[i], entry->d_name);
			i++;
		}
		entry = readdir(directory);
	}
	
	int element = i;

	qsort(listdir, element, sizeof(char*), compar);
	
	for(i=0; i < element; i++){
		printf("%s\n", listdir[i]);
	}
	
	return 0;
}
