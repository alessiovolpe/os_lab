#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>


int main(void){

	int n, i, file = 0, dir = 0, link = 0, spec = 0;
	struct dirent **listfile;
	n = scandir(".", &listfile, NULL, alphasort);
	char *path;
	struct stat st;
	
	for(i = 2; i < n; i++){
		asprintf(&path, "./%s", listfile[i]->d_name);
		lstat(path, &st);
		if(S_ISLNK(st.st_mode))
			link++;
		else if(S_ISDIR(st.st_mode))
			dir++;
		else if(S_ISREG(st.st_mode))
			file++;
		else
			spec++;
	}
	
	printf("Ci sono: %d file, %d dir, %d link e %d file speciali.\n",file,dir,link,spec);
	

	return 0;
	
}
