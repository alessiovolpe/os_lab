#include <linux/inotify.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>



#define EVENT_SIZE (sizeof( struct inotify_event))
#define EVENT_BUF_LEN (1024 * (EVENT_SIZE + 16))

int main(int argc, char *argv[]){
    
    if(argc == 2){
        chdir(argv[1]);
    }
    else{
        printf("Inserisci il path della cartella da spiare come parametro.\n");
        exit(1);
    }
    
    int fd = inotify_init();
    
    if(fd == -1){
        perror("Inotify_init Error: ");
    }
    
    int watched = inotify_add_watch(fd, ".", IN_CREATE);
    
    if(watched == -1){
        perror("Inotify_add_watch Error: ");
    }
    
    int i = 0, lenght;
    char buffer[EVENT_BUF_LEN];
    
    while(1){
        
        lenght = read(fd, buffer, EVENT_BUF_LEN);
        
        if(lenght < 0){
            perror("Read Error: ");
        }
        i = 0;
        while(i<lenght){
            struct inotify_event *event = (struct inotify_event *)&buffer[i];
            if(event->len){
                if(event->mask & IN_CREATE){
                    if(event->mask & IN_ISDIR)
                        printf("New directory created: %s\n", event->name);
                    else
                        printf("New file created: %s\n", event->name);
                }
                
                i += EVENT_SIZE + event->len;
            }
        }
    }
    
    inotify_rm_watch(fd,watched);
    close(fd);
    
    return 0;
}
