#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

struct file{
    int index;
    char *name;
    struct file *next;
};

int taken(const char *name){
    char p;
    int i = 0, n = strlen(name);
    int number = 0;
    for(; i < n; i++){
        if(isdigit((char)name[i])){
            for(; i < n; i++){
                number += (name[i]-'0');
                number *= 10;
            }
            return number/10;
        }
    }
    return 0;
}

int recinsert(struct file **prev,struct file **head, const char *name, int index){
    struct file *tmp = NULL;
    if((*head) == NULL){
        (*head)=malloc(sizeof(struct file));
        (*head)->index = index;
        asprintf(&((*head)->name), "%s", name);
        (*head)->next = NULL;
        return 0;
    }
    else if(index <= (*head)->index){
        tmp = malloc(sizeof(struct file));
        tmp->next = (*head);
        asprintf(&tmp->name, "%s", name);
        tmp->index = index;
        if(prev != NULL)
            (*prev)->next = tmp;
        else{
            tmp->next = (*head);
            (*head) = tmp;
        }
        
        return 0;
    }
    else{
        return recinsert(head, &((*head)->next), name, index);
    }
}


void print(struct file * head){
    if(head == NULL){
    }
    else {
        printf("%s \n", head->name);
        print(head->next);
        free(head);
    }
}
int main(int argc, char *argv[]){
    
    struct file *listfile = NULL;
    
    if(argc == 2){
        chdir(argv[1]);
    }
    else{
        printf("Numero di argomenti errato.\n");
    }

    struct dirent **namelist;
    int n = scandir(".", &namelist, NULL, alphasort);
    int i = 0;
    
    for(; i < n; i++){
        
        if(taken(namelist[i]->d_name)){
            recinsert(NULL, &listfile, namelist[i]->d_name, taken(namelist[i]->d_name));
            
        }
    }
    print(listfile);
    
}
