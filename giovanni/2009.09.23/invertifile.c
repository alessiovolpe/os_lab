#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


int main(int argc, char *argv[]){

	int fdin = open(argv[1], O_RDONLY);
	int fdout = open(argv[2], O_CREAT | O_RDWR, 0666);
	struct stat st;
	
	if(fstat(fdin, &st))
		perror("Error fstat");
	
	off_t size = st.st_size;
	off_t offset = 0;
	char buffer[4096];
	char tmp;
	int i, j = 0, n = size/4096;
	

	
	while(j < n){
		if(pread(fdin, buffer, 4096, offset) != 4096)
			printf("Errore pread, non ha letto 4096 B.\n");
		
		for(i = 0; i < 2048; i++){
			tmp = buffer[4095-i];
			buffer[4095-i] = buffer[i];
			buffer[i] = tmp;
		}
		
		if(pwrite(fdout, buffer, 4096, offset) != 4096)
			perror("Error pwrite");
		
		offset += 4096;
		size -= 4096;
		j++;
	}
	
	pread(fdin, buffer, size, offset);
	
	for(i = 0; i < size/2; i++){
		tmp = buffer[(size-1)-i];
		buffer[(size-1)-i] = buffer[i];
		buffer[i] = tmp;
	}
	
	pwrite(fdout, buffer, size, offset);
	
	close(fdin);
	close(fdout);
	
	return 0;
}
