#include <stdio.h>
#include <unistd.h>
#include <string.h>

void readl(FILE **fd, char *arg[]){
    
    char *line = NULL, *token = NULL;
    size_t len = 0;
    int i = 0;
    //FILE *fd = fopen(file, "r");
    if(getline(&line, &len, *fd) < 0)
        perror("error:");
    if(strcspn(line, "\r\n"))
        line[strcspn(line, "\r\n")] = '\0';
    
    token = strtok(line, " ");
    asprintf(&arg[0], "%s",token);
    token = strtok(NULL, " ");
    
    
    while(token != NULL){
        i++;
        asprintf(&(arg[i]) ,"%s",token);
        token = strtok(NULL, " ");
    }
    arg[i+1] = NULL;
}



int main(int argc, char *argv[]){
    
    int pdes[2];
    char *arg[10], *arg1[10];
    FILE *fd =fopen(argv[1], "r");
    
    readl(&fd, arg);
    readl(&fd, arg1);
    fclose(fd);
    
    //printf("%s\n",arg1[0]);
    
    pipe(pdes);
    //printf("%s %s\n", exe,arg[0]);
    if(fork()){
        dup2(pdes[1], 1);
        close(pdes[0]);
        execvp(arg[0], arg);
    }
    else{
        dup2(pdes[0], 0);
         close(pdes[1]);
         execvp(arg1[0], arg1);
    }
    
    return 0;
}
