#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <signal.h>


#define read_nth(number, n) ((number >> n) & 1)

int main(int argc, char *argv[]){
    
    if (argc =! 3){
        printf("Number of arguments wrong.\n");
        return -1;
    }
    
    printf("%d\n", getpid());
    
    sigset_t set;

    sigemptyset(&set);
    sigaddset(&set, SIGUSR1);
    sigprocmask(SIG_BLOCK, &set, NULL);


    
    pid_t rx_pid = atoi(argv[1]);
    int i = 0, j;
    char buffer;
    while (i < strlen(argv[2])){
        
        for(j = 7; j >= 0; j--){
            buffer = argv[2][i];
            if(read_nth((int)argv[2][i], j))
                kill(rx_pid, SIGUSR1);
                else
                kill(rx_pid, SIGUSR2);
            sigwaitinfo(&set, NULL);
        }
        i++;
    }
    
    
    
    
    
    
    return 0;
}
