#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>


void set_nth(char *number, int n){
    *number = *number << 1;
    *number = *number | n;
}

int main(void){
    
    pid_t mypid = getpid();
    printf("This is my pid: %d\n", mypid);
    
    int i, rv_signal;
    siginfo_t info;
    sigset_t set;
    
    sigemptyset(&set);
    sigaddset(&set, SIGUSR1);
    sigaddset(&set, SIGUSR2);
    sigprocmask(SIG_BLOCK, &set, NULL);
    
    char buffer;
    while(1){
        
        for(i = 0; i < 8; i++){
            
            rv_signal = sigwaitinfo(&set, &info);

            
            if(rv_signal == 10){
                set_nth(&buffer, 1);
            }
            else{
                set_nth(&buffer, 0);
            }
            kill(info.si_pid, SIGUSR1);
        }
        //printf("%c\n",buffer);
        write(fileno(stdout), &buffer, 1);
        
    }
    
    
    
    return 0;
}
