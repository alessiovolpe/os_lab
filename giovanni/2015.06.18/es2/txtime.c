#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <signal.h>
#include <time.h>

#define read_nth(number, n) ((number >> n) & 1)

int main(int argc, char *argv[]){
    
    if (argc =! 3){
        printf("Number of arguments wrong.\n");
        return -1;
    }
    sigset_t set;

    sigemptyset(&set);
    sigaddset(&set, SIGUSR1);
    sigprocmask(SIG_BLOCK, &set, NULL);

    struct timespec time_s;
    unsigned long time = 0, start, end;

    
    pid_t rx_pid = atoi(argv[1]);
    int i = 0, j, n = atoi(argv[2]);
    char k = 'K';
    while (i < n){
        
        clock_gettime(CLOCK_REALTIME, &time_s);
        start = time_s.tv_nsec;
        
        for(j = 7; j >= 0; j--){
            if(read_nth((int)argv[2][i], j))
                kill(rx_pid, SIGUSR1);
                else
                kill(rx_pid, SIGUSR2);
            sigwaitinfo(&set, NULL);
        }
        clock_gettime(CLOCK_REALTIME, &time_s);
        end = time_s.tv_nsec;
        
        time += (end-start);
        i++;
    }
    
    printf("Duration of execution:%lu ns\n", time/n);
    
    
    
    
    
    return 0;
}
