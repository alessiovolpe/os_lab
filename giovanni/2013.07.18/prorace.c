#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>


#define MAXLENGHT 15
#define MAXPARAMETERS 1024
#define MAXCOMMAND 10
struct comand{
    char *argv[1024];
    struct comand *next;
};

int getcomand(struct comand **head){
    int i = 0, j = 0, z = 0, n = 0;
    char prev, curr;
    (*head) = malloc(sizeof(struct comand));
    (*head)->argv[0] = malloc(sizeof(char *)*100);
    struct comand *comand_list = (*head);
    do{
        curr = getchar();
        if(prev == '\n' && curr == '\n'){
            return n;
        }
        else if(curr == ' '){
            j++;
            comand_list->argv[j] = malloc(sizeof(char *)*100);
            z = 0;
        }else if(curr == '\n'){
            comand_list->next = malloc(sizeof(struct comand));
            comand_list = comand_list->next;
            comand_list->argv[0] = malloc(sizeof(char *)*100);
            j = 0;
            z = 0;
            n += 1;
        }else{
            comand_list->argv[j][z] = curr;
            z++;
        }
        prev = curr;
    }while(1);
    
}


int main(void){
    
    struct comand *head = NULL;
    
    int n = getcomand(&head);
    
    char *processname[100000];
    
    fd_set rfds;
    int retval;
    char *start;
    /*watch stdin (fd 0) to see when it has input*/
    FD_ZERO(&rfds);
    FD_SET(0, &rfds);
    
    int i = 0;
    pid_t pid;

    
    while(i < n){
        
        pid = fork();
        if(!pid){
            /*-----wait for start from parent------*/
            select(1, &rfds, NULL, NULL, NULL);
            execvp(head->argv[0], head->argv);
        }
        asprintf(&(processname[pid]),"%s", head->argv[0]);
        head = head->next;
        i++;
    }
    i = 0;
    
    printf("Inserisci ""start"" per far partire i processi.\n");
    
    scanf("%*s");
    write(0, "\0", 1);
    
    while(i < n){
        pid = waitpid(-1, NULL, 0);
        printf("finito %s\n",processname[pid]);
        i++;
    }
    
    
    
    
    return 0;
}
