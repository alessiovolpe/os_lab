#include <stdio.h>
#include <unistd.h>
#include <sys/eventfd.h>


int main(void){
    
    int efd;
    uint64_t u;
    ssize_t s;
    
    efd = eventfd(0,0);
    while(1){
        switch (fork()) {
                
            case 0:
            {
                
                int i = 0;
                read(efd, &u, sizeof(uint64_t));
                for(i ; i < u; i++)
                    printf("x\n");
                break;
            }
            default:
            {
                int x;
                scanf("%d", &x);
                write(efd, &x, sizeof(uint64_t));
            }
                
        }
    }
    return 0;
}
