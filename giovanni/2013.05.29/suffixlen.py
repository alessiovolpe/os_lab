#!/usr/bin/python

from os import listdir
from os.path import isfile, splitext, getsize, join
from sys import argv

listfile = [f for f in listdir(argv[1]) if isfile(f)]

dictionary = {}

for file in listfile:
    filename,filextension = splitext(file)
    try:
        x = dictionary[filextension] + getsize(join(argv[1], file))
        dictionary[filextension] = x
    except KeyError:
        dictionary[filextension] = []
        dictionary[filextension] = getsize(join(argv[1],file))


for index in dictionary:
    print index,":",dictionary[index]
