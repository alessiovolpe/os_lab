#include <stdio.h>     /* for printf */
#include <stdlib.h>    /* for exit */
#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>



int main(int argc, char *argv[]){
    
    char **myargv;
    opterr = 0;
    static struct option long_option[] = {
        {"in", required_argument, 0, 'i'},
        {"out", required_argument, 0, 'o'},
        {"help", no_argument, 0, 'h'}
    };
    int next_option;
    int fdout, fdin, printhelp;
    
    do{
        
        next_option = getopt_long(argc, argv, "ho:i:", long_option, NULL);
        
        
        switch(next_option){
            case 'h':
                /*User has request the print of help information */
                printf("  -h  --help             Display this usage information.\n"
                       "  -o  --output filename  Write output to filename.\n"
                       "  -i  --in filename      Read input from filename.\n");
                next_option = -1;
                printhelp = 1;
                break;
                
            case 'o':
                /*User has request to redirect output to the argument binded from -o opt */
                
                fdout = open(optarg, O_CREAT | O_RDWR, 0666);
                dup2(fdout, 1);
                break;
                
            case 'i':
                /*User has request to redirect the input from the file binded from -i opt */
                fdin = open(optarg, O_RDONLY);
                dup2(fdin, 0);
                break;
                
            case '?':
                
                if(!printhelp){
                    myargv = &argv[optind-2];
                    
                    execvp(myargv[0], myargv);
                    
                }
                
                break;
                
        }
        
    }while(next_option != -1);
    
    return 0;
}
