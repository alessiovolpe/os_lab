#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>




int main(int argc, char *argv[]){
	
	sigset_t sig_set;
	sigemptyset(&sig_set);
	sigaddset(&sig_set, SIGHUP);
	sigprocmask(SIG_BLOCK, &sig_set, NULL);
	
	struct timespec timeout;
	timeout.tv_sec = 0;
	timeout.tv_nsec = 1;
	int i = 0;
	char buffer;
	char *filename;
	int signal = -1;
	
	asprintf(&filename, "%s%d", argv[1], i);
	int fd = open(filename, O_CREAT | O_RDWR, 0666);
	
	while(1){
	
		buffer = getchar();
		write(fd, &buffer, 1);
		signal = sigtimedwait(&sig_set, NULL, &timeout);
		if(signal == SIGHUP){
			close(fd);
			i++;
			asprintf(&filename, "%s%d", argv[1], i);
			fd = open(filename, O_CREAT | O_RDWR, 0666);
		}
		
	
	}


	return 0;
}
