#!/usr/bin/python

from sys import argv
from os import listdir, chdir, getcwd, link, makedirs
from os.path import isfile, getmtime, exists, join


cwd = getcwd()
chdir(argv[1])
listdirA = [f for f in listdir('.') if isfile(f)]
chdir(cwd)
chdir(argv[2])
listdirB = [f for f in listdir('.') if isfile(f)]

chdir(cwd)
if not exists(argv[3]):
	makedirs(argv[3])

print listdirA, listdirB

for file in listdirA:
	try:
		index = listdirB.index(file)
		mtimeA = getmtime(join(argv[1], file))
		mtimeB = getmtime(join(argv[2], listdirB[index]))
		if(mtimeA > mtimeB):
			link(join(argv[1],file), join(argv[3],file))
		else:
			link(join(argv[2],listdirB[index]), join(argv[3], listdirB[index]))
		del listdirB[index]
	except ValueError:
			link(join(argv[1],file), join(argv[3],file))

for file in listdirB:
	link(join(argv[2], file), join(argv[3], file))
