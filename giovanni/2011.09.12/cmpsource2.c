#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>

int filter(const struct dirent *listfile){
	
	struct stat st;
	char *path;
	asprintf(&path, "./%s", listfile->d_name);
	stat(path, &st);

	if(S_ISREG(st.st_mode)){
	
		char prev, curr, next, end = '\0';
		int i = 0, len = strlen(listfile->d_name);

		while(i <= len+1){
			if(prev == '.' && (curr == 'h' || curr == 'c') && next == end)
				return 1;
			prev = curr;
			curr = next;
			next = listfile->d_name[i];
			i++;
			}	
		}
		return 0;
}

int ispresent(char *name, struct dirent **listsource, int len){
	int i;
	for(i=0; i<len; i++){

		if(strcmp(name, listsource[i]->d_name) == 0){
				return i;
		}
	}
		
	return -1;
}

int isdiff(char *path1, char *path2){
	
	int fd1, fd2;
	fd1 = open(path1, O_RDONLY);s
	fd2 = open(path2, O_RDONLY);
	char c1, c2;
	int read1 = 1, read2 = 1;
	while(1){
		read1 = read(fd1, &c1, 1);
		read2 = read(fd2, &c2, 1);
		if((c1-c2))
		close(fd1);
		close(fd2);
			return 1;
		if((read1 == 0) && (read2 == 0))
			close(fd1);
			close(fd2);
			return 0;
	}
		
	return -1;

}


int main(int argc, char *argv[]){

if(argc =! 3){
	printf("Numero di argomenti errato.\n");
	return 0;
}

	char *pwd = malloc(1024);
	getcwd(pwd, 1024);

	int n1, n2, i;
	struct dirent ** listsourcedir1, **listsourcedir2;
	chdir(argv[1]);
	n1 = scandir(".", &listsourcedir1, filter, alphasort);
	
	chdir(pwd);
	chdir(argv[2]);
	n2 = scandir(".", &listsourcedir2, filter, alphasort);
	
	chdir(pwd);
		
			
	for(i=0; i < n1; i++){
		int ispres = ispresent(listsourcedir1[i]->d_name, listsourcedir2, n2);
		
		if(ispres != (-1) ){

			char *path1, *path2;
			asprintf(&path1, "./%s/%s", argv[1], listsourcedir1[i]->d_name);
			asprintf(&path2, "./%s/%s", argv[2], listsourcedir2[ispres]->d_name);
			if(isdiff(path1, path2)){
				printf("%s %s differ\n", path1, path2);
			}
		}
		else
			printf("%s/%s not in %s\n", argv[1],listsourcedir1[i]->d_name, argv[2]);
		
	}
	

	
	for(i=0; i < n2; i++){
		int ispres = ispresent(listsourcedir2[i]->d_name, listsourcedir1, n1);
			
		if(ispres != -1){
			//printf("%s  %s \n", argv[1],listsourcedir1[i]->d_name);
			char *path1, *path2;
			asprintf(&path1,"./%s/%s", argv[2], listsourcedir2[i]->d_name);
			asprintf(&path2,"./%s/%s", argv[1], listsourcedir1[ispres]->d_name);
			if(isdiff(path1, path2)){
				printf("%s %s differ\n", path1, path2);
			}
		}
		else
			printf("%s/%s not in %s\n",argv[2], listsourcedir2[i]->d_name, argv[1]);
	}	
	
	
	
	return 0;
}
