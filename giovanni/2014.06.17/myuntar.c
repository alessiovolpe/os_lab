#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

off_t fsize(const char *filename) {
    struct stat st;
    
    if (stat(filename, &st) == 0)
        return st.st_size;
    
    return -1;
}



int main(int argc, char *argv[]){
    
    if (argc != 2){
        printf("Numero di argomenti errato.\n");
        return 0;
    }
    
    char *word, c;
    word = malloc(1024);
    FILE *mytar, *myuntar;
    mytar = fopen(argv[1], "r");
    off_t dimension;
    
    while ((c = fgetc(mytar)) != EOF){
    int i = 0;
        word[i] = c;
        i++;
    while ( (c = fgetc(mytar)) != '\0'){
        word[i]=c;
        i++;
    }
    printf("%s\n", word);
    myuntar = fopen(word, "w+");
    *word = '\0';
    i = 0;
    while ( (c = getc(mytar)) != '\0'){
        word[i]=c;
        i++;
    }
    dimension = atoi(word);
    printf("%llu\n", dimension);
    *word = '\0';
    i = 0;
    while (i < dimension){
        c = fgetc(mytar);
        putc(c, myuntar);
        i++;
    }
    fclose(myuntar);
    }
    
    
    
    
    return 0;
}
