import os, sys
char = sys.argv[1]
rootDir = sys.argv[2]
max = 0


fileName = ""
for root, subFolders, files in os.walk(rootDir):
        for file in files:
            with open(os.path.join(root, file), 'r') as tmpFile:
                contentFile = tmpFile.read()
            tmpValue = contentFile.count(char)
	    if tmpValue > max:
                percentuale = (100*tmpValue)/os.path.getsize(os.path.join(root, file))
                fileName = file
                max = tmpValue

print fileName, str(percentuale)+'%'
