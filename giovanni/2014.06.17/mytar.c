#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

int getListFile(char *path, char *listFile[]){
    
    DIR *dir;
    struct dirent *ent;
    int i = 0;
    if ((dir = opendir (path)) != NULL) {
        /* print all the files and directories within directory */
        while ((ent = readdir (dir)) != NULL) {
            //printf ("%s\n", ent->d_name);
            listFile[i] = ent->d_name;
            i++;
        }
        listFile[i] = NULL;
        closedir (dir);
    } else {
        /* could not open directory */
        perror ("");
        return EXIT_FAILURE;
    }

    return i;
    
}
// if the file is regular this function return the size of file
off_t is_regular_file(const char *path)
{
    struct stat path_stat;
    stat(path, &path_stat);
    stat(path, &path_stat);
    if (S_ISREG(path_stat.st_mode)){
        return path_stat.st_size;
    }
    return 0;
}

int main(int argc, char *argv[]){

    if (argc != 3){
        printf("Numero di argomenti errato.\n");
        return 0;
    }
    int numberFile;
    char *listFile[1024];
    int i = 0;
    
    numberFile = getListFile(argv[1], listFile);
    while (listFile[i] != NULL){
    printf("%s -> %llu\n",listFile[i],  is_regular_file(listFile[i]));
        i++;
    }
    
    FILE *mytar = fopen(argv[2], "w");
    FILE *fileToRead;
    off_t dimension;
    char *openFile;
    i = 0;
    while (listFile[i] != NULL){
        dimension = is_regular_file(listFile[i]);
        if (dimension){
            if ((strcmp(argv[0], listFile[i]) && strcmp(".DS_Store", listFile[i])) != 0){
            char dimensionC[1024];
            sprintf(dimensionC ,"%llu", dimension);
            fputs(listFile[i], mytar);
            fputc('\0', mytar);
            fputs(dimensionC, mytar);
            fputc('\0', mytar);
            fileToRead = fopen(listFile[i], "r");
            openFile = malloc(dimension);
            fread(openFile, dimension, 1, fileToRead);
            fwrite(openFile, dimension, 1, mytar);
            fclose(fileToRead);
            }
        }
        i++;
        
    }
    fclose(mytar);
   

return 0;

}
