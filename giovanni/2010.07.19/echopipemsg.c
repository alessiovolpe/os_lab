#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include <string.h>

struct msgbuf{
	long mtype;
	char mtext[40];
};


int main(void){
	
	struct timespec start, end;
	int i = 0;

	int msgq = msgget(IPC_PRIVATE, IPC_CREAT | S_IRUSR | S_IWUSR);
	
	
	
	clock_gettime(CLOCK_MONOTONIC, &start);
	
	if(!fork()){
		struct msgbuf msgpson;
		
		while(1){
			msgrcv(msgq, &msgpson, 40, 0, MSG_NOERROR);
		 	msgsnd(msgq, &msgpson, 40, MSG_NOERROR);
		}
	}
	else{
		struct msgbuf msgparent;
		msgparent.mtype = 1;
		strcpy(msgparent.mtext, "Questo è un messaggio da 40 byte, ciao!");
		
		while(i < 100000){
			msgsnd(msgq, &msgparent, 40, MSG_NOERROR);
			msgrcv(msgq, &msgparent, 40, 0, MSG_NOERROR);
			i++;
		}
	}
	
	
	clock_gettime(CLOCK_MONOTONIC, &end);
	printf("Time of execution: %.5fs\n", ((double)end.tv_sec+1.0e-9*end.tv_nsec - ((double)start.tv_sec+1.0e-9*start.tv_nsec)));
	
	return 0;
}
