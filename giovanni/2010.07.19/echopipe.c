#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>

const char *message = "Questo è un messaggio da 40 byte, ciao!";



int main(void){
	
	struct timespec start, end;
	int fdpipes1[2], i = 0;
	int fdpipes2[2], fdout;
	char *buffer = malloc(sizeof(char)*40);
	
	if(pipe(fdpipes1) == -1)
		perror("Error pipe: ");
		
	if(pipe(fdpipes2) == -1)
		perror("Error pipe: ");
	
	clock_gettime(CLOCK_MONOTONIC, &start);
	
	if(!fork()){
		dup2(fdpipes1[0], 0);
		close(fdpipes1[1]);
		dup2(fdpipes2[1], 1);
		close(fdpipes2[0]);
		
		while(1){
			read(0, &buffer, 40);
			write(1, &buffer, 40);
		}
	}
	else{
		fdout = dup(1);
		dup2(fdpipes1[1], 1);
		close(fdpipes1[0]);
		dup2(fdpipes2[0], 0);
		close(fdpipes2[1]);
		
		while(i < 100000){
			write(1, &message, 40);
			read(0, &message, 40);
			i++;
		}
	}
	
	dup2(fdout, 1);
	clock_gettime(CLOCK_MONOTONIC, &end);
	printf("Time of execution: %.5fs\n", ((double)end.tv_sec+1.0e-9*end.tv_nsec - ((double)start.tv_sec+1.0e-9*start.tv_nsec)));
	
	return 0;
}
