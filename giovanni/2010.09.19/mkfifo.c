#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <poll.h>




int main(int argc, char *argv[]){
    
    if(argc != 2){
        printf("Inserisci il numero corretto di argomenti.\n");
    }
    
    if(mkfifo(argv[1], 0666)){
        perror("Errore mkfifo: ");
    }
    
    
    
    
    int fd = open(argv[1], O_RDWR);
    char c;
    
    
    while(1){
        read(0, &c, 1);
        write(fd, &c, 1);        
    }


    return 0;
}
