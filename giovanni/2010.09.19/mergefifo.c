#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <poll.h>
#include <string.h>

int main(int argc, char*argv[]){
    
    int fd1, fd2;
    
    fd1 = open(argv[1], O_RDONLY);
    fd2 = open(argv[2], O_RDONLY);
    
    struct pollfd fds[3];
    
    fds[0].fd = fd1;
    fds[0].events = 0 | POLLIN;
    
    fds[1].fd = fd2;
    fds[1].events = 0 | POLLIN;
    
    fds[2].fd = 0;
    fds[2].events = 0 | POLLIN;
    
    int i = 0;
    char buffer[1024];
    
    while(1){
        
        if(!poll(&fds[0], 3, -1))
            perror("Error poll: ");
            
        for(i = 0; i < 3; i++){
            if(fds[i].revents == POLLIN){
                read(fds[i].fd, buffer, 1);
                write(1, buffer, 1);  
            }
        }
        
    
    }

    return 0;
}
