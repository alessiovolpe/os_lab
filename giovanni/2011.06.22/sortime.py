#!/usr/bin/python

from collections import OrderedDict
from os import walk
from sys import argv
from os.path import join, getctime

listfile = []
udictionary = {}

for(root, _, files) in walk(argv[1]):
	for file in files:
		listfile.append(join(root,file))

for file in range(len(listfile)):
	try:
		udictionary[getctime(listfile[file])].append(listfile[file])
	except:
		udictionary[getctime(listfile[file])] = []
		udictionary[getctime(listfile[file])].append(listfile[file])
	
odictionary = OrderedDict(sorted(udictionary.items()))

for key in odictionary:
	for file in odictionary[key]:
		print file
