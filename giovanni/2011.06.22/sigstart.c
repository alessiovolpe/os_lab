#include <signal.h>
#include <stdio.h>
#include <unistd.h>




int main(int argc, char *argv[]){

	int myargc = argc-2;
	char *myargv[myargc+1];
	int i;

	for(i=0; i < myargc; i++){
		asprintf(&(myargv[i]), "%s", argv[i+2]);
	}
	myargv[i] = NULL;
	
	sigset_t signal_set;
	sigemptyset(&signal_set);
	sigaddset(&signal_set, atoi(argv[1]));
	sigprocmask(SIG_BLOCK, &signal_set, NULL);
	
	int signal;
	while(1){
		sigwait(&signal_set, &signal);
		if(signal == SIGUSR1){
			if(!fork()){
				execvp(myargv[0], myargv);
			}
		}
	}
	
	return 0;
}
