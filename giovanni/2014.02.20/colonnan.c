#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>



int is_reg(const char *path){
    struct stat buffer;
    lstat(path, &buffer);
    return S_ISREG(buffer.st_mode);
}

int countChar(const char *buffer, int *numberOfChar, int inputNumber, int count){
    int i = strlen(buffer), j = 0;
    while ( j <= i ){
        if ( buffer[j] == '\0' || buffer[j] == '\n')
            count = -1;
        else if ( count == inputNumber ){
            (*numberOfChar)++;
            printf("%c", buffer[j]);
        }
        j++;
        count++;
    }
    
    return 0;
}

int main(int argc, char *argv[]){
    
    if (argc != 3){
        printf("Numero di argomenti errato.\n");
        return -1;
    }
    
    char *buffer;
    int numberOfChar = 0, count = 0;
    buffer = malloc(1024*sizeof(char));
    
    if ( is_reg(argv[1])){
        int inputFile = open(argv[1], O_RDONLY);
        
        while ( read(inputFile, buffer, 1024) ){
            count = countChar(buffer, &numberOfChar, atoi(argv[2])-1, count);
        }
        printf("\n%d\n", numberOfChar);
        close(inputFile);

    }
    
    
    
    
    
    
    
    
    return 0;
}
