#!/usr/bin/python

from os import listdir
from os.path import isfile
import collections

listfile = [f for f in listdir(".") if isfile(f)]

dictionary = {}

for file in listfile:
    try:
        dictionary[len(file)].append(file)
    except KeyError:
        dictionary[len(file)] = []
        dictionary[len(file)].append(file)

dictionary = collections.OrderedDict(sorted(dictionary.items()))

for i in dictionary:
    print i,": ",dictionary[i]
