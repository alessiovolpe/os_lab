#include <stdio.h>
#include <unistd.h>
#include <sys/signalfd.h>
#include <signal.h>
#include <stdlib.h>

#define handle_error(msg) \
do { perror(msg); exit(EXIT_FAILURE); } while (0)


void mycat(){
    char buffer[1024];
    while(1){
        scanf("%s",buffer);
        printf("%s\n", buffer);
    }
}

void catchsign(){
    sigset_t mask;
    int sigfd;
    struct signalfd_siginfo fdsiginfo;
    ssize_t s;

    sigemptyset(&mask);
    sigaddset(&mask, SIGUSR1);
    
    sigfd = signalfd(-1, &mask, 0);

    if (sigprocmask(SIG_BLOCK, &mask, NULL) == -1)
        handle_error("sigprocmask");
    
    if (sigfd == -1)
        handle_error("signalfd");
    
    while(1){
        s = read(sigfd, &fdsiginfo, sizeof(struct signalfd_siginfo));
        if (s != sizeof(struct signalfd_siginfo))
            handle_error("read");
        
        if(fdsiginfo.ssi_signo == SIGUSR1){
            printf("Ho ricevuto un segnale.\n");
        }
    }
}


int main(void){
    
    switch (fork()) {
        case 0:
            mycat();
            break;
        default:
            catchsign();
            break;
        case -1:
            perror("Error:");
    }
    
    
    
    return 0;
}
