#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/signalfd.h>

#define handle_error(msg) \
do { perror(msg); exit(EXIT_FAILURE); } while (0)


int main(int argc, char *argv[]){
    /*---inizializzo campi e strutture----*/
    char *path, buffer[1024];
    asprintf(&path, "%s%s", "/tmp/giro", argv[1]);
    pid_t pid_receive= atoi(argv[1]);
    
    sigset_t mask;
    int sigfd;
    struct signalfd_siginfo fdsiginfo;
    ssize_t s;
    /*-----setto i segnali che devo gestire personalmente----*/
    sigemptyset(&mask);
    sigaddset(&mask, SIGUSR1);
    
    /*------creo il file descriptor dove leggere i segnali-----*/
    sigfd = signalfd(-1, &mask, 0);
    
    while(1){
        
        if (sigprocmask(SIG_BLOCK, &mask, NULL) == -1)
            handle_error("sigprocmask");
        
        if (sigfd == -1)
            handle_error("signalfd");
        
        int fd = open(path, O_WRONLY | O_CREAT, S_IRWXU | S_IRWXG);
        if (read(STDIN_FILENO, buffer, 1024) == 0){
            kill(pid_receive, SIGUSR2);
            exit(1);
        }
        write(fd, buffer, strlen(buffer));
        close(fd);
        
        kill(pid_receive, SIGUSR1);
        
        s = read(sigfd, &fdsiginfo, sizeof(struct signalfd_siginfo));
        if (s != sizeof(struct signalfd_siginfo))
            handle_error("read");
        
        if(fdsiginfo.ssi_signo == SIGUSR1){
            printf("Ho ricevuto un segnale.\n");
        }
    }
    
        return 0;
    }
