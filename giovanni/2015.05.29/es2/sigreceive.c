#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/signalfd.h>

#define handle_error(msg) \
do { perror(msg); exit(EXIT_FAILURE); } while (0)

int main(void){
    
    char *path, buffer[1024];
    printf("pid %d\n", getpid());
    asprintf(&path, "%s%d", "/tmp/giro", getpid());
    
    sigset_t mask;
    int sigfd;
    struct signalfd_siginfo fdsiginfo;
    ssize_t s;

    /*-----setto i segnali che devo gestire personalmente----*/
    sigemptyset(&mask);
    sigaddset(&mask, SIGUSR1);
    sigaddset(&mask, SIGUSR2);
    
    /*------creo il file descriptor dove leggere i segnali-----*/
    sigfd = signalfd(-1, &mask, 0);
    
   
    while(1){
        
        if (sigprocmask(SIG_BLOCK, &mask, NULL) == -1)
            handle_error("sigprocmask");
        
        if (sigfd == -1)
            handle_error("signalfd");
    
        s = read(sigfd, &fdsiginfo, sizeof(struct signalfd_siginfo));
        if (s != sizeof(struct signalfd_siginfo))
            handle_error("read");
        
        if(fdsiginfo.ssi_signo == SIGUSR1){
            printf("Ho ricevuto un segnale.\n");
        }else if(fdsiginfo.ssi_signo == SIGUSR2){
            exit(1);
        }
        
        int fd = open(path, O_RDONLY);
        read(fd, buffer, 1024);
        close(fd);
        remove(path);
        printf("%s\n", buffer);
            
        kill(fdsiginfo.ssi_pid, SIGUSR1);
        
    }
    
    return 0;
}
