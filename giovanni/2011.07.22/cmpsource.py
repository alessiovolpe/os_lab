#!/usr/bin/python

from os import walk
from os.path import join
from sys import argv
from filecmp import cmp


listfile1 = {}
listfile2 = {}
for(root, directory, files1) in walk(argv[1]):
	for file in files1:
		try:
			listfile1[file].append(join(root, file))
		except KeyError:
			listfile1[file] = []
			listfile1[file].append(join(root, file))

for(root, directory, files2) in walk(argv[2]):
	for file in files2:
		try:
			listfile2[file].append(join(root, file))
		except KeyError:
			listfile2[file] = []
			listfile2[file].append(join(root, file))
	
for file in listfile1:
	try:
		if(not cmp(listfile2[file][0] ,listfile1[file][0])):
			print listfile2[file][0], listfile1[file][0], "differ"
	except KeyError:
		print listfile1[file][0]," not in ", argv[2]
		
		
for file in listfile2:
	try:
		listfile1[file]
	except KeyError:
		print listfile2[file][0]," not in ", argv[1]
		




