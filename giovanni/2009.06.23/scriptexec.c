#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>

int main(int argc, char * argv[]){

	int fd = open(argv[1], O_RDONLY);
	char buffer, i = 0, j = 0;
	char *myargv[15];
	
	for(i=0; i < 15; i++){
		myargv[i] = malloc(sizeof(char)*25);
	}
	i = 0;
	while(read(fd, &buffer, 1)){
		if(buffer == '\n'){
			j++;
			myargv[i][j] = '\0';
			myargv[i+1] = (char *)NULL;

			if(!fork()){
				execvp(myargv[0], myargv);
				break;
			}
			i = 0;  j = 0;
			myargv[i] = malloc(sizeof(char)*25);
		}
		else if(buffer == ' '){
			j++;
			myargv[i][j] = '\0';
			i++; j = 0;
			myargv[i] = malloc(sizeof(char)*25);
		}
		else{
			myargv[i][j] = buffer;
			j++;
		}
	}

	sleep(1);

	return 0;
}
