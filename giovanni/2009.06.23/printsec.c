#include <stdio.h>
#include <signal.h>



int main(void){
	
	sigset_t sigset;
	sigemptyset(&sigset);
	sigaddset(&sigset, SIGUSR1);
	sigprocmask(SIG_BLOCK, &sigset, NULL);
	int second = 0;
	
	struct timespec t;
	t.tv_sec = 0;
	t.tv_nsec = 1;
	
	
	while(1){
	
	sleep(1);
	if(sigtimedwait(&sigset, NULL, &t) == SIGUSR1)
		printf("%d\n", second);
	second++;
	
	}

	return 0;
}
