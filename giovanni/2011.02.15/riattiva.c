#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>




int main(int argc, char *argv[]){


	char *myargv[1024];
	int i = 1, status = -1;
	
	
	for(i; i < argc; i++){
		myargv[i-1] = argv[i];
	}
	myargv[i] = (char *)NULL;
	
	while(1){
		if(!fork()){
			execvp(myargv[0], myargv);
		}
		wait(&status);
		printf("The exit status of child is: %d\n", status);
		sleep(1);
		if(status ==0){
			break;
		}
	}
	
	

	return 0;
}
