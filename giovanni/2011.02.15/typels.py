#!/usr/bin/python

from os import listdir
from sys import argv
from os.path import isfile, join
import magic

listfile = [f for f in listdir(argv[1]) if isfile(f)]

for file in listfile:
	print magic.from_file(join("./", file))	

