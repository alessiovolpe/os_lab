#!/usr/bin/python

import sys, os

fd = os.path.join("./", sys.argv[1])

lines = []
fl = open(fd, "r")

with fl as f:
	for line in f:
		lines.append(line)
		
fl.close()

i = 0
for line in lines:
	try:
		lines[i], lines[i+1] = lines[i+1], lines[i]
		i = i+2
	except:
		break
	
fl = open(fd, "w")

for line in lines:
	fl.write(line)
	
fl.close()
