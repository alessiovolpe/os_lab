//
//  main.c
//  prorace
//
//  Created by simone faggi on 23/05/17.
//  Copyright © 2017 simone faggi. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define NOPID 100000

struct listcmd {
    char *cmd[1024];
    struct listcmd *next;
};


int getcmd(struct listcmd **head){
    int arg = 0, posch = 0, nocmd = 0;
    char ch_p = '\0', ch_a;
    
    (*head) = malloc(sizeof(struct listcmd));
    (*head)->cmd[arg] = malloc(BUFSIZ * sizeof(char*));
    struct listcmd *tmp = *head;
    
    while (1) {
        ch_a = getchar();
        
        if (ch_p == '\n' && ch_a == '\n') {
            return nocmd;
        }
        if (ch_a == '\n') { // nuovo comando
            tmp->next = malloc(sizeof(struct listcmd));
            tmp = tmp->next;
            tmp->cmd[0] = malloc(BUFSIZ * sizeof(char*));
            arg = 0;
            posch = 0;
            nocmd++;
        }
        else if (ch_a == ' ') {  // nuovo parametro
            arg++;
            tmp->cmd[arg] = malloc(BUFSIZ * sizeof(char*));
            posch = 0;
        }
        else{  // è un carattere
            tmp->cmd[arg][posch] = ch_a;
            posch++;
        }
        ch_p = ch_a;
    }
}


int main(void) {
    
    struct listcmd *head = NULL;
    
    int nocmd = getcmd(&head);
    
    char *processpid[NOPID];
    
    pid_t pid;
    
    for (int i = 0; i<nocmd; i++) {
        pid = fork();
        
        if (!pid){
            execvp(head->cmd[0], head->cmd);
        }
        asprintf(&(processpid[pid]), "%s", head->cmd[0]);
        head = head->next;
    }
    
    for (int i = 0; i<nocmd; i++) {
        pid = waitpid(-1, NULL, 0);
        printf("Terminato: %s\n", processpid[pid]);
    }
        
    return 0;
    
}
