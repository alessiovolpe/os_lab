#include <stdio.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/type.h>

int sort(const struct dirent **el_1, const struct dirent **el_2){
    struct stat file_info_1;
    struct stat file_info_2;
    
    char *path;
    
    asprintf(&path, "./%s", el_1->d_name);
    stat(path, &file_info_1);
    
    asprintf(&path, "./%s", el_2->d_name);
    stat(path, &file_info_2);
    
    if (file_info_2.st_ino == file_info_1.st_ino) {
        return alphasort(el_1, el_2);
    }
    return file_info_1.st_ino - file_info_2.st_ino;
    
}

void lsino(char* path){
    
    struct dirent **list;
    struct stat file_info;
    
    int n = scandir(path, &list, NULL, sort);
    
    for (int i = 0; i<n; i++) {
        char *pathname;
        asprintf(&pathname, "./%s", list[i]->d_name);
        stat(pathname, &file_info);
        printf("%s/%s %d\n", path, list[i]->d_name, file_info.st_ino);
    }
}


int main(int argc, char* argv[]){
    
    if (argc > 1) {
        chdir(argv[1]);
    }
    
    lsino(".");
    
    return 0;
}
