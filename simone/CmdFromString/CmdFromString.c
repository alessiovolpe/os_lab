#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <execs.h>

#define BUFLEN 1024

struct listcmd {
    char *cmd[1024];
    struct listcmd *next;
};


int cmd_from_string(struct listcmd **head, char* string){
    int arg = 0, posch = 0, nocmd = 0;
    char ch_a;
    
    if ((*head) != NULL){
    	return cmd_from_string((&(*head)->next), string);
   	}
   	
    (*head) = malloc(sizeof(struct listcmd));
    (*head)->cmd[arg] = malloc(BUFSIZ * sizeof(char*));
    (*head)->next = NULL;
   
    int i = 0;
    while (string[i] != '\0') {
        if (string[i] == ' ') {  // nuovo parametro
            arg++;
            (*head)->cmd[arg] = malloc(BUFSIZ * sizeof(char*));
            posch = 0;
        }
        else{  // è un carattere
            (*head)->cmd[arg][posch] = string[i];
            posch++;
        }
        i++;
    }	 
}


int main(void){
	struct listcmd *head = NULL;
	
	char *string = "ls";
	
	cmd_from_string(&head, string);

	int i = 0;
	pid_t pid;
	while(head != NULL){
		pid = fork();
		if(!pid){
			execvp(head->cmd[0], head->cmd);
		}
		head = head->next;
	}
	
	return 0;
}

