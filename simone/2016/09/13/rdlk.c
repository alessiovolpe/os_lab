//
//  main.c
//  rdlk
//
//  Created by simone faggi on 08/05/17.
//  Copyright © 2017 simone faggi. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>

int main(int argc, const char * argv[]) {
    
    struct flock lock;
    int fd = open(argv[1], O_RDONLY);
    
    memset(&lock, 0, sizeof(lock));
    
    lock.l_type = F_WRLCK;
    fcntl(fd, F_SETLKW, &lock);
    
    int time = rand() % 10 + 1;
    printf("locked      attende per %d secondi\n", time);
    sleep(time);
    
    lock.l_type = F_UNLCK;
    fcntl(fd, F_SETLKW, &lock);
    
    time = rand() % 10 + 1;
    printf("unlocked        attende per %d secondi\n", time);
    
    close(fd);
    
}
