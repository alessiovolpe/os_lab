//
//  main.c
//  cplk
//
//  Created by simone faggi on 08/05/17.
//  Copyright © 2017 simone faggi. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>

int main(int argc, const char * argv[]) {
    
    if (argc != 3) {
        printf("ARGUMENTS ERROR\n\n");
        return -1;
    }
    
    struct flock lock;
    char buf[BUFSIZ];
    int fd1 = open(argv[1], O_RDONLY);
    int fd2 = open(argv[2], O_WRONLY);
    
    
    memset(&lock, 0, sizeof(lock));
    
    lock.l_type = F_WRLCK;
    fcntl(fd2, F_SETLKW, &lock);

    
    long el_read = read(fd1, buf, BUFSIZ);
    while (el_read) {
        write(fd2, buf, el_read);
        el_read = read(fd1, buf, BUFSIZ);
    }
    
    int time = rand() % 10 + 1;
    printf("locked          attende per %d secondi\n", time);
    sleep(time);
    
    lock.l_type = F_UNLCK;
    fcntl(fd2, F_SETLKW, &lock);
    
    time = rand() % 10 + 1;
    printf("unlocked          attende per %d secondi\n", time);
    sleep(time);
    
    close(fd1);
    close(fd2);
    
    return 0;
}
