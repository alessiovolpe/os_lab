//
//  groupdir.c
//
//
//  Created by simone faggi on 09/05/17.
//
//

#include "groupdir.h"


char **groupdir(const char *dirp, const char *group){
    
    struct dirent **list = NULL;
    struct stat file_info;
    struct group* grp;
    int n, count = 0;
 
    
    if((n = scandir(dirp, &list, NULL, alphasort)) < 0){
        return NULL;
    }
    
    char** file_list = malloc(n+1 * sizeof(char*));
    
    /*printf("\nFiles of group: (%s)\n", group);*/
    
    for (int i=0; i<n; i++) {
        if(stat(list[i]->d_name, &file_info)<0) return NULL;
        if ((strcmp(list[i]->d_name,".")) != 0 && (strcmp(list[i]->d_name,"..")) != 0 && ((file_info.st_mode & S_IFMT) != S_IFDIR)){
            grp = getgrgid(file_info.st_gid);
            if (strcmp(grp->gr_name, group) == 0) {
                asprintf(&file_list[count]," %s", list[i]->d_name);
                count++;
            }
        }
    }
    
    asprintf(&file_list[count], "%c", '\0');
    
    return file_list;
}

void freestringarray(char** v){
    int i = 0;
    while (v[i] != NULL) {
        free(v[i]);
        i++;
    }
    free(v);
}
