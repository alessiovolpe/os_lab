//
//  main.c
//  printgroupdir
//
//  Created by simone faggi on 08/05/17.
//  Copyright © 2017 simone faggi. All rights reserved.
//

#include <stdio.h>
#include "printgroupdir.h"

int main(int argc, char *argv[]){
    
    if (argc != 3) {
        printf("ERROR ARGUMENTS\n");
        return -1;
    }
    
    printgroupdir(argv[1], argv[2]);
    
    return 0;
    
}
