//
//  Header.h
//  printgroupdir
//
//  Created by simone faggi on 08/05/17.
//  Copyright © 2017 simone faggi. All rights reserved.
//

#ifndef Header_h
#define Header_h

#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <grp.h>
#include <uuid/uuid.h>
#include <string.h>

void printgroupdir(const char* dirp, const char* group);

#endif /* Header_h */
