//
//  testgd.c
//
//
//  Created by simone faggi on 09/05/17.
//
//

#include "groupdir.h"

int main(int argc, char* argv[]){
    
     if (argc != 3) {
     printf("ERROR ARGUMENTS \n");
     return -1;
     }
     
    char** gr_files = groupdir(argv[1], argv[2]);
    if (gr_files == NULL) {
        printf("\nERROR in groupdir() execution\n\n");
        return -1;
    }
    
    int i = 0;
    while (strcmp(gr_files[i], "\0")!=0) {
        printf("%s\n", gr_files[i]);
        i++;
    }
    
    freestringarray(gr_files);
    
    return 0;
}
