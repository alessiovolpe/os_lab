#!/usr/bin/python
import sys, os, grp
from os import listdir, stat
from os.path import join, isdir

listfile = [f for f in listdir(sys.argv[1]) if not isdir(join(sys.argv[1], f))]
#in listfile tutti i file della directory

for file in listfile:
	fileinfo = stat(join(sys.argv[1], file))
	group_id = fileinfo.st_gid
	group = grp.getgrgid(group_id)[0]		#grp.struct_group(gr_name='simone', gr_passwd='x', gr_gid=1000, gr_mem=[])
	if (group == sys.argv[2]):
		print join(sys.argv[1], file)


