//
//  main.c
//  printgroupdir
//
//  Created by simone faggi on 08/05/17.
//  Copyright © 2017 simone faggi. All rights reserved.
//

#include <printgroupdir.h>

void printgroupdir(const char* dirp, const char* group){
    struct dirent **list;
    struct stat file_info;
    struct group* grp;
    int count = 0;
    
    int n = scandir(dirp, &list, NULL, alphasort);
    
    printf("\nFiles of group: (%s)\n", group);
    for (int i=0; i<n; i++) {
        stat(list[i]->d_name, &file_info);
        if (strcmp(list[i]->d_name,".") != 0 && strcmp(list[i]->d_name,"..") != 0 && (file_info.st_mode & S_IFMT) != S_IFDIR){
            grp = getgrgid(file_info.st_gid);
            if (strcmp(grp->gr_name, group) == 0) {
                printf("%s\n", list[i]->d_name);
                count++;
            }
        }
    }
    
    if (count == 0) printf("NONE\n");
    else printf("%d files of group (%s) in dir (%s)\n\n", count, group, dirp);
    
}
