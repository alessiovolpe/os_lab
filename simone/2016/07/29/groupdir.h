//
//  groupdir.h
//
//
//  Created by simone faggi on 09/05/17.
//
//

#ifndef groupdir_h
#define groupdir_h


#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <grp.h>
#include <uuid/uuid.h>
#include <string.h>
#include <stdlib.h>


char **groupdir(const char *dirp, const char *group);

void freestringarray(char** v);

#endif /* groupdir_h */
