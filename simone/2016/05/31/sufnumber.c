//
//  main.c
//  sufnumber
//
//  Created by simone faggi on 09/05/17.
//  Copyright © 2017 simone faggi. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <dirent.h>
#include <ctype.h>
#include <sys/stat.h>
#include <time.h>



int getnum(const struct dirent *elem){
    
    char* last_point = NULL;
    if ((last_point = strrchr(elem->d_name, '.')) != NULL) {
        last_point = &last_point[1];
        if (atoi(last_point) != 0) {
            return atoi(last_point);
        }
        else if (strcmp(last_point, "0") == 0){
            return 1;
        }
    }
    return 0;
}

/* ALPHASORT TORNA NUMERO (>0 SE ELEM1->D_NAME > ELEM2->D_NAME); ( 0 SE  ELEM1->D_NAME = ELEM2->D_NAME);( <0 ALTRIMENTI)*/

int sort(const struct dirent **elem1, const struct dirent **elem2){
    
    int n1 = getnum(*elem1);
    int n2 = getnum(*elem2);
    
    if (n1 == n2) return alphasort(elem1, elem2);
    else if (n1 > n2) return 1;
    else return -1;
    
}

int filt(const struct dirent* elem){
    if (elem != NULL) {
        struct stat file_info;
        stat(elem->d_name, &file_info);
        return getnum(elem);
    }
    return 0;
}

void exe(struct dirent **list, int i, int n, int old_sleep){
    
    char *path_exe = NULL;
    asprintf(&path_exe, "./%s", list[i]->d_name);
        
    int sleep_time = getnum(list[i]) - old_sleep;
        
    sleep(sleep_time);
    printf("Sleep: %d\n", sleep_time);
    printf("Eseguo: %s\n", list[i]->d_name);
        
    if (!fork()) {
        execl(path_exe, list[i]->d_name);
    }
    
    if (i<n-1) {
        exe(list, i+1, n, getnum(list[i]));
    }
    
}

int main(int argc, const char * argv[]) {
    
    if (argc != 2) {
        printf("ERROR ARGUMENTS \n");
        return -1;
    }
    
    struct dirent **list;
    
    int n = scandir(argv[1], &list, filt, sort);
    
    exe(list, 0, n, 0);
    
    return 0;
}
