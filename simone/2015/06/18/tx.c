#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>

int getbit(int buffer, int position){			
	return ((buffer >> position) & 1);
}

int main(int argc, char* argv[]){

	if (argc < 3){
		printf("Arguments error\n");
		return -1;
	}
	
	sigset_t mask;
	int sigfd;
	
	sigemptyset(&mask);
	sigaddset(&mask, SIGUSR1);
	sigaddset(&mask, SIGUSR2);
	sigprocmask(SIG_BLOCK, &mask, NULL);
	
	pid_t pid_rx = atoi(argv[1]);
	
	int i = 0, j;
	char buffer;
	for(i; i<strlen(argv[2]); i++){
		for(j=7; j>=0; j--){
			buffer = argv[2][i];
			if(getbit(buffer, j)){
				kill(pid_rx, SIGUSR1);
			} else kill(pid_rx, SIGUSR2);
			sigwaitinfo(&mask, NULL);			//NULL perchè non mi servono info sender;
		}
	}
	
	return 0;
}
