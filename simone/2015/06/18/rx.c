#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>


void setbit(char* buffer, int bit){		
	*buffer = *buffer << 1;				//Inserisco bit a destra.
	*buffer = *buffer | bit;			//Setto il bit a 1 o 0.
}


int main(void){
	printf("PID: %d\n", getpid());
	
	sigset_t mask;
	siginfo_t send_info;
	int sigfd;
	
	sigemptyset(&mask);
	sigaddset(&mask, SIGUSR1);
	sigaddset(&mask, SIGUSR2);
	sigprocmask(SIG_BLOCK, &mask, NULL);
	
	if (sigfd = signalfd(-1, &mask, 0) < 0){
		perror("signalfd"); 
		return -1;
	}
	
	char buffer;
	while(1){
		int i = 0;
		for(i; i<8; i++){
			int sig_num = sigwaitinfo(&mask, &send_info);	//In send_info le info del sender (send_info.si_pid ==> PID)
		
			if(sig_num == SIGUSR1){
			setbit(&buffer, 1);
			}
			else setbit(&buffer, 0);
		
			kill(send_info.si_pid, SIGUSR1);
		}
		
		write(fileno(stdout), &buffer, 1);
	}
	
	return 0;
	
}
