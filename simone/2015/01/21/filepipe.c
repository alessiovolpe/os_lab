#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

struct listcmd {
    char *cmd[1024];
    struct listcmd *next;
};
 
int getcmd(struct listcmd **head, FILE* fd){
    int arg = 0, posch = 0, nocmd = 0;
    char ch_a;
    
    (*head) = malloc(sizeof(struct listcmd));
    (*head)->cmd[arg] = malloc(BUFSIZ * sizeof(char*));
    struct listcmd *tmp = *head;
    
    while (1) {
        ch_a = fgetc(fd);
        
        if (ch_a == EOF) {
            return nocmd;
        }
        if (ch_a == '\n') { // nuovo comando
            tmp->next = malloc(sizeof(struct listcmd));
            tmp = tmp->next;
            tmp->cmd[0] = malloc(BUFSIZ * sizeof(char*));
            arg = 0;
            posch = 0;
            nocmd++;
        }
        else if (ch_a == ' ') {  // nuovo parametro
            arg++;
            tmp->cmd[arg] = malloc(BUFSIZ * sizeof(char*));
            posch = 0;
        }
        else{  // è un carattere
            tmp->cmd[arg][posch] = ch_a;
            posch++;
        }
    }
}

void fork_pipe(struct listcmd *head){

	int mypipe[2];
	pipe(mypipe);
	
    if(fork()){
        dup2(mypipe[1], 1);
        close(mypipe[0]);
        execvp(head->cmd[0], head->cmd);
    }
    else{
        dup2(mypipe[0], 0);
        close(mypipe[1]);
        execvp(head->next->cmd[0], head->next->cmd);
    }
    
	waitpid(-1, 0, NULL);
}

int main(int argc, char *argv[]){
	
	if (argc < 2){
		printf("Error arg.\n");
		return -1;
	}
	
	FILE* arg_file = fopen(argv[1], "r");
	
	struct listcmd *head = NULL;
	getcmd(&head, arg_file);
	
	fork_pipe(head);
	
	return 0;
}
