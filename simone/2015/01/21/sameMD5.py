#!/usr/bin/python

import hashlib
from os import walk
from os.path import join
from sys import argv


def md5Checksum(filePath):
    with open(filePath, 'rb') as fh:
        m = hashlib.md5()
        while True:
            data = fh.read(8192)
            if not data:
                break
            m.update(data)
        return m.hexdigest()
        
# In dirlist le dir da scandire
dirlist = []
for i in range(len(argv)):
	dirlist.append(argv[i])
	
dictionary = {}

for dir in dirlist:
	for root, _, files in walk(dir):
		for file in files:
			try: 
				dictionary[md5Checksum(join(root, file))].append(file)
			except:
				dictionary[md5Checksum(join(root, file))] = []
				dictionary[md5Checksum(join(root, file))].append(file)		

for key in dictionary:
	print
	print key, ":",
	for file in range(len(dictionary[key])):
		print dictionary[key][file],

