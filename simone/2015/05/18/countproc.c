#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>


int filt(const struct dirent *elem){
	struct stat file_info;
	stat(elem->d_name, &file_info);
	if( (atoi(elem->d_name) != 0) /*&& ((file_info.st_mode & S_IFMT) == S_IFDIR)*/ ) return 1;
	else return 0;
}

int main(void){
	struct dirent **list;
	
	int n = scandir("/proc", &list, filt, alphasort);
	
	int i = 0;
	for(i ; i<n; i++){
		printf("proc: %s\n", list[i]->d_name);
	}
	
	
	int count = n;
	printf("Number of active porcess: \n%d\n", count);
	while(1){
	
		if (count != n){
			printf("%d\n", count);
			count = n;
		}
		
		sleep(1);
		
		n = scandir("/proc", &list, filt, alphasort);
		
	}
	
	return 0;
}
