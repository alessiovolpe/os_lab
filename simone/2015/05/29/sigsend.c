#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/signalfd.h>


int main(int argc, char* argv[]){
	char* path, buffer[1024];
	asprintf(&path, "%s%s", "/tmp/giro", argv[1]);
	int pid_recv = atoi(argv[1]);
	
	sigset_t mask;
	int sigfd;
	struct signalfd_siginfo sig_info;
	ssize_t s;
	
	if(sigemptyset(&mask)<0){
		perror("sigemptyset");
		exit(EXIT_FAILURE);
	}
	
	sigaddset(&mask, SIGUSR1);
	
	if ((sigfd = signalfd(-1, &mask, 0)) < 0){
		perror("signalfd");
		exit(EXIT_FAILURE);
	}
	
	
	if(sigprocmask(SIG_BLOCK, &mask, NULL)<0){
		perror("sigprocmask");
		exit(EXIT_FAILURE);
	}
	
	while(1){
		
		int fd = open(path, O_WRONLY | O_CREAT , O_IRWXU | IRWXG);
		if (read(STDIN_FILENO, buffer, sizeof(buffer)) == 0){		/* STDIN TERMINA */
			kill(pid_recv, SIGUSR2);
			exit(1);
		}
		
		write(fd, buffer, strlen(buffer));
		close(fd);
		
		kill(pid_recv, SIGUSR1);
		
		s = read(sigfd, &mask, sizeof(struct signalfd_siginfo));
		
		if(sig_info.ssi_signo == SIGUSR1){
			printf("Ho ricevuto un segnale SIGUSR1\n");
		}
	}
	
	return 0;

}
