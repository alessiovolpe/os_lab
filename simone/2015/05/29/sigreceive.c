#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/signalfd.h>


int main(void){
	char *path, buffer[1024];
	
	printf("PID: %d\n", getpid())
	asprintf(&path, "%s%d", "/tmp/giro", getpid());
	
	sigset_t mask;
	int sigfd;
	struct signalfd_siginfo sig_info;
	ssize_t s;
	
	if(sigemptyset(&mask)<0){
		perror("sigemptyset");
		exit(EXIT_FAILURE);
	}
	
	sigaddset(&mask, SIGUSR1);
	sigaddset(&mask, SIGUSR2);
	
	if ((sigfd = signalfd(-1, &mask, 0)) < 0){
		perror("signalfd");
		exit(EXIT_FAILURE);
	}
	
	
	if(sigprocmask(SIG_BLOCK, &mask, NULL)<0){
		perror("sigprocmask");
		exit(EXIT_FAILURE);
	}
	
	while(1){
	
		s = read(sigfd, &mask, sizeof(struct signalfd_siginfo));
		
		if(sig_info.ssi_signo == SIGUSR1){
			printf("Ho ricevuto un segnale SIGUSR1\n");
		}
		else if (sig_info.ssi_signo == SIGUSR2) exit(1);
		
		int fd = open(path, O_RDONLY);
		read(fd, &buffer, sizeof(buffer));
		printf("%s\n", buffer);
		remove(path);
		close(fd);
		
		kill(sig_info.ssi_pid, SIGUSR1);
		
	}
	
	return 0;
}
