#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/signalfd.h>
#include <unistd.h>

void mycat(){
	char buffer[1024];
	while(1){
		scanf("%s", buffer);
		printf("%s\n", buffer);
	}
}

void catchsign(){
	sigset_t mask;
	int sigfd;
	struct signalfd_siginfo sig_info;
	ssize_t s;
	
	if(sigemptyset(&mask)<0){
		perror("sigemptyset");
		exit(EXIT_FAILURE);
	}
	
	sigaddset(&mask, SIGUSR1);
	
	if ((sigfd = signalfd(-1, &mask, 0)) < 0){
		perror("signalfd");
		exit(EXIT_FAILURE);
	}
	
	
	if(sigprocmask(SIG_BLOCK, &mask, NULL)<0){
		perror("sigprocmask");
		exit(EXIT_FAILURE);
	}
	
	while(1){
		s = read(sigfd, &mask, sizeof(struct signalfd_siginfo));
		
		if(sig_info.ssi_signo == SIGUSR1){
			printf("Ho ricevuto un segnale SIGUSR1\n");
		}
	}

}

int main(void){

	switch(fork()){
		case 0:
			mycat();
			break;
		default:
			catchsign();
			break;
		case -1:
			perror("fork");
			break;
	}
	
	return 0;
		
}
