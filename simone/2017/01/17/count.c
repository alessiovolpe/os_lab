//
//  main.c
//  countfiles
//
//  Created by simone faggi on 10/05/17.
//  Copyright © 2017 simone faggi. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

struct fileelem{
    dev_t dev;
    ino_t ino;
    struct fileelem *next;
};

int addfileelem(struct fileelem **head, dev_t dev, ino_t ino){
    
    if (*head == NULL) {
        *head = malloc(sizeof(struct fileelem));
        (*head)->dev = dev;
        (*head)->ino = ino;
        (*head)->next = NULL;
        return 1;
    }
    else if ((*head)->dev == dev && (*head)->ino == ino){
        return 0;
    }
    else return addfileelem(&((*head)->next), dev, ino);
}

int filt(const struct dirent *elem){
    
    if (strcmp(elem->d_name, ".") == 0 || strcmp(elem->d_name, "..") == 0){
        return 0;
    }
    else return 1;
}

int dirscan (char* path, struct fileelem **head){
    
    struct dirent **list;
    int count = 0;
    
    int n = scandir(path, &list, filt, alphasort);
    
    for (int i = 0; i<n; i++) {
        struct stat file_info;
        char *filepath;
        asprintf(&filepath, "%s/%s", path, list[i]->d_name);
        stat(filepath, &file_info);
        switch (file_info.st_mode & S_IFMT) {
            case S_IFREG:
                count = count + addfileelem(head, file_info.st_dev, file_info.st_ino);
                printf("file %s ... %d\n", filepath, count);
                break;
            case S_IFDIR:
                count = count + dirscan(filepath, head);
                break;
        }
    }
    return count;
}

int main(int argc, const char * argv[]) {
    
    struct fileelem *head = NULL;
    
    if (argc > 1) {
        chdir(argv[1]);
    }
    
    int result = dirscan(".", &head);
    
    printf("Num of elements in subtree: %d\n", result);
    
    return 0;

}
