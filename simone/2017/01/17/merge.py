#!/usr/bin/python

import os, sys, shutil

a_dir = sys.argv[1]
b_dir = sys.argv[2]
c_dir = sys.argv[3]

a_file = [f for f in os.listdir(a_dir) if not os.path.isdir(os.path.join(a_dir, f))]
b_file = [f for f in os.listdir(b_dir) if not os.path.isdir(os.path.join(b_dir, f))]

c_file = {}

for f in a_file:
    c_file[f] = os.path.join(a_dir, f)

for f in b_file:
    try:
        if (os.path.getmtime(c_file[f]) < os.path.getmtime(os.path.join(b_dir, f))):
            c_file[f] = os.path.join(b_dir, f)
    except KeyError:
            c_file[f] = os.path.join(b_dir, f)

for f in c_file:
    shutil.move(c_file[f], os.path.join(c_dir, f))
