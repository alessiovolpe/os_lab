//
//  main.c
//  ager
//
//  Created by simone faggi on 30/04/17.
//  Copyright © 2017 simone faggi. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <utime.h>

int ager_time = 864000;  // default: 10 giorni in secondi

char* help = "([1]--> actime || modtime || help) ([2]--> ager_time(days))  ([3....]-->Files || Current directory if not specify)";

struct file_list{
    const char* pathname;
    struct file_list* next;
};

int getmod(const char* argv){
    if (strcmp(argv, "help") == 0) {
        printf("%s", help);
    }
    else if (strcmp(argv, "modtime") == 0){
        return 1;
    }
    else if (strcmp(argv, "actime") == 0){
        return 1;
    }
    printf("ERROR MODE: ./ager2 help for more.\n");
    return -1;
}

int addlist(struct file_list **head, const char* name){
    if (*head == NULL){
        *head = malloc(sizeof(struct file_list));
        (*head)->pathname = name;
        (*head)->next = NULL;
        return 1;
    }
    else return addlist(&((*head)->next), name);
}


int filt(const struct dirent *elem){
    struct stat buf;
    stat(elem->d_name, &buf);
    if (strcmp(elem->d_name,".") == 0 || strcmp(elem->d_name,"..") == 0 || (buf.st_mode & S_IFMT) == S_IFDIR){
        return 0;
    }
    return 1;
}


int ager(struct file_list *list, const char* ager_mod, int ager_time){
    if (list == NULL) {
        printf("Error list==NULL\n");
        return -1;
    }
    else{
        while (list != NULL) {
            struct stat buf_st;
            struct utimbuf new_t;
            
            if (stat(list->pathname, &buf_st)<0){
                perror(list->pathname);
                return -1;
            }
            
            ager_time = ager_time * 86400;
            
            if (strcmp(ager_mod, "actime") == 0) {
                new_t.actime = buf_st.st_atime - ager_time;
            }
            else if (strcmp(ager_mod, "modtime") == 0){
                new_t.modtime = buf_st.st_mtime - ager_time;
            }
            
            if (utime(list->pathname, &new_t)<0){
                perror(list->pathname);
                return -1;
            }
            
            list = list->next;
        }
    }
    return 0;
}

/*  |ARGV[0]|                |ARGV[1]|                  |[ARGV[2]|            |ARGV[3...] */
/*   ./ager2       ([1]== actime || modtime || help) ([2]==(int)ager_time)  ([3....]==Files)*/

int main(int argc, const char * argv[]) {
    
    struct file_list *file_l = NULL;
    
    if (argc < 3) {
        printf("ARGUMENTS ERROR: ./ager2 help for more\n");
        return -1;
    }
    
    if (argc == 3) {
        struct dirent **list;
        int n = scandir(".", &list, filt, alphasort);
        for (int i=0; i<n; i++) {
            addlist(&file_l, list[i]->d_name);
        }
    }
    
    else{
        for (int i=0; i<argc; i++) {
            addlist(&file_l, argv[i]);
        }
    }
    
    int ager_time = atoi(argv[2]);
    
    if(getmod(argv[1])<0) return -1;
    
    if(ager(file_l, argv[1], ager_time)<0) return -1;
    
    return 0;
    
}
