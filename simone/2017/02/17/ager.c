//
//  main.c
//  ager
//
//  Created by simone faggi on 30/04/17.
//  Copyright © 2017 simone faggi. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <utime.h>

struct file_list{
    const char* pathname;
    struct file_list* next;
};


int addlist(struct file_list **head, const char* name){
    if (*head == NULL){
        *head = malloc(sizeof(struct file_list));
        (*head)->pathname = name;
        (*head)->next = NULL;
        return 1;
    }
    else return addlist(&((*head)->next), name);
}


int filt(const struct dirent *elem){
    struct stat buf;
    stat(elem->d_name, &buf);
    if (strcmp(elem->d_name,".") == 0 || strcmp(elem->d_name,"..") == 0 || (buf.st_mode & S_IFMT) == S_IFDIR){
        return 0;
    }
    return 1;
}


int ager(struct file_list *list){
    if (list == NULL) {
        printf("Error list==NULL\n");
        return -1;
    }
    else{
        while (list != NULL) {
            struct stat buf_st;
            struct utimbuf new_t;
            
            if (stat(list->pathname, &buf_st)<0){
                perror(list->pathname);
                return -1;
            }
            
            
            new_t.actime = buf_st.st_atime - 864000;
            new_t.modtime = buf_st.st_mtime - 864000;
            
            if (utime(list->pathname, &new_t)<0){
                perror(list->pathname);
                return -1;
            }
            
            list = list->next;
        }
    }
    return 0;
}



int main(int argc, const char * argv[]) {
    
    struct file_list *file_l = NULL;
    
    if (argc == 1) {
        struct dirent **list;
        int n = scandir(".", &list, filt, alphasort);
        for (int i=0; i<n; i++) {
            addlist(&file_l, list[i]->d_name);
        }
    }
    
    else{
        for (int i=0; i<argc; i++) {
            addlist(&file_l, argv[i]);
        }
    }
    
    if(ager(file_l)<0) return -1;
    
    return 0;
    
}
