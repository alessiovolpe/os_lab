#!/usr/bin/python

import os
import sys

listfile = [f for f in os.listdir("./") if not os.path.isdir(os.path.join("./", f))]

dictionary = {}

for file in listfile:
    _, ext = os.path.splitext(file)
    if ext not in dictionary:
        dictionary[ext] = []
    dictionary[ext].append(file)

for i in dictionary:
    if i != "":
        sys.stdout.write(i + ": ")
        for j in dictionary[i]:
            sys.stdout.write(j + " ")
        print

for i in dictionary:
    if i == "":
        for j in dictionary[i]:
            sys.stdout.write(j + " ")
        print
