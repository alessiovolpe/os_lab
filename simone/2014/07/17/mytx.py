#!/usr/bin/python

import os, sys

listfile = [f for f in os.listdir(sys.argv[1]) if os.path.isfile(os.path.join(sys.argv[1], f))]

ddd = open("ddd.txt", "w")

for file in listfile:
    ddd.write(str(os.stat(file).st_size) + " " + file + "\n")

ddd.close()
