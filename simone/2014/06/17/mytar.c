//
//  main.c
//  mytar
//
//  Created by simone faggi on 10/05/17.
//  Copyright © 2017 simone faggi. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

off_t getsize(char* file){
    struct stat file_info;
    stat(file, &file_info);
    return file_info.st_size;
}

int filt(const struct dirent *elem){
    if (elem != NULL) {
        
        struct stat file_info;
        stat(elem->d_name, &file_info);
        
        if (strcmp(elem->d_name, ".") == 0 || strcmp(elem->d_name, "..") == 0|| strcmp(elem->d_name, ".DS_Store") == 0 || (file_info.st_mode & S_IFMT) == S_IFDIR ) {
            return 0;
        }
        return 1;
    }
    return 0;
}



int file_list(const char* dir, char* reg_list[]){
    struct dirent **list;
    int num;
    
    int n = scandir(dir, &list, filt, alphasort);
    
    for (num = 0; num<n; num++) {
            reg_list[num] = list[num]->d_name;
        }
    
    return num;
}


int main(int argc, const char * argv[]) {
    
    if (argc != 3) {
        printf("ERROR ARGUMENTS\n");
        return -1;
    }
    
    
    FILE* new_file = fopen(argv[2], "w");
    FILE* fileToRead;
    char* list[1024];
    char* openfile = NULL;
    off_t size;
    
    int lenlist = file_list("/Users/simone/Desktop/Sistemi Operativi/lab_OS/esC/mytar/mytar", list);
    
    list[lenlist] = NULL;

    int i = 0;
    while (list[i] != NULL) {
        size = getsize(list[i]);
        if (size) {
            if (strcmp(argv[0], list[i])) {
                char dim[200];
                
                fileToRead = fopen(list[i], "r");
                
                sprintf(dim, "%llu", size);
                
                fputs("Name: ", new_file);          //name
                fputs(list[i], new_file);
                fputs(" -----> Size: ", new_file);
                fputc('\0', new_file);
                
                fputs(dim, new_file);               //size
                fputs(" byte\n", new_file);
                fputc('\0', new_file);
                
                fclose(fileToRead);
            }
        }
        i++;
    }
    
    fclose(new_file);
    
    
    
}
