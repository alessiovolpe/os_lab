#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <execs.h>

#define BUFLEN 1024

struct listcmd {
    char *cmd[1024];
    struct listcmd *next;
};


int getcmd(struct listcmd **head, FILE* fd){
    int arg = 0, posch = 0, nocmd = 0;
    char ch_p = '\0', ch_a;
    
    (*head) = malloc(sizeof(struct listcmd));
    (*head)->cmd[arg] = malloc(BUFSIZ * sizeof(char*));
    struct listcmd *tmp = *head;
    
    while (1) {
        ch_a = fgetc(fd);
        
        if (ch_a == EOF) {
            return nocmd;
        }
        if (ch_a == '\n') { // nuovo comando
            tmp->next = malloc(sizeof(struct listcmd));
            tmp = tmp->next;
            tmp->cmd[0] = malloc(BUFSIZ * sizeof(char*));
            arg = 0;
            posch = 0;
            nocmd++;
        }
        else if (ch_a == ' ') {  // nuovo parametro
            arg++;
            tmp->cmd[arg] = malloc(BUFSIZ * sizeof(char*));
            posch = 0;
        }
        else{  // è un carattere
            tmp->cmd[arg][posch] = ch_a;
            posch++;
        }
        ch_p = ch_a;
    }
}


void runcmd(char* filepath){

	struct listcmd *head = NULL;
	
	
	FILE* fd = fopen(filepath, "r");
	
	
	int nofcmd = getcmd(&head, fd);
	
	pid_t pid;
	int i = 0;
	for (i; i<nofcmd; i++) {
        pid = fork();
        if (!pid){
            execvp(head->cmd[0], head->cmd);
        }
        head = head->next;
    }

	
	waitpid(-1, NULL, 0);
	
	kill(0, SIGTERM);
}

int main(int argc, char* argv[]){
	
		if (argc < 2 ){
			printf("Error arg\n");
			return -1;
		}
		
		runcmd(argv[1]);
		
}

