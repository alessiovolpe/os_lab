#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/inotify.h>
#include <sys/stat.h>

#define EVENT_SIZE  ( sizeof (struct inotify_event) )
#define BUF_LEN     ( 1024 * ( EVENT_SIZE + 16 ) )

void inotify(char *dirpath){
	int length;
  	int fd;
  	int wd;
  	char buffer[BUF_LEN];

  	fd = inotify_init();

  	if ( fd < 0 ) {
   	 	perror( "inotify_init" );
 	}

  	wd = inotify_add_watch( fd, dirpath,  IN_CREATE | IN_DELETE | IN_DELETE_SELF);
	
	int flag = 1;
	
	while (flag) {
    	int i = 0;
    	length = read( fd, buffer, BUF_LEN );

    	if ( length < 0 ) {
   	   		perror( "read" );
  	 	}  
	
  	  	while ( i < length ) {
      		struct inotify_event *event = ( struct inotify_event * ) &buffer[ i ];
      			/* Creazione di un elemento */
        		if ( event->mask & IN_CREATE ) {
        			/* Creazione cartella */
          			if ( event->mask & IN_ISDIR ) {
            			printf( "The directory %s was created.\n", event->name );  
            			pid_t pid;
            			pid = fork();
            			if(!pid){
           					char *newdirpath = malloc((strlen(dirpath))+event->len+1);
           					strcpy(newdirpath,dirpath);
                        	strcat(newdirpath,"/");
                        	strcat(newdirpath,event->name);
                        	inotify(newdirpath);
            			}
          			}
          			/* Creazione file */
          			else {
            			printf( "The file %s was created.\n", event->name );
          			}
        		}
        		/* Eliminazione di un elemento */
       			else if ( event->mask & IN_DELETE ) {
       				/* Eliminazione cartella */
          			if (event->mask & IN_ISDIR){
          				printf("The directory %s was deleted.\n", event->name);
          			}
          			else {
            			printf( "The file %s was deleted.\n", event->name );
          			}
        		}
        		else if ( event->mask & IN_DELETE_SELF){
        			printf("The directory %s was deleted.\n", dirpath);
        			flag = 0;
        		}
        		i += EVENT_SIZE + event->len;
     		}
    }
    ( void ) inotify_rm_watch( fd, wd );
  	( void ) close( fd );
}



int main(int argc, char* argv[]){
	
	if (argc < 2) {
		printf("Error arg\n");
		return -1;
	}
	
	int dir;

	if(dir = mkdir(argv[1], 0700) < 0) return -1;
	
	inotify(argv[1]);
	
	return 0;
	
		
}
