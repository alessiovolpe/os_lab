//
//  main.c
//  lineandir
//
//  Created by simone faggi on 11/05/17.
//  Copyright © 2017 simone faggi. All rights reserved.
//

#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>

#define BUFSIZE 1024


int reg(const char* file){
    
    struct stat file_info;
    
    if (lstat(file, &file_info) < 0) {
        printf("ERROR lstat() function \n");
        return 0;
    }
    
    if (!S_ISREG(file_info.st_mode)) {
        return 0;
    }
    
    return 1;
}

int filt(const struct dirent *elem){
    if (elem != NULL) {
        return reg(elem->d_name);
    }
    return 0;
}

int count_char(char* arg_file, int arg_n){
    
    FILE* fd;
    
    if ((fd=fopen(arg_file, "r")) == NULL) {
        printf("ERROR fopen() function \n");
        return -1;
    }
    
    int count = 0;
    char buffer[BUFSIZE];
    
    int line = 1;
    while (fgets(buffer, BUFSIZE, fd) != NULL) {
        if (line == arg_n) {
            count = (int)strlen(buffer) - 1;
        }
        line++;
    }
    
    return count;
    
}

int main(int argc, const char * argv[]) {
    
    struct dirent **list;
    int result = 0;
    int arg_n = atoi(argv[2]);
    
    int n = scandir(argv[1], &list, filt, alphasort);
    
    for (int i = 0; i<n; i++) {
        result += count_char(list[i]->d_name, arg_n);
    }
    
    printf("%d\n", result);
    
    return 0;
    
}
