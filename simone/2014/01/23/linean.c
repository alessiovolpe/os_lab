//
//  main.c
//  linean
//
//  Created by simone faggi on 26/04/17.
//  Copyright © 2017 simone faggi. All rights reserved.
//

#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>

#define BUFSIZE 200

int reg(const char* file){
    
    struct stat file_info;
    
    if (lstat(file, &file_info) < 0) {
        printf("ERROR lstat() function \n");
        return -1;
    }
    
    if (!S_ISREG(file_info.st_mode)) {
        return -1;
    }
    
    return 0;
}

int count_char(FILE* arg_file, int arg_n){
    
    int count = 0;
    char buffer[BUFSIZE];
    
    int line = 1;
    while (fgets(buffer, BUFSIZE, arg_file) != NULL) {
        if (line == arg_n) {
            count = (int)strlen(buffer) - 1;
        }
        line++;
    }
    
    return count;
    
}

/* argc = 3; argv[0] = linean.c; argv[1] = filename; argv[2] = int n */
int main(int argc, const char * argv[]) {
    
    FILE* arg_file;
    
    /* ===================== CONTROLLO ERRORI ====================== */
    if (argc != 3) {
        printf("ERROR arguments number \n");
        return -1;
    }
    
    if (reg(argv[1])<0) {
        printf("ERROR reg() function \n");
        return -1;
    }
    
    if ((arg_file=fopen(argv[1], "r")) == NULL) {
        printf("ERROR fopen() function \n");
        return -1;
    }
    /* ============================================================= */
    
    int arg_n = atoi(argv[2]);                              // n-esima riga
    int count = 0;
    
    count = count_char(arg_file, arg_n);
    printf("Caratteri nella %d° riga: %d \n", arg_n, count);
    
    return 0;
    
}
