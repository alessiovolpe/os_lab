#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/msg.h>


struct msgbuf {
	long mtype;       /* message type, must be > 0 */
	char mtext[40];    /* message data */
};

const char *mex = "messaggio";

int main(void){
	
	struct timespec start, end;
	
	int count = 0;
	char *buf_c = malloc(sizeof(char)*40);
	
	int msgq = msgget(IPC_PRIVATE, IPC_CREAT | S_IRUSR | S_IWUSR);

	clock_gettime(CLOCK_MONOTONIC, &start);
	
	pid_t pid = fork();
	
	while(count < 100000){
		if(!pid){
			struct msgbuf msgc;
			msgrcv(msgq, &msgc, 40, 0, MSG_NOERROR);
			msgsnd(msgq, &msgc, 40, MSG_NOERROR);
		}
		else {
			struct msgbuf msgp;
			
			msgp.mtype = 1;
			strcpy(msgp.mtext, mex);
			
			msgsnd(msgq, &msgp, 40, MSG_NOERROR);
			msgrcv(msgq, &msgp, 40, 0, MSG_NOERROR);
		}
		count++;
	}
		
	if(!pid){
		return;
	}
	
	clock_gettime(CLOCK_MONOTONIC, &end);
	
	double time_spent = ((double)end.tv_sec+1.0e-9*end.tv_nsec - ((double)start.tv_sec+1.0e-9*start.tv_nsec));
	
	printf("TEMPO IMPIEGATO: %f seconds\n", time_spent);
	
	return 0;
}
