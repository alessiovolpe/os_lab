#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>

void waitforsignal(int sig){
	sigset_t mask;
        siginfo_t send_info;
	int sigfd;

	sigemptyset(&mask);
        sigaddset(&mask, sig);
        sigprocmask(SIG_BLOCK, &mask, NULL);
	
	if (sigfd = signalfd(-1, &mask, 0) < 0){
                perror("signalfd");
        }
	
	int sig_num = sigwaitinfo(&mask, &send_info);
}

void runcmd(char* argv[]){
	execvp(argv[0], argv);
}

int main(int argc, char* argv[]){

	if (argc < 2){
		printf("Error:");
		return -1;
	}

	char **new_argv;
	new_argv = &argv[2];
	

	int i = 0;
	for(i ; i < argc; i++){
		printf("waiting for %d signal\n", atoi(argv[1]));
		waitforsignal(atoi(argv[1])); 
		runcmd(new_argv);	
	}

	return 0;
}
