#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

const char *mex = "messaggio";
int main(void){
	
	struct timespec start, end;
	
	int count = 0, fdout;
	char *buf_c = malloc(sizeof(char)*40);
	
	int p1[2];
	int p2[2];
	pipe(p1);	// padre scrive; figlio legge
	pipe(p2);	// padre legge; figlio scrive

	clock_gettime(CLOCK_MONOTONIC, &start);
	
	pid_t pid = fork();
	
	while(count < 100000){
		if(!pid){
			close(p1[1]);	//chiude in scrittura da pipe1
			close(p2[0]);	//chiude in lettura da pipe2
			
			read(p1[0], &buf_c, 40);
			write(p2[1], &buf_c, 40);	
		}
		else {
			close(p1[0]);	//chiude in lettura da pipe1
			close(p2[1]); 	//chiude in scrittura da pipe2
			
			write(p1[1], &mex, 40);
			read(p2[0], &mex, 40);		
		}
		count++;
	}
		
	if(!pid){
		return;
	}
	
	clock_gettime(CLOCK_MONOTONIC, &end);
	
	double time_spent = ((double)end.tv_sec+1.0e-9*end.tv_nsec - ((double)start.tv_sec+1.0e-9*start.tv_nsec));
	
	printf("TEMPO IMPIEGATO: %f seconds\n", time_spent);
	
	return 0;
}
