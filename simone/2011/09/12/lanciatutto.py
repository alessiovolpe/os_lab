#!/usr/bin/python

import os, sys


listfile = [f for f in os.listdir(".") if (f != sys.argv[0].split("/")[1] and os.path.isfile(os.path.join("./", f)) and os.access(os.path.join("./", f), os.X_OK))]

new_argv = []

for i in range(1, len(sys.argv)):
	new_argv.append(sys.argv[i])
	
for file in listfile:
	new_argv.insert(0, os.path.join("./", file))
	if not os.fork():
		os.execvp(new_argv[0], new_argv)
	
	
