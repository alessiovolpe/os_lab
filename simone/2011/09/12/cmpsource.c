#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

int filt(const struct dirent *elem){
	char *last_point = strchr(elem->d_name, '.');
	
	if(last_point != NULL && (!strcmp(last_point, ".c") || !strcmp(last_point, ".h"))) 
		return 1;
	
	return 0;
}


void checkdiff(struct dirent **list1, struct dirent **list2, char* dirpath1, char* dirpath2, int len1, int len2){
	int i = 0, j = 0;
	
	for(i; i<len1; i++){
		int find = 0;
		for(j; j<len2; j++){
			if (!strcmp(list1[i]->d_name, list2[j]->d_name)){
				find = 1;
				break;
			}
		}
		if (find == 0){
			printf("%s not in %s\n", list1[i]->d_name, dirpath2);
		}
	}
}


int main(int argc, char *argv[]){
	
		if(argc < 3){
			printf("ERR arg.\n");
			return;		
		}
		
		struct dirent **list1;
		struct dirent **list2;
		int n1, n2;
		n1 = scandir(argv[1], &list1, filt, alphasort);
		n2 = scandir(argv[2], &list2, filt, alphasort);
		
		checkdiff(list1, list2, argv[1], argv[2], n1, n2);
		checkdiff(list2, list1, argv[2], argv[1], n2, n1);
		
		return 0;
}
