#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

int filt(const struct dirent *elem){
	char *last_point = strchr(elem->d_name, '.');
	
	if(last_point != NULL){
		if(!strcmp(last_point, ".c") || !strcmp(last_point, ".h")){
			return 1;
		}
	}
	
	return 0;
}

void checkfiles(char* path1, char* path2){
	FILE *fd1, *fd2;
	fd1 = fopen(path1, "r");
	fd2 = fopen(path2, "r");
	
	int ch1, ch2;
	ch1 = getc(fd1);
	ch2 = getc(fd2);
	
	while((ch1 != EOF) && (ch2 != EOF) && (ch1 == ch2)){
		ch1 = getc(fd1);
		ch2 = getc(fd2);
	}
	
	if(ch1 != ch2){
		printf("%s and %s differ\n", path1, path2);
	}
	
}

//Return 1 if in one dir, 0 otherwise.
void checkdiff(struct dirent **list1, struct dirent **list2, char* dirpath1, char* dirpath2, int len1, int len2){
	int i = 0, j = 0;
	char *path1, *path2;
	
	for(i; i<len1; i++){
		int find = 0;
		for(j; j<len2; j++){
			if (!strcmp(list1[i]->d_name, list2[j]->d_name)){			//file in entrambe le directory
				find = 1;
				asprintf(&path1, "%s/%s", dirpath1, list1[i]->d_name);
				asprintf(&path2, "%s/%s", dirpath2, list1[j]->d_name);
				checkfiles(path1, path2);
				break;
			}
		}
	}
}


int main(int argc, char *argv[]){
	
		if(argc < 3){
			printf("ERR arg.\n");
			return;		
		}
		
		struct dirent **list1;
		struct dirent **list2;
		int n1, n2;
		n1 = scandir(argv[1], &list1, filt, alphasort);
		n2 = scandir(argv[2], &list2, filt, alphasort);
		
		checkdiff(list1, list2, argv[1], argv[2], n1, n2);
		checkdiff(list2, list1, argv[2], argv[1], n2, n1);
		
		return 0;
}
