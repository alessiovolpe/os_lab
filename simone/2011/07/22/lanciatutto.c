#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>

int filt(const struct dirent *elem){
	struct stat file_info;
	stat(elem->d_name, &file_info);
	
	if(file_info.st_mode & S_IXUSR && strcmp(elem->d_name, "lanciatutto") && strcmp(elem->d_name, ".") && strcmp(elem->d_name, "..") && (file_info.st_mode & S_IFMT)!=S_IFDIR){
		return 1;
	}
	
	return 0;
}

void runexe(char *myargv[]){
	pid_t pid;
	pid = fork();
	
	if(!pid){
		execvp(myargv[0], myargv);
	}
}


int main(int argc, char *argv[]){
	
	char **myargv;
	
	int i;
	for(i=1; i<argc; i++){
		asprintf(&myargv[i], "%s", argv[i]);
	}
	
	struct dirent **list;
	int n = scandir(".", &list, filt, alphasort);
	
	
	for(i=0; i<n; i++){
		asprintf(&myargv[0], "%s%s", "./", list[i]->d_name);
		runexe(myargv);
	}
	
	for(i=0; i<n; i++){
		waitpid(-1, NULL, 0);
	}
	
	return 0;
}
