#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <sys/types.h> 
#include <sys/stat.h> 
#include <fcntl.h>

int main(int argc, char* argv[]){

	static struct option long_options[] = {
            {"in",     	required_argument, 0, 'i'},
            {"out", 	required_argument,  0,  'o'},
            {"help", 	no_argument, 0,  'h' },
 
   	};
   	
   	int next_opt;
   	int fdin, fdout;
   	int print = 0;
   	next_opt = getopt_long(argc, argv, "o:i:h" , long_options, NULL );

   	while(next_opt != -1){

	   	switch(next_opt){
   			case 'o':
   			fdout = open(optarg, O_CREAT | O_RDWR , 0666);
   			dup2(fdout, 1);
   			break;
   			
   			case 'i':
   			fdin = open(optarg, O_RDONLY);
   			dup2(fdin, 0);
   			break;
   			
   			case 'h':
   			printf(	" -h 	--help				Display this usage information.\n"
   					" -i 	--in filename		Read input to filename.\n"
   					" -o 	--out filename		Write output to filename\n");
   			next_opt = -1;
   			print = 1;
   			break;
   			
   		}
   		
   		next_opt = getopt_long(argc, argv, "o:i:h" , long_options, NULL );
   	}
   	
   	if (!print){
   		char **new_argv = &argv[optind]; 
   		execvp(new_argv[0], new_argv);
   	}
   	
    
    
    return 0;
}
