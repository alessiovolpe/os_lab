#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/stat.h> 
#include <string.h>
#include <fcntl.h>

int waitsig(){
	sigset_t mask;
    siginfo_t send_info;
	int sigfd;

	sigemptyset(&mask);
    sigaddset(&mask, SIGHUP);
    sigprocmask(SIG_BLOCK, &mask, NULL);
	
	if (sigfd = signalfd(-1, &mask, 0) < 0){
                perror("signalfd");
    }
	
	sigwaitinfo(&mask, &send_info);
}

int main(int argc, char *argv[]){
	
	if (argc < 2){
		printf("ERR arg.\n");
		return -1;
	}
	
	int nofile = 0;
	char *path;

	
	while(1){
		asprintf(&path, "%s%d", argv[1], nofile);
		int fdarg = open(path, O_CREAT | O_RDWR, 0666);
		char str[1024];
		
		int i = 0;
		for(i; i<1024; i++){
			str[i] = '\0';
		}
		
		pid_t pid;
		pid = fork();
		
		if(!pid){
			while(1){
				read(STDIN_FILENO, str, 1024);
				write(fdarg, str, strlen(str));
			}
		}
		
		if(pid != 0){
			waitsig();
			kill(pid, SIGTERM);
		}
		
		nofile++;
	}
	
	

}
