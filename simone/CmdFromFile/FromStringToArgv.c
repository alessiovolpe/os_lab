#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

struct listcmd {
    char *cmd[1024];
    struct listcmd *next;
};

//AGGGIUNGE COMANDI CON PARAMETRI IN LISTCMD
//RETURN NUMERO DI COMANDI TROVATI NEL FILE
 
int getcmd(struct listcmd **head, FILE* fd){
    int arg = 0, posch = 0, nocmd = 0;
    char ch_a;
    
    (*head) = malloc(sizeof(struct listcmd));
    (*head)->cmd[arg] = malloc(BUFSIZ * sizeof(char*));
    struct listcmd *tmp = *head;
    
    while (1) {
        ch_a = fgetc(fd);
        
        if (ch_a == EOF) {
            return nocmd;
        }
        if (ch_a == '\n') { // nuovo comando
            tmp->next = malloc(sizeof(struct listcmd));
            tmp = tmp->next;
            tmp->cmd[0] = malloc(BUFSIZ * sizeof(char*));
            arg = 0;
            posch = 0;
            nocmd++;
        }
        else if (ch_a == ' ') {  // nuovo parametro
            arg++;
            tmp->cmd[arg] = malloc(BUFSIZ * sizeof(char*));
            posch = 0;
        }
        else{  // è un carattere
            tmp->cmd[arg][posch] = ch_a;
            posch++;
        }
    }
}


