//
//  esercizio_1
//  scrivere un programma che passato come parametro il path di una directory
//  (cwd se manca il paramtro) stampi il path relativo di tutti i file con
//   nome palindromo presenti nel sottoalbero.
//


#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/dir.h>

int isPal(const char nameFile[], int nname){
    int i=0,j=nname-1;
    for (; i<j && !strncmp(&nameFile[i], &nameFile[j], 1); i++, j--);
    if (j <= i) return 1;
    else return 0;
}

void visit(const char *path){
  //  struct stat desc ;
    struct dirent **file;
    int count = scandir(path, &file, NULL, NULL);
    for (int i = 2; i<count; i++) {
        if (file[i]->d_type == DT_DIR) {
            char name[file[i]->d_namlen];
            char newPath[1024];
            strcpy(name, file[i]->d_name);
            sprintf(newPath, "%s%s/", path, name);
            visit(newPath);
        }else{
            if (isPal(file[i]->d_name, file[i]->d_namlen)) {
                char name[file[i]->d_namlen];
                char newPath[1024];
                strcpy(name, file[i]->d_name);
                sprintf(newPath, "%s%s/", path, name);
                printf("%s\n", newPath);
            }
        }
    }
}


int main(int argc,const char *argv[]) {
   
    visit(argv[1]);
    return 1;
}
