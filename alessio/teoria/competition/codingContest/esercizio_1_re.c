//
//  esercizio_1
//  scrivere un programma che passato come parametro il path di una directory
//  (cwd se manca il paramtro) stampa il numero di tutti i file e directory
//   presenti nel sottoalbero.
//


#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/dir.h>

struct n_file {
    int file;
    int directory;
};

static struct n_file res;

void visit(const char *path){
    struct dirent **file;
    int count = scandir(path, &file, NULL, NULL);
    for (int i = 2; i<count; i++) {
        if (file[i]->d_type == DT_DIR) {
            res.directory++;
            char name[file[i]->d_namlen];
            char newPath[1024];
            strcpy(name, file[i]->d_name);
            sprintf(newPath, "%s%s/", path, name);
            visit(newPath);
        }else{
            res.file++;
        }
    }
}


int main(int argc,const char *argv[]) {
    res.file = 0;
    res.directory = 0;
    visit(argv[1]);
    printf("file: %d\n directory: %d\n", res.file, res.directory);
    return 1;
}
