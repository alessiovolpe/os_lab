//
//  main.c
//  esercizio_3
//
// Scrivere un programma che deve ottenere il pid passato come parametro.
// indicare se e' impossibile
//

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>


int found(int child , int n){
    printf("PID: %d, ", child);
    if (child == n) {
        printf("\nFOUND\n");
        sleep(100);
        return 1;
    }else{
        printf("NOT FOUND ");
        return 0;
    }
}

int main(int argc, const char * argv[]) {
    int trovato = 0;
    char *com = malloc(20);
    if (argc<1) {
        printf("no argument\n");
        return 0;
    }
    
    int n = atoi(argv[1]);
    sprintf(com, "ps -p %d", n);
    
    while (!trovato) {
        errno=0;
        kill(n, 0);
        if (errno != ESRCH) {
            printf("Processo già esistente\n");
            system(com);
            return 0;
        }
        pid_t child = fork();
        if (child != 0) {
            trovato = found(child, n);
            printf("child %d chiuso\n", wait(NULL));
        } else {
            return 1;
        }
    }
    return 0;
}
