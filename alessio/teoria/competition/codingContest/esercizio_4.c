/*
 * Educational resources to learn
 *
 * esercizio_4
 * Trovare i file di contenuto uguale nella directory corrente 
 * convertirli in link dello stesso file.
 *
 */

#include <unistd.h> //symlink(file, new link)
#include <stdio.h> //stat
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/dir.h>
#include <sys/param.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int isEqual(char* path1, char* path2, int sizefl){
    int f1;
    int f2;
    
    char *buf1 = malloc(sizefl+1);
    char *buf2 = malloc(sizefl+1);
    
    if ((f1 = open(path1, S_IRUSR)) <0){
        return 0;
    }
    if ((f2 = open(path2, S_IRUSR)) < 0) {
        return 0;
    }
    
    
    read(f1, buf1, sizefl);
    read(f2, buf2, sizefl);
    
    close(f1);
    close(f2);
    
    if (strcmp(buf1, buf2) == 0){
        free(buf1);
        free(buf2);
        return 1;
    } else {
        free(buf1);
        free(buf2);
        return 0;
    }
}


//
void equalmanager(struct dirent **listfile,int nfile){
    struct stat desc ;
    struct stat desc_next;
    char *path = malloc(50);
    char *path_next = malloc(50);
    for (int i = 3; i<nfile; i++) { //primi 2 file sono dir attuale e padre
        sprintf(path, "./%s", listfile[i]->d_name);
        stat(path, &desc);
        for (int j = i+1; j < nfile; j++) {
            sprintf(path_next, "./%s", listfile[j]->d_name);
            stat(path_next, &desc_next);
            if (desc.st_size == desc_next.st_size) {
                if (isEqual(path, path_next,(int) desc.st_size)) {
                    printf("file: %s == file: %s\n", listfile[i]->d_name, listfile[j]->d_name);
                    unlink(path_next);
                    symlink(path, path_next);
                }
            }
        }

    }
}

int main(int argc, char *argv[]){
    
    struct dirent **file;
    int count = scandir("./", &file, NULL, NULL);
    
    printf("n file: %d\n", count);
    
    for (int i = 3; i < count; i++) {
        printf("file: %s \n", file[i]->d_name);

    }
    
    equalmanager(file, count);
}
