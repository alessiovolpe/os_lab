#include <monitor.h>
#define MAX

monitor bb;
condition can_w, can_r;

generic_type buf[MAX]; 
int pos_w, pos_r; //posizione in cui scrivere o leggere
int count; // elementi nel buf
int n_w, n_r; //numero lettori e scrittori in attesa
int exit;  //exit status if 1 reader return NULL

void bb_create(void){
	bb = monitor_create();
	can_w = condition_create();
	can_r = condition_create();
	pos_w = pos_r = count = n_w = n_r = exit = 0;
}

void write(generic_type val){ //monitor entry
	monitor_entry(bb);
	if(count == MAX ){
		if(n_w == MAX){
			pos_r = (pos_r + 1) % MAX; //perdo l elemento che è da piu' tempo nel buffer
			count--;
			condition_signal(can_w); //sblocco il primo scrittore
		}
		n_w++;
		condition_wait(can_w);  // mi metto in attesa
        n--;
	}
	buf[pos_w] = val;
	count++;
	pos_w = (pos_w + 1) % MAX;
	condition_signal(can_r);
	monitor_exit(bb);
}


generic_type read(void){	
	monitor_entry(bb);
	if( count <= 0 ){ //niente da leggere
		if(n_r == MAX){ //coda di attesa piena
			exit = 1;
			condition_signal(can_r);
		}
		n_r++;
		condition_wait(can_r);
		n_r--;
		if(exit) {
			exit = 0;
			return NULL;
		}
	}
	generic_type val = buf[pos_r];
	count--;
	pos_r = (pos_r + 1) % MAX;
	condition_signal(can_w);
	monitor_exit(bb);
	return val;
}










