#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>

struct list{
	char *name;
	struct list *next;
};

void add(struct list **head, const char *name){
	if((*head) == NULL){
		(*head) = malloc(sizeof(struct list));
		asprintf(&((*head)->name), "%s", name);
		(*head)->next = NULL;
	} else {
		add(&(*head)->next, name);
	}
}

int pop(struct list **head, char **buf){
	if((*head) == NULL) return -1;
	asprintf(buf,"%s", (*head)->name);
	struct list *tmp = (*head);
	(*head) = (*head)->next;
	free(tmp);
	return 0;
}

int filter(const struct dirent *el){
	const char *endstr = el->d_name + strlen(el->d_name) - 2;
	if(!strcmp(".c",endstr) || !strcmp(".o",endstr))
		return 1;
	return 0;
}

int check(const char *f1, const char *f2){
	int len1 = strlen(f1) - 2;
	int len2 = strlen(f2) - 2;
	if((len1 != len2) || strncmp(f1, f2, len1))
		return 0;
	struct stat info1, info2;
	stat(f1, &info1);
	stat(f2, &info2);
	if(info1.st_mtime > info2.st_mtime)
		return 1;
	return 0;
}

void gen(const char *pathdir, struct list **head){
	struct dirent **files;
	int n = scandir(pathdir, &files, filter, alphasort);
	int i;
	for(i=0; i<n; i++){
		
		if(i < (n-1) && check(files[i]->d_name, files[i]->d_name)){
			add(head, files[i]->d_name);
			i++;
		} else if (i == (n-1)){
			add(head, files[i]->d_name);
		}
	}
}	

int main( int argc, char *argv[]){
	struct list *head = NULL;
	gen(".", &head);	
	char *buf;
	while(!pop(&head, &buf)){
		printf("%s\n", buf);
	}
}


