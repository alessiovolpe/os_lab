#!/bin/bash
 
# store arguments in a special array 
args=("$@") 
# get number of elements 
ELEMENTS=${#args[@]}
if [ $ELEMENTS -eq 0 ]
then
    ELEMENTS=1
    args[0]="./"
    echo $ELEMENTS
echo ${args[0]}
fi

for (( i=0;i<$ELEMENTS;i++)); do 
    find ${args[i]} -type f -print0 | xargs -0 md5sum | sort | uniq -w32 --all-repeated=prepend | awk '{ print $2 }' | awk 'BEGIN{RS=RS RS}{$1=$1}1'
done
