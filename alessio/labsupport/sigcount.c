/*
 Scrivere un programma (linguaggio C) che incrementi un contatore ogni secondo. Quando riceve un segnale SIGUSR1 il programma deve stampare il valore attuale del contatore.

 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h> 
#include <sys/shm.h>

#define MEMSIZE 64

int count(char *shm){
	*shm=0x0;
	while(1){
		sleep(1);
		*shm = *shm + 0x1;
	}
	exit(EXIT_SUCCESS);
}


int main(){
	char c;
	int shmid;
	key_t key = 5678;
	char *shm, *s;
	pid_t pid = getpid();
	printf("This is my pid: %d\n", pid);

	int rv_signal;
	siginfo_t info;
	sigset_t set;
	sigemptyset(&set);
	sigaddset(&set, SIGUSR1);
	sigprocmask(SIG_BLOCK, &set, NULL);

	pid = fork();
	if(pid == 0){
		if((shmid = shmget(key, MEMSIZE, IPC_CREAT | 0666)) < 0) {
			perror("shmget");
			exit(1);
		}
		if((shm = shmat(shmid, NULL, 0)) == (char *) -1) {
			perror("shmat");
			exit(1);
		}
		count(shm);
	}
	if((shmid = shmget(key, MEMSIZE, IPC_CREAT | 0666)) < 0) {
		perror("shmget");
		exit(1);
	}
	if((shm = shmat(shmid, NULL, 0)) == (char *) -1) {
		perror("shmat");
		exit(1);
	}
	while(1){
		rv_signal = sigwaitinfo(&set, &info);
		if(rv_signal == SIGUSR1)
			printf("elapsed: %u\n",(unsigned int) *shm);
	}
}
