#include <stdio.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>

struct list{
	char *name;
	ino_t ino;
	struct list *next;
};

void add(struct list **head, const char *name, ino_t ino){
	if((*head) == NULL){
		(*head) = malloc(sizeof(struct list));
		asprintf(&((*head)->name), "%s", name);
		(*head)->ino = ino;
		(*head)->next = NULL;
	} else if ((*head)->ino == ino){
		asprintf(&((*head)->name), "%s %s", (*head)->name, name);
		return;
	} else {
		add(&(*head)->next, name, ino);
	}
}

int pop(struct list **head, char **buf){
	if((*head) == NULL) return -1;
	asprintf(buf,"%s", (*head)->name);
	struct list *tmp = (*head);
	(*head) = (*head)->next;
	free(tmp);
	return 0;
}

int sort(const struct dirent **el_1, const struct dirent **el_2){
	struct stat file_info_1;
	struct stat file_info_2;

	char *path;

	asprintf(&path, "./%s", (*el_1)->d_name);
	stat(path, &file_info_1);

	asprintf(&path, "./%s", (*el_2)->d_name);
	stat(path, &file_info_2);

	if (file_info_2.st_ino == file_info_1.st_ino) {
		return alphasort(el_1, el_2);
	}
	return file_info_1.st_ino - file_info_2.st_ino;

}

int filter(const struct dirent *el){
	struct stat fi;
	char *path;
	asprintf(&path, "./%s", (el)->d_name);
	stat(path, &fi);
	if(fi.st_nlink > 1) return 1;
	return 0;
}

void lsino(char* path, struct list **head){

	struct dirent **list;
	struct stat file_info;

	int n = scandir(path, &list, filter, sort);
	int i;
	for (i = 0; i<n; i++) {
		char *pathname;
		asprintf(&pathname, "./%s", list[i]->d_name);
		stat(pathname, &file_info);
		add(head, list[i]->d_name, file_info.st_ino);
	}
}

void print(struct list **head, char *dir){
	char *buf;
	while(!pop(head, &buf)){
		printf("%s/%s\n",dir, buf);
	}
}
int main(int argc, char* argv[]){

	if (argc > 1) {
		chdir(argv[1]);
	}
	struct list *head = NULL;
	lsino(".", &head);
	print(&head, argv[1]);
	return 0;
}
