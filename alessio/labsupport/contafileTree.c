/*
 Scrivere un programma che conti I file ordinari e le directory presenti nel sottoalbero della directory passata come parametro (o della directory corrente se non viene passato alcun parametro).
 Ogni file o directory deve venir contato una sola volta anche se e’ presente con diversi nomi a causa dei link fisici. Ogni altro tipo di file che non sia file ordinario o directory (e.g. socket, named pipe, file speciali, link simbolici) devono essere ignorati.

 */


#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>

struct list{
	dev_t dev;
	ino_t ino;
	struct list *next;
};

int filter(const struct dirent *el){
	if( strcmp(el->d_name, ".") == 0 || strcmp(el->d_name, "..") == 0){
		return 0;
	} else {
		return 1;
	}
}

int add(struct list **head, dev_t dev,  ino_t ino){
	if((*head) == NULL){
		(*head) = malloc(sizeof(struct list));
		(*head)->dev = dev;
		(*head)->ino = ino;
		(*head)->next = NULL;
		return 1;
	} else if (ino == (*head)->ino && dev == (*head)->dev){
		return 0;
	} else {
		return add(&((*head)->next), dev, ino);
	}
}

int counter_f(const char *path, struct list **head){
	struct dirent **files;
	struct stat buf;
	char n_path[PATH_MAX];
	int count = 0;
	int n = scandir(path, &files, filter, alphasort);
	int i;
	for(i = 0; i < n; i++){
	    printf("%s \n", files[i]->d_name);
		n_path[0] = '\0';
		sprintf(n_path, "%s/%s", path, files[i]->d_name);
		stat(n_path, &buf);
		switch (buf.st_mode & S_IFMT) {
			case S_IFREG:
				count += add(head, buf.st_dev, buf.st_ino);
				break;
			case S_IFDIR:
				count += counter_f(n_path, head) 
					+ add(head, buf.st_dev, buf.st_ino);
				break;
		}
	}
	return count;
}

int main(int argc, char *argv[]){
	if(argc > 1)
		chdir(argv[1]);
	struct list *head = NULL;
	printf("%d\n", counter_f(".", &head));
}



