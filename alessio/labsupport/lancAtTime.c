#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
struct list{
	char *argv;
	useconds_t msec;
	struct list *next;
};

void add(struct list **head, const char *exec, const int sec){
	if((*head) == NULL){
		(*head) = malloc(sizeof(struct list));
		asprintf(&((*head)->argv), "%s", exec);
		(*head)->msec = (sec*1000);
	}else{
		add(&(*head)->next, exec, sec);
	}
}

int pop(struct list **head, char **buf, useconds_t* usec){
	if((*head) == NULL) return -1;
	asprintf(&(*buf),"%s", (*head)->argv);
	*usec = (*head)->msec;
	struct list *tmp = (*head);
	(*head) = (*head)->next;
	free(tmp);
	return 0;
}

int get_number(const char* s) {
	int value = 0;
	const char *scanstr = s + strlen(s) - 1;
	while (scanstr > s && isdigit(*(scanstr-1))) {
		scanstr--;
	} 
	sscanf(scanstr,"%d", &value);
	return value;
}

int sort(const struct dirent **a, const struct dirent **b){
	int na = get_number((*a)->d_name);
	int nb = get_number((*b)->d_name);
	if(na == nb) return alphasort(a,b);
	return (na - nb);
}

int isexec( const char *name){
	struct stat sb;
	char *buf;
	asprintf(&buf, "%s/%s", ".", name);
	if(stat(buf, &sb) == -1)
		exit(1);
	if(sb.st_mode & S_IXUSR)
		return 1;
	return 0;
}

int filter(const struct dirent *el){
	size_t len = strlen(el->d_name);
	if( isdigit(el->d_name[len-1]) && isexec(el->d_name)){
		return 1;
	}
	return 0;
}

void walk(const char *path, struct list **head){
	struct dirent **files;
	int len = scandir(path, &files, filter,sort);
	int i;
	for( i = 0; i < len; i++){
		double s = get_number(files[i]->d_name);
		add(head, files[i]->d_name, s);
	}
}

void lanciaex(struct list **head){
	char *buf;
	pid_t wpid, pid;
	int status;
	useconds_t usec;
	while(!pop(head, &buf, &usec)){
		if((pid = fork ()) < 0)
			exit(1);
		if(pid == 0){
			char *argv[3];
			asprintf(&argv[0], "%s%s", "./", buf);
			asprintf(&argv[1], "%s", buf);
			argv[2] = NULL;
			usleep(usec);
			execv(argv[0], (char * const*)argv);
			return;
		}
	}
}

int main( int argc, char *argv[]){
	if(argc > 1)
		chdir(argv[1]);
	struct list *head = NULL;
	walk(".", &head);
	lanciaex(&head);
}
