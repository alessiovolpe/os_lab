#!/bin/bash

ar1=$1
ar2=$2
ar3=$3

echo "$ar1 $ar2 $ar3"

union(){
	a=$(find "$ar1" -maxdepth 1 -type f)
	b=$(find "$ar2" -maxdepth 1 -type f)
	un="$a $b"
	local f;
	for f in $un; do
		basename "$f"
	done | sort -u
}

for f in $(union); do
	a="$ar1$f"
	b="$ar2$f"

	if [ -e "$a"]; then
		if [ -e "$b"]; then
			if [ "$a" -nt "$b"]; then
				mv -f  "$a $ar3" 
			else
				 mv -f  "$b $ar3" 
			fi
		else # file exist in a but not in b
			mv -f  "$a $ar3" 
		fi
	else #exist in b but not in a
		mv -f  "$b $ar3" 
	fi
done


#						if [ -e "$a"]; then
#						if [ -e "$b"]; then
#						if [ "$a" -nt "$b"]; then
#						mv "$a" "$3/"
##						else
#						mv "$b" "$3/"
#						fi
#						else # file exist in a but not in b
#						mv "$a" "$3/"
#						fi
#						else #exist in b but not in a
#						mv "$b" "$3/"
#						fi

