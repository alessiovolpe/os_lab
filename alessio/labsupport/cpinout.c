#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdint.h>
#include <time.h>
#include <signal.h>


#define SECOND 1000000
#define TIME 0.3

void showactivity(pid_t pid){
    sigset_t mask;
    sigemptyset(&mask);
    sigaddset(&mask, SIGINT);
    sigprocmask(SIG_BLOCK, &mask, NULL);
    struct timespec time;
    time.tv_nsec = 1;
    int signal = -1;

    while(1){
        dup2(2,1);
        
        write(2, "\r-", 2);
        fflush(stdout);
        usleep(TIME*SECOND);
        write(2,"\r\\",2);
        fflush(stdout);
        usleep(TIME*SECOND);
        write(2,"\r|",2);
        fflush(stdout);
        usleep(TIME*SECOND);
        write(2,"\r/",2);
        fflush(stdout);
        usleep(TIME*SECOND);
        signal = sigtimedwait(&mask, NULL, &time);
        if(signal == 2){
            kill(pid, SIGUSR2);
            sleep(1);
            kill(0, 9);
        }
    }
}

void cpinout(void){
    
    struct timespec time, start, end, middle1, middle2;
    time.tv_nsec = 1;
    char buff[10];
    sigset_t mask;
    double computation_time;
    unsigned long long writechar = 0, writechars = 0;;
    sigemptyset(&mask);
    sigaddset(&mask, SIGUSR1);
    sigaddset(&mask, SIGINT);
    sigaddset(&mask, SIGUSR2);
    int signal = -1;
    
    clock_gettime(CLOCK_MONOTONIC, &start);
    clock_gettime(CLOCK_MONOTONIC, &middle1);
    sigprocmask(SIG_BLOCK, &mask, NULL);
    int fd1 = dup(1);
    int fd2 = dup(2);
    while(1){
        read(0, &buff, 1);
        write(1, &buff, 1);
        writechar++; writechars++;
        signal = sigtimedwait(&mask, NULL, &time);
        clock_gettime(CLOCK_MONOTONIC, &middle2);
        if(signal == SIGUSR1){
            dup2(fd2,1);
            printf("\r%llu written byte.\n", writechar);
            dup2(fd1,1);
        }
        else if(signal == SIGUSR2){
            dup2(fd2,1);
            clock_gettime(CLOCK_MONOTONIC, &end);
            printf("\r\r%.5f B/s\n", ((double)writechar/(((double)end.tv_sec + 1.0e-9*end.tv_nsec) -
                                  ((double)start.tv_sec + 1.0e-9*start.tv_nsec))));
            dup2(fd1,1);
            return;
        }
        else if( (((double)middle2.tv_sec+1.0e-9*middle2.tv_nsec)-((double)middle1.tv_sec+1.0e-9*middle1.tv_nsec)) > 1.0 ){
            dup2(fd2, 1);
            printf("\r%llu written byte.\n", writechars);
            writechars = 0;
            clock_gettime(CLOCK_MONOTONIC, &middle1);
            dup2(fd1, 1);
        }
    }
    
}

int main(void){
    pid_t pid = fork();
    
    if(pid == 0){
        cpinout();
        return;
    }
    
    showactivity(pid);
    
    return 0;
}
