#!/usr/bin/env python3

import os, sys, shutil

a=sys.argv[1]
b=sys.argv[2]
c=sys.argv[3]

af=[ f for f in os.listdir(a) if os.path.isfile(os.path.join(a, f))]
bf=[ f for f in os.listdir(b) if os.path.isfile(os.path.join(b, f))]
cf= {}
for f in af:
	cf[f] = os.path.join(a, f)

for f in bf:
	try:
		if (os.path.getmtime(cf[f]) < os.path.getmtime(os.path.join(b, f))):
			cf[f] = os.path.join(b, f)
	except KeyError:
		cf[f] = os.path.join(b, f)


for f in cf:
	shutil.move(cf[f], os.path.join(c, f))

