/*
 Completare l’esercizio 1 ponendo una gestione opportuna dei parametri in linea comando che preveda di poter modificare solo la data di accesso o quella di modifica, di definire il tempo di “invecchiamento” e un help. Si possono inserire altri parametri ritenuti opportuni.

 */
#include <stdio.h>
#include <utime.h>
#include <sys/dir.h>
#include <sys/stat.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <dirent.h>


#define DEFAULTAGE 864000 //n sec of 10 days 

#define NAN 0x0
#define	AC	0x01  //only access
#define MOD	0x02  //only modification
#define REC	0x04  //recursive mode
#define VER	0x08  //verbose mode 

#define NOARGV 0x100 
 

#define MATCH_OPT(opt) (options & opt)
#define SET_DEFAULT() options = options | AC | MOD;

static unsigned int aging = DEFAULTAGE;
static unsigned int options = NAN | NOARGV;

char *help = "\n\n\nAGER\t\t\tUser Commands\t\t\tAGER\n\n \
NAME\n \
\tager - file aging \n\n \
SYNOPSIS\n \
\tager  [OPTION]... [FILE]...\n\n \
DESCRIPTION\n \
\tAger ages the atime and mtime of a FILEs (the current directory and ager time = 10 day by default)\n\n \
OPTIONS\n \
\t-a, --access\n \
\t\tager only access time of a FILE(s) (the current directory by default)\n\n \
\t-m, --modification\n \
\t\tager only modification time of a FILE(s) (the current directory by default)\n\n \
\t-R, --recursive\n \
\t\tager recursively mode\n\n \
\t--timeager\n \
\t\tset the time ager in seconds for aging (10 day by default)\n \
\t\te.g. ager -T 3600 file or ager --teager 3600 file \n\n \
\t-v , --verbose\n \
\t\texplain what is being done\n\n \
\t--help\n \
\t\tdisplay this help and exit\n\n\n\n";

void print_time(time_t time){
    char buffer[26];
    struct tm* tm_info;

    tm_info = localtime(&time);

    strftime(buffer, 26, " %Y-%m-%d %H:%M:%S ", tm_info);
    printf(buffer);

}


static inline void verbose(char *pathfile, struct utimbuf new_times){ // verbose mode stampo quello che faccio
	if(MATCH_OPT(VER)){
		printf("%s \t\t", pathfile);
		if(MATCH_OPT(AC)){
		 	printf("act:");
		 	print_time(new_times.actime);
		}
		if(MATCH_OPT(MOD)){
		 	printf("modt:");
		 	print_time(new_times.modtime);
		}
		printf("\n");
	}	
}

void ager(char *pathfile){ // invecchio il file secondo le opzioni
	struct utimbuf new_times;
	struct stat stat_file;
	if (stat(pathfile, &stat_file) < 0) { //get the stat time of file;
		perror(pathfile);
		exit(1);
	}
	new_times.actime = stat_file.st_atime;
	new_times.modtime = stat_file.st_mtime;
	if(MATCH_OPT(AC)) new_times.actime -= aging;  //get last access time - agin
	if(MATCH_OPT(MOD)) new_times.modtime -= aging; //get last data modification - agin
	if (utime(pathfile, &new_times) < 0) { //set new time at file
		perror(pathfile);
		exit(1);
	}
	verbose(pathfile, new_times);
}

void get_ager(char buf[]){ //ottengo il nuovo tempo di invecchiamento 
	unsigned int res = atoi(buf);
	if(!res){
		printf("timeager not valid\n");
		exit(1);
	}
	aging = res;
}

void get_help(){ // stampo help e termino 
	printf(help);
	exit(0);
}

get_opt(char buf[]){ //parser short options
	int j = 1;
	char c = buf[j];
	while(c != '\0' && c != '\n' && c != '\t'){
		if (c == 'a') options = options | AC;
		else if (c == 'm') options = options | MOD;
		else if (c == 'v') options = options | VER;
		else if (c == 'R') options = options | REC;
		else{
			printf("invalid options\n");
			exit(1);
		}
		j++;
		c = buf[j];
	}
}

void get_options(int argc, char *argv[], int *i){ //parser options
	char buf[256];
	while(*i < argc && !strncmp(argv[*i], "-", 1)){ //valuto se si tratta di un opzione cerco la sua rispondente
		buf[0] = '\0';
		strcpy(buf, argv[*i]);
		if(!strcmp(buf, "--access")) options = options | AC;
		else if(!strcmp(buf,"--modification")) options = options | MOD;
		else if(!strcmp(buf, "--recursive")) options = options | REC;
		else if(!strcmp(buf, "--verbose")) options = options | VER;
		else if(!strcmp(buf, "--timeager")){
			(*i)++;
			if(*i >= argc){
				printf("timeager not valid\n");
				exit(1);
			}
			buf[0] = '\0';
			strcpy(buf, argv[*i]);
			get_ager(buf);
		} 
		else if(!strcmp(buf, "--help")) get_help();
		else get_opt(buf);	
		(*i)++;	 
	}
	if(!MATCH_OPT(AC) && !MATCH_OPT(MOD)) SET_DEFAULT(); //non sono state trovate opzioni
}

int is_dir(const char *path){
	struct stat s;
	if (stat(path, &s) == -1){
		perror("stat");
		return 0;
	}
	return S_ISDIR(s.st_mode);
}

void walk(const char *pathdir){
	struct dirent **list;
	int count =  scandir(pathdir, &list, NULL, NULL);
	int i;
	for(i = 2; i < count; i++){
		char newpath[1024];
		char name[128];
		strcpy(name, list[i]->d_name);
		
		if(list[i]->d_type == DT_DIR && MATCH_OPT(REC)){
			sprintf(newpath, "%s%s/", pathdir, name);
			walk(newpath);
		}
		sprintf(newpath, "%s%s", pathdir, name);
		ager(newpath);
	}
}

void ager_run(int argc, char *argv[], int *i){
		if(MATCH_OPT(NOARGV)) walk("./");
		else{
			while((*i) < argc){
				if(is_dir(argv[*i])) walk(argv[*i]);
				else ager(argv[*i]);
				(*i)++;
			}
		}
}


int main(int argc, char *argv[]) {
	int i = 1;
	get_options(argc, argv, &i);
	if(i < argc) options ^= NOARGV;
	ager_run(argc, argv, &i);
	return 0;
}
