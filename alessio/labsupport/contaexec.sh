#! /bin/bash

all=($(find $1 -maxdepth 1 -type f -executable 2>> /dev/null))
declare -A hasht

for f in "${all[@]}"; do
	line=$(head -n 1 $f)
	if [[ "$line" != "#!"* ]]; then 
		hasht['exec']=$((hasht['exec']+1)) 
	else
		line=${line#*/}
		line=${line% *}
		line=/$line
		hasht[$line]=$((hasht[$line]+1))
	fi
done

for i in "${!hasht[@]}"
do
	if [ "$i" != "exec"  ]; then 
  		echo $i: ${hasht[$i]}
	fi
done
echo exec: ${hasht["exec"]}
