
/*
 a) 18 punti
 Scrivete un programma così organizzato. Un processo progenitore genera un albero binario di processi (i.e., il progenitore ha due figli, ognuno di questi due figli ha due figli, e così via. La profondità dell'albero è N. Scegliete un N piccolo, tipo 3 o 4.
 b) 8 punti
 I processi foglia (quelli che non hanno figli) generano un numero casuale fra 1 e 10 e lo comunicano al loro processo padre. Il processi intermedi ricevono i dati dai loro processi figli, li sommano assieme e spediscono il risultato al loro processo padre. Il processo progenitore non fa nulla.
 Tutti i processi devono stampare esaurientemente la loro attività: chi sono, la loro posizione nell'albero (tipo: liv. 2 – destra), cosa ricevono e cosa inviano.
 c) 4 punti
 Una volta ottenuto il risultato, il processo genitore lo comunica a tutti i processi sottostanti.
 Risolvete l'esercizio tramite PIPE.
 */


#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>

#define N 4


int main(){
	int d = 0;
	int pos = 0;
	pid_t p1;
	pid_t p2;
	
	while(d < N){
		printf("pid: %d deep: %d,  %s\n", getpid(), d,(d)? ((pos)? "sx":"dx"):"" );
		pid_t p1 = fork();
		if(p1 != 0){
			pid_t p2 = fork();
			if(p2 != 0){
				break;
			} else {
				pos = 1;
			}
		}else {
			pos = 0;
		}
		d++;
	}
	if(d == N){
		printf("pid: %d deep: %d Sono una foglia! %s\n", getpid(), d, (pos)? "sx":"dx");
	}
	sleep(32 - (d *10));
	printf("pid: %d deep: %d chiudo\n", getpid(), d);
}

