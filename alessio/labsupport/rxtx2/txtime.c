#include <sys/signalfd.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>   /* for clock_gettime */

#define BILLION 1000000000L

int main(int argc, char *argv[]){

	sigset_t mask;
	int sfd;
	struct signalfd_siginfo fdsd;
	ssize_t s;

	sigemptyset(&mask);
	sigaddset(&mask, SIGUSR1);

	sigprocmask(SIG_BLOCK, &mask, NULL);
	sfd = signalfd(-1, &mask, 0);

	pid_t pid = atoi(argv[1]);
	long nchar = atoi(argv[2]);
	fdsd.ssi_pid = pid;
	char *str = "";
	long i = 0;
	for(; i < nchar; i++) asprintf(&str, "%sK", str);
	long len = nchar;

	uint64_t sum=0, med = 0 ;
	struct timespec start, end;
		
	for (i = 0;i < len; i++) {
		int j = 7;
		clock_gettime(CLOCK_MONOTONIC, &start);
		for(;j >= 0; j--){
			int bit = ((str[i])>>j) & 1;
			if(bit) kill(fdsd.ssi_pid, SIGUSR1);
			else kill(fdsd.ssi_pid, SIGUSR2);
			s = read(sfd, &fdsd, sizeof(struct signalfd_siginfo));
		}
		clock_gettime(CLOCK_MONOTONIC, &end);
		sum += BILLION * (end.tv_sec - start.tv_sec) + end.tv_nsec - start.tv_nsec;
	}
	kill(fdsd.ssi_pid, SIGQUIT);
	med = sum/nchar;
	s = read(sfd, &fdsd, sizeof(struct signalfd_siginfo));
	if (fdsd.ssi_signo == SIGUSR1) printf("tempo medio %llu nanosec\n",(long long unsigned int) med );
}
