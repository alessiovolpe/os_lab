#!/bin/bash

if [ $# -ne 2 ]; then 
	echo "no argv"
	exit	
fi

dir=$1
group=$2

files=($( find $dir -maxdepth 1 -not -type d -group $group | rev | cut -d / -f 1 | rev))

for f in ${files[@]}; do
	echo $f
done
