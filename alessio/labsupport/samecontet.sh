#!/bin/bash

ar=($(find .  -maxdepth 1 -type f))
uni=($(md5sum ./* 2> /dev/null | sort  | uniq -d -w 32 | awk '{print $1}'))
for i in ${uni[@]}; do
	for j in ${ar[@]}; do
		sum=$(md5sum $j | awk '{print $1}')
		if [ "$i" =  "$sum" ]; then
			echo -n  "$j "
	    fi
	done
	echo " "
done
