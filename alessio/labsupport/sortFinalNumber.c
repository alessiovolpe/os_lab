#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>

int sort(const struct dirent **a, const struct dirent **b){
	size_t la = strlen((*a)->d_name);
	size_t lb = strlen((*b)->d_name);
	char *bufa = "", *bufb = "";
	int i;
	for(i = (la-1); isdigit((*a)->d_name[i]); i--){
		asprintf(&bufa, "%c%s", (*a)->d_name[i], bufa);
	}
	for(i = (lb-1); isdigit((*b)->d_name[i]); i--){
		asprintf(&bufb, "%c%s", (*b)->d_name[i], bufb);
	}
	int na = atoi(bufa);
	int nb = atoi(bufb);
	if(na == nb) return alphasort(a,b);
	return (na - nb);
}

int filter(const struct dirent *el){
	size_t len = strlen(el->d_name);
	if( isdigit(el->d_name[len-1]) ){
		return 1;
	}
	return 0;
}

void walk(const char *path){
	struct dirent **files;
	int len = scandir(path, &files, filter,sort);
	int i;
	for( i = 0; i < len; i++){
		printf("%s\n", files[i]->d_name);
	}
}

int main( int argc, char *argv[]){
	if(argc > 1)
		chdir(argv[1]);
	walk(".");
}
