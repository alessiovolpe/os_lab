#!/usr/bin/env python3

import os
from sys import argv
from hashlib import md5

dictionary = {}
md5key=md5()

if len(argv) == 1:
	argv.append("./")

for	i in range(len(argv)):
	for dirpath,_,filenames in os.walk(argv[i]):
		for f in filenames:
			path = os.path.abspath(os.path.join(dirpath, f))
			with open(path, "rb") as thefile:
				buf = thefile.read()
				md5key = md5()
				md5key.update(buf)
			key = md5key.hexdigest()
			if key not in dictionary:
				dictionary[key] = []
			dictionary[key].append(path)

for k in dictionary:
	if len(dictionary[k]) > 1:
		print(k, " " , dictionary[k])




#for dirpath,val,filenames in os.walk("./"):
#	print("\n\n")
#	print("dirpath:", dirpath)
#	print("val:", val)
#	print( "filenames:", filenames)
#	print("\n\n")
#	for f in filenames:
#		print(os.path.abspath(os.path.join(dirpath, f)))

#from hashlib import md5

#md = md5()
#with open("/Users/alessio/Desktop/OScript/dir2/fileD", "rb") as thefile:
#	buf = thefile.read()
#	md.update(buf)

#print(md.hexdigest())



