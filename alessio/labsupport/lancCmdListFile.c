/*
 Scrivere un programma C di nome filepipe che abbia come unico parametro il pathnae di un file di testo.
 Questo file contiene due comandi con I rispettivi parametri, uno per riga.
 Il programma deve mettere in esecuzione concorrente I due comandi in modo che l'output del primo venga fornito come input del secondo usando una pipe.
 Il programma deve terminare quando entrambi I comandi sono terminati.
 Esempio: se il file ffff contiene:
 ls -l
 tac
 il comando:
 filepipe ffff
 deve restituire lo stesso output del comando:
 ls -l | tac
 */


#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define MAXCMDLEN 128
#define MAXCMD 128
#define MAXARGV 128


typedef struct{
	char *argv[3];
}cmd_t;

int run_cmd(int in, int out, cmd_t *cmd){
	pid_t pid;
	if((pid = fork ()) < 0)
		exit(1);
	if (pid == 0){// figlio
		if (in != 0){ //se il descriptor in non e' = 0, cioe'non e'lo stdin per questo processo ce lo faccio diventare
			dup2(in, 0);
			close (in);
		}
		if (out != 1){//se il descriptor out non e' = 1, cioe'non e'lo stdout per questo processo ce lo faccio diventare
			dup2 (out, 1);
			close (out);
		}
		return execvp(cmd->argv[0], (char * const *)cmd->argv);//exec(v)(p) v=accetta vettore di argomenti, il primo argv e'il programma da invocare con (p) lo cerca in /bin o /usr/bin
	}
	return pid;
}

int fork_pipes(int n, cmd_t *cmd){
	int in, fd[2];
	in = 0; //il primo prcesso ottine l'input dallo stdin "originale"
	pid_t pidcld[MAXCMD];
	int i;
	for (i = 0; i < n-1; ++i){
		pipe(fd); //creo una nuova pipe

		//il processo riceve input da in (il primo processo in=0) e fa output su fd[1] della pipe
		pidcld[i] = run_cmd(in, fd[1], cmd + i); //
		close (fd [1]);
		//in diventa l'in della pipe che sara' input del prossimo processo
		in = fd[0];
	}
	waitpid(run_cmd(in, 1, cmd + i), NULL, 0 );
	for(i=0; i < n - 1; i++)
		kill(pidcld[i],SIGKILL);	// Attende termine di un figlio (uno qualunque)
	
}

void parser_line(FILE *fd, cmd_t *cmd, char first_c){
	char c = first_c;
	int i = 0;
	int nargv = 0;
	char buf[MAXCMDLEN];
	while(1){
		while(1){
			if(c == ' ' || c == '\t' || c == '\n' || c == EOF)
				break;
			buf[i] = c;
			i++;
			c = fgetc(fd);
		}
		if (i > 0){
			buf[i] = '\0';
			cmd->argv[nargv] = malloc(i+1);
			strcpy(cmd->argv[nargv], buf);
			nargv++;
		}
		if (c == '\n' || c == EOF){
			cmd->argv[nargv] = NULL;
			return;
		}
		i=0;
		while (c == ' ' || c == '\t') c = fgetc(fd);
	}
}

int parser(FILE *fd, cmd_t *cmd){
	int line = 1;
	char c;
	do{
		c = fgetc(fd);
		while (c == ' ' || c == '\t' || c == '\n')
			c = fgetc(fd);
		if (c == EOF) break;
		parser_line(fd, &cmd[line-1], c);
		line++;
	}while (1);

	return line-1;
}

int main(int argc, char *argv[]){
	if (argc < 1){
		printf("not argument");
		exit(1);
	}
	cmd_t cmd[MAXCMD];
	FILE *fd = fopen(argv[1], "r");
	int n = parser(fd, cmd);

	return fork_pipes (n, cmd);
}
