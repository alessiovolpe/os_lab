/*
 Scrivere un programma ager in grado di invecchiare file.
 Il programma deve poter prendere una lista di file come parametri o nessun parametro, nel qual caso invecchierà tutti I file della directory corrente. “invecchiare” significa predatare il tempo di ultimo accesso e di modifica di 10 giorni.
 Esempio:
 $ ls -l file
 -rw-r--r-- 1 renzo renzo
 $ ./ager file
 $ ls -l file
 -rw-r--r-- 1 renzo renzo
 */

#include <stdio.h>
#include <utime.h>
#include <sys/dir.h>
#include <sys/stat.h>

#define AGER 864000 //n sec of 10 days

int ager(char *pathfile){
	struct utimbuf new_times;
	struct stat stat_file;
	
	if (stat(pathfile, &stat_file) < 0) { //get the stat time of file;
		perror(pathfile);
		return 1;
	}
	
	new_times.actime = stat_file.st_atime - AGER;  //get last access time - AGER
	new_times.modtime = stat_file.st_mtime - AGER; //get last data modification - AGER
	
	if (utime(pathfile, &new_times) < 0) { //set new time at file
		perror(pathfile);
		return 1;
	}
	return 0;
}
	
int ager_all(){
	struct dirent **file;
	int count = scandir("./", &file, NULL, NULL);
	int i;
	for(i = 2; i < count; i++){
		if( ager(file[i]->d_name) ) return 1;
	}
	return 0;
}

int main(int argc, char *argv[]) {
	int i;
	for(i = 1; i < argc; i++){
		if( ager(argv[i]) ) return 1;
	}
	
	if (argc  < 2 && ager_all()){
		return 1;
	}
	return 0;
}
