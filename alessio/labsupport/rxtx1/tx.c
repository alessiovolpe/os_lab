#include <sys/signalfd.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]){

	sigset_t mask;
	int sfd;
	struct signalfd_siginfo fdsd;
	ssize_t s;

	sigemptyset(&mask);
	sigaddset(&mask, SIGUSR1);

	sigprocmask(SIG_BLOCK, &mask, NULL);
	sfd = signalfd(-1, &mask, 0);

	pid_t pid = atoi(argv[1]);
	fdsd.ssi_pid = pid;
	char *str;
	asprintf(&str, "%s", argv[2]);
	int len = strlen(str);
	int i=0;
	for (;i < len; i++) {
		int j = 7;
		for(;j >= 0; j--){
			int bit = ((str[i])>>j) & 1;
			if(bit) kill(fdsd.ssi_pid, SIGUSR1);
			else kill(fdsd.ssi_pid, SIGUSR2);
			s = read(sfd, &fdsd, sizeof(struct signalfd_siginfo));

		}
	}
	kill(fdsd.ssi_pid, SIGQUIT);
	s = read(sfd, &fdsd, sizeof(struct signalfd_siginfo));
	if (fdsd.ssi_signo == SIGUSR1) printf("done\n");
}
