#!/bin/bash

read -p "Insert string: " str
str1=$(echo $str|rev)
echo $str1

([[ "$str" == "$str1" ]] && echo "$str is palindrome") || echo "$str is not palindrome"
