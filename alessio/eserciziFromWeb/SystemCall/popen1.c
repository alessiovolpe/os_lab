#include  <sys/wait.h>
#include  "ourhdr.h"

int main(void)
{
  char  line[MAXLINE];
  FILE  *fpin;

  if ( (fpin = popen("tr A-Z a-z", "r")) == NULL)
    perror("popen error");

  for ( ; ; ) {
    fputs("prompt> ", stdout);
    fflush(stdout);
    if (fgets(line, MAXLINE, fpin) == NULL) /* read from pipe */
      break;
    if (fputs(line, stdout) == EOF)
      perror("fputs error to pipe");
  }
  if (pclose(fpin) == -1)
    perror("pclose error");
  putchar('\n');
  exit(0);
}
