#include  <sys/types.h>
#include  <sys/wait.h>
#include  <unistd.h>

<I>void
pr_exit(int status)
{
  if (WIFEXITED(status))
    printf("normal termination, exit status = %d\n",
        WEXITSTATUS(status));
  else if (WIFSIGNALED(status))
    printf("abnormal termination, signal number = %d%s\n",
        WTERMSIG(status),
#ifdef  WCOREDUMP
        WCOREDUMP(status) ? " (core dumped)" : "");
#else
        "");
#endif
  else if (WIFSTOPPED(status))
    printf("child stopped, signal number = %d\n",
        WSTOPSIG(status));
}</I>
