#include  <sys/types.h>
#include  <sys/wait.h>
#include  <errno.h>
#include  <unistd.h>

int
system(const char *cmdstring) /* version without signal handling */
{
  pid_t pid;
  int   status;

  
  /*
   * Se si passa il valore NULL a system, lo standard POSIX.2 
   * prevede che system debba ritornare 1 se system e' in
   * grado di accettare comandi, 0 in caso contrario; ovvero
   * system deve essere definita, ma non e' detto che debba
   * funzionare
   */
  if (cmdstring == NULL)
    return(1);    /* always a command processor with Unix */

  /*
   * Lanciamo un nuovo processo per fare la exec
   */
  if ( (pid = fork()) < 0) {
    status = -1;  /* probably out of processes */

  } else if (pid == 0) {        /* child */
    
    /* 
     *  Qui si poteva evitare l'uso della shell, ma sarebbe
     *  stato piu' complesso. Avremmo dovuto spezzare la
     *  stringa in un insieme di argomenti, e passarli come
     *  array di argomenti; avremmo dovuto usare execlp;
     *  inoltre, ci saremmo persi la gestione dei 
     *  metacaratteri. Utilizziamo l'opzione -c della
     *  shell che fa tutto questo.
     */
    execl("/bin/sh", "sh", "-c", cmdstring, (char *) 0);
    
    /* 
     *  Exec puo' fallire in tanti modi; per esempio la
     *  shell puo' non essere presente; ritorniamo un
     *  errore standard. Usiamo _exit per evitare che il
     *  figlio faccia flushing dello standard I/O
     */
    _exit(127);   /* execl error */

  } else {              /* parent */
    while (waitpid(pid, &status, 0) < 0)
      /* 
       *  Wait puo' fallire se viene interrotto da un
       *  segnale; poiche' la gestione dei segnali non
       *  e' trattata a sufficienza, sorvoliamo per il
       *  momento.
       */
      if (errno != EINTR) {
        status = -1; /* error other than EINTR from waitpid() */
        break;
      }
  }

  return(status);
}
