#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>

/* Numero di giocatori nel sistema */
#define NPROCESSI 5


/* Scrive un messaggio di errore e poi termina */
void errquit(char* message)
{
  perror(message);
  exit(1);
}

int main(int argc, char* argv[])
{
  
  /* Process id ottenuto da fork */
  int pid;
  
  /* Indice del processo */
  int index = 1;
  
  /* Pipe utilizzato dal parent per leggere il risultato finale */
  int parentpipe[2];
  
  /* File descriptor per costruire pipe */
  int fd[2];

  /* File descriptor da utilizzare per leggere/scrivere */
  int fdin, fdout;
  
  /* Valore da leggere */
  int valore;
  
  /* Counter */
  int i;

  pipe(parentpipe);
  
  /* Faccio il fork di NGIOCATORI figli */
  index = 1;
  for (i=2; i <= NPROCESSI; i ++) {
    pipe(fd);
    pid = fork();
    if (pid < 0) {
      errquit("FORK error");
    } else if (pid == 0) {
      /* Child (index = i)*/
      index = i;
      fdin = fd[0];
    } else {
      /* Parent (index = i-1) */
      fdout = fd[1];
      break;
    }
  }
  
  /* Scelgo cosa fare, a seconda che sia padre e/o figlio */
  if (index == 1) {
    /* Primo processo */
    fdin = parentpipe[0];
    write fdout 1
    read x
    
  } else if (index == NPROCESSI) {
    /* Ultimo processo */
    fdout = parentpipe[1];
    read fdin x
    x = x + index;
    write fdout x
  } else {
    /* Processi intermedi */
    read fdin x
    x = x + index;
    write fdout x
    
  }

}


