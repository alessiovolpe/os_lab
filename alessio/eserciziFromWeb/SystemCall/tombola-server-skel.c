/**
 
**/


#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

/* Numero di giocatori nel sistema */
#define NGIOCATORI 3

/* Numero di valori da estrarre */
#define MAXNUM 10

/* Scrive un messaggio di errore e poi termina */
void errquit(char* message)
{
  perror(message);
  exit(1);
}

/* Genera un numero casuale fra 0 e max-1 */
int casuale(int max)
{
  long int val = random();
  return val % max;
}


void doparent(int* giocatori)
{
  /* Cartella di numeri estratti, mantenuta come array di booleani */
  int estratti[MAXNUM];
 
  /* Numero di valori ancora da estrarre */
  int daestrarre;
  
  /* True se qualcuno ha fatto tombola */
  int tombola;
  
  /* File descriptor per il file condiviso*/
  int fdout, fdin; 
  
  /* Contatori, variabili di appoggio */
  int i, valore;
  
  /* Character read */
  char c;
  
  /* Inizializza il generatore di numeri casuali */
  srandom(getpid());
  
  /* Inizializza struttura dati */
  for (i=0; i < MAXNUM; i++)
    estratti[i] = 0;
  daestrarre = MAXNUM;
  
  /* Apre il file per scrivere le giocate / leggere conferme */
  fdout = open("from-p", O_WRONLY);
  fdin = open("to-p", O_RDONLY);
 
  /* Main loop */
  tombola = 0;
  while (!tombola && daestrarre > 0) {
    
    /* Decrementa il numero di estrazioni mancanti*/
    daestrarre--;
    
    /* Estrai un numero che non e' gia' stato estratto */
    printf("Parent: prima dell'estrazione\n", valore);
    do {
      valore = casuale(MAXNUM);
    } while (estratti[valore]);
    estratti[valore]=1;
    
    /* Invia ai child il numero estratto */
    for (i=0; i < NGIOCATORI; i++) {
      if (write(fdout, &valore, sizeof(int))<0)
        errquit("PARENT: WRITE error");
    }
    
    /* Aspetta le risposte */
    for (i=0; i < NGIOCATORI; i++) {
      printf("Mancano %d risposte\n", NGIOCATORI-i);
      if (read(fdin, &c, 1)<0)
        errquit("PARENT: READ error");
      if (c == 't') {
        printf("Parent: Tombola!\n");
        tombola=1;
      } 
    }
    
    printf("Parent: Ricevute tutte le risposte\n");  
  }
  
  
  /* Scrive -1 per concludere la partita */
  valore = -1;
  for (i=0; i < NGIOCATORI; i++) {
    if (write(fdout, &valore, sizeof(int))<0)
      errquit("PARENT: WRITE error");
  }
  
  /* attende la terminazione dei figli */ 
  i = NGIOCATORI;
  while (i > 0) {
    if (wait(&valore) > 0)
    i--;
    printf("Parent: Ancora %d giocatori nella sala\n", i);
  }
  
}


int main(int argc, char* argv[])
{
  
  /* Process id ottenuto da fork */
  int pid;
  
  /* Process id dei giocatori */
  int giocatori[NGIOCATORI];

  /* Counter */
  int i;

  /* Crea i canali FIFO per la comunicazione con i child */
  if (mkfifo("from-p", S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH) < 0)
    errquit("MKFIFO from-p");
  if (mkfifo("to-p", S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH) < 0)
    errquit("MKFIFO to-p");

  /* Faccio il fork di NGIOCATORI figli */
  for (i=0; i < NGIOCATORI; i++) {
    pid = fork();
    if (pid < 0) 
      errquit("Problema con fork");
    else if (pid > 0) {
      /* Siamo nel Parent */
      giocatori[i] = pid;
    } else {
      /* Siamo nel child */
      if (execl("./tombola-client", "tombolaclient", (char *) 0))
        errquit("EXEC errror");
    }
  }
  
  doparent(giocatori);

  /* Li rimuovo */
  if (unlink("from-p") < 0)
    errquit("UNLINK from-p");
  if (unlink("top") < 0)
    errquit("UNLINK to-p");
  
  
}


