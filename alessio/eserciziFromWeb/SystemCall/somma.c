#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>

/* Numero di giocatori nel sistema */
#define NPROCESSI 5


/* Scrive un messaggio di errore e poi termina */
void errquit(char* message)
{
  perror(message);
  exit(1);
}

int main(int argc, char* argv[])
{
  
  /* Process id ottenuto da fork */
  int pid;
  
  /* Indice del processo */
  int index = 1;
  
  /* Pipe utilizzato dal parent per leggere il risultato finale */
  int parentpipe[2];
  
  /* File descriptor per costruire pipe */
  int fd[2];

  /* File descriptor da utilizzare per leggere/scrivere */
  int fdin, fdout;
  
  /* Valore da leggere */
  int valore;
  
  /* Counter */
  int i;

  /* Creo il pipe affinche' il parent legga il risultato finale */
  if (pipe(parentpipe) < 0) {
    errquit("PIPE error");
  }
  
  /* Faccio il fork di NGIOCATORI figli */
  index = 1;
  for (i=2; i <= NPROCESSI; i ++) {
    if (pipe(fd) < 0) {
      errquit("PIPE error");
    }
    pid = fork();
    if (pid < 0) {
      errquit("FORK error");
    } else if (pid == 0) {
      /* Child (index = i)*/
      fdin = fd[0];
      index = i;
    } else {
      /* Parent (index = i-1) */
      fdout = fd[1];
      break;
    }
  }
  
  /* Scelgo cosa fare, a seconda che sia padre e/o figlio */
  if (index == 1) {
    /* Primo processo */
    fdin = parentpipe[0];
    valore = 1;
    printf("process[%d]: mandato %d\n", index, valore);
    if (write(fdout, &valore, sizeof(int))<0) { 
      errquit("WRITE error");
    }
    if (read(fdin, &valore, sizeof(int))<0) {
      errquit("READ error");
    }
    printf("process[%d]: ricevuto %d\n", index, valore);
  
  } else if (index == NPROCESSI) {
    /* Ultimo processo */
    fdout = parentpipe[1];
    if (read(fdin, &valore, sizeof(int))<0) {
      errquit("READ error");
    }
    printf("process[%d]: ricevuto %d\n", index, valore);
    valore = valore + index;
    printf("process[%d]: mandato %d\n", index, valore);
    if (write(fdout, &valore, sizeof(int))<0) { 
      errquit("WRITE error");
    }
    
  } else {
    /* Processi intermedi */
    if (read(fdin, &valore, sizeof(int))<0) {
      errquit("READ error");
    }
    printf("process[%d]: ricevuto %d\n", index, valore);
    valore = valore + index;
    printf("process[%d]: mandato %d\n", index, valore);
    if (write(fdout, &valore, sizeof(int))<0) { 
      errquit("WRITE error");
    }
    
  }

}


