/**

**/


#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

/* Numero di valori in una cartella */
#define DIMCARTELLA 4

/* Numero di valori da estrarre */
#define MAXNUM 10

/* Scrive un messaggio di errore e poi termina */
void errquit(char* message)
{
  perror(message);
  exit(1);
}

/* Genera un numero casuale fra 0 e max-1 */
int casuale(int max)
{
  long int val = random();
  return val % max;
}



void dochild()
{
  /* Cartella di numeri, mantenuta come array di booleani */
  int cartella[MAXNUM];
  
  /* Numeri mancanti alla tombola */
  int mancanti; 
  
  /* File descriptor per comunicazione con il parent */
  int fdin, fdout; 
  
  /* Contatori, variabili di appoggio */
  int i, valore;
  
  /* Buffer */
  char buf[256];
  
  /* Genera cartella casuale */
  srandom(getpid());
  for (i=0; i < MAXNUM; i++)
  cartella[i] = 0;
  i=0;
  while (i < DIMCARTELLA) {
    valore = casuale(MAXNUM);
    if (!cartella[valore]) {
      cartella[valore] = 1;
      i++;
    }
  }
  mancanti = DIMCARTELLA;
  
  sprintf(buf, "to-%d", getpid()); 
  mkfifo(buf, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
  fdin = open(buf, O_RDONLY);
  fdout = open("to-p", O_WRONLY);
  
  /* Main loop */
  while (mancanti > 0) {
    
    /* Aspetto una nuova giocata */
    if (read(fdin, &valore, sizeof(int))<0)
      errquit("CHILD: READ error");
    
    if (valore < 0) {
      /* Se il valore e' negativo, qualcuno ha fatto tombola */
      printf("Child[%d]: fine partita\n",getpid());
      exit(0);
    }
    
    printf("Child[%d]: e' stato estratto %d\n",getpid(), valore);
    if (cartella[valore]) {
      printf("Child[%d]: ok!, mancano %d\n",getpid(), mancanti);
      cartella[valore] == 0;
      mancanti--;
    }
    
    /* Mando un carattere al parent per dirgli che ho ricevuto
       la giocata; eventualmente per dirgli che ho fatto tombola
     */
    if (write(fdout, (mancanti > 0 ? "r" : "t"), 1) != 1)
      errquit("WRITE error");
  }
  
}

int main(int argc, char* argv[])
{
  
  dochild();
  
}
