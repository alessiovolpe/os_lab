#include  <signal.h>
#include  "ourhdr.h"

static void sig_int(int);

int
main(void)
{
  sigset_t  newmask, oldmask, pendmask;

  if (signal(SIGINT, sig_int) == SIG_ERR)
    perror("can't catch SIGINT");

  sigemptyset(&newmask);
  sigaddset(&newmask, SIGINT);
          /* block SIGINT and save current signal mask */
  if (sigprocmask(SIG_BLOCK, &newmask, &oldmask) < 0)
    perror("SIG_BLOCK error");

  sleep(5);   /* SIGINT here will remain pending */

  if (sigpending(&pendmask) < 0)
    perror("sigpending error");
  if (sigismember(&pendmask, SIGINT))
    printf("\nSIGINT pending\n");

          /* reset signal mask which unblocks SIGINT */
  if (sigprocmask(SIG_SETMASK, &oldmask, NULL) < 0)
    perror("SIG_SETMASK error");
  printf("SIGINT unblocked\n");

  sleep(5);   /* SIGINT here will terminate with core file */

  exit(0);
}

static void
sig_int(int signo)
{
  printf("caught SIGINT\n");

  if (signal(SIGINT, SIG_DFL) == SIG_ERR)
    perror("can't reset SIGINT");
  return;
}
