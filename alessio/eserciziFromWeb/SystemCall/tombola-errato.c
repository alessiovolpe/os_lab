/**
 
Questa versione contiene un errore: segnali multipli possono
essere messi in coda e catturati come singolo segnale.

**/


#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>

/* Numero di giocatori nel sistema */
#define NGIOCATORI 3

/* Numero di valori da estrarre */
#define MAXNUM 10

/* Numero di valori in una cartella */
#define DIMCARTELLA 4

/* File name utilizzato per comunicare */
#define FILENAME "giocata"



/* Scrive un messaggio di errore e poi termina */
void errquit(char* message)
{
  perror(message);
  exit(1);
}

/* Genera un numero casuale fra 0 e max-1 */
int casuale(int max)
{
  long int val = random();
  return val % max;
}


/* True se questo processo e' un child */
static int child = 0;

/* True se il parent ha spedito una nuova giocata */
static int nuovagiocata;

/* Numero di risposte dai child */
static int rispostemancanti;

/* Tombola! */
static int tombola;

void sig_usr1(int sig)
{
  if (child) {
    write(1, "CHILD_USR1\n", 12);
    nuovagiocata = 1;
  } else {
    write(1, "PAREN_USR1\n", 12);
    rispostemancanti--;
  }
  
}

void sig_usr2(int sig)
{
  write(1, "PAREN_USR2\n", 12);
  tombola = 1;
}


void doparent(int* giocatori)
{
  /* Cartella di numeri estratti, mantenuta come array di booleani */
  int estratti[MAXNUM];
  
  /* Numero di valori ancora da estrarre */
  int daestrarre;
  
  /* File descriptor per il file condiviso*/
  int fd; 
  
  /* Maschere */
  sigset_t     newmask, oldmask, zeromask;
  
  /* Contatori, variabili di appoggio */
  int i, valore;
  
  srandom(getpid());
  
  
  /* Inizializza struttura dati */
  for (i=0; i < MAXNUM; i++)
    estratti[i] = 0;
  daestrarre = MAXNUM;
  
  /* Apre il file con le giocate in lettura */
  fd = open(FILENAME, O_WRONLY);
  
  /* Inizializza signal handling */ 
  if (signal(SIGUSR1, sig_usr1) == SIG_ERR)
    errquit("CHILD: SIGNAL error");
  if (signal(SIGUSR2, sig_usr2) == SIG_ERR)
    errquit("CHILD: SIGNAL error");
  sigemptyset(&zeromask);
  sigemptyset(&newmask);
  sigaddset(&newmask, SIGUSR1);
  sigaddset(&newmask, SIGUSR2);
  if (sigprocmask(SIG_BLOCK, &newmask, &oldmask) < 0)
    errquit("CHILD: SIG_BLOCK error");
  
  /* Main loop */
  tombola = 0;
  while (!tombola && daestrarre > 0) {
    
    daestrarre--;
    
    /* Estrai un numero che non e' gia' stato estratto */
    printf("Parent: prima dell'estrazione\n", valore);
    do {
      valore = casuale(MAXNUM);
    } while (estratti[valore]);
    estratti[valore]=1;
    
    /* Scrive il valore estratto */
    if (lseek(fd, SEEK_SET, 0) < 0)
      errquit("PARENT: SEEK error");
    if (write(fd, &valore, sizeof(int))<0)
      errquit("PARENT: WRITE error");
    
    /* Sveglia i giocatori */
    printf("Parent: e' stato estratto %d\n", valore);
    rispostemancanti = NGIOCATORI;
    for (i=0; i < NGIOCATORI; i++) {
      printf("Parent: sveglio %d\n", giocatori[i]);
      if (kill(giocatori[i], SIGUSR1) < 0)
        errquit("PARENT: KILL SIGUSR1");
    }
    
    /* Aspetta le risposte */
    while (rispostemancanti > 0) {
      sigsuspend(&zeromask);  /* aspetta una nuova giocata */
      printf("Parent: Mancano %d risposte\n", rispostemancanti);
    }
    
    printf("Parent: Ricevute tutte le risposte\n");  
  }
  
  valore = -1;
  
  if (tombola) {
    printf("Parent: Tombola!\n");
  } else {
    printf("Parent: Nessuno ha vinto\n");
  }
  
  /* Scrive -1 per concludere la partita */
  if (lseek(fd, SEEK_SET, 0) < 0)
    errquit("PARENT: SEEK error");
  if (write(fd, &valore, sizeof(int))<0)
    errquit("PARENT: WRITE error");
  
  printf("Parent: faccio terminare la partita\n");
  rispostemancanti = NGIOCATORI;
  for (i=0; i < NGIOCATORI; i++) {
    if (kill(giocatori[i], SIGUSR1) < 0)
      errquit("PARENT: KILL SIGUSR1");
  }
  
  while (rispostemancanti > 0) {
    if (wait(&valore) > 0)
      rispostemancanti--;
    printf("Parent: Ancora %d giocatori nella sala\n", rispostemancanti);
  }
  
}

void dochild()
{
  /* Cartella di numeri, mantenuta come array di booleani */
  int cartella[MAXNUM];
  
  /* Numeri mancanti alla tombola */
  int mancanti; 
  
  /* Maschere per gestione segnali */
  sigset_t     newmask, oldmask, zeromask;
  
  /* File descriptor per il file condiviso*/
  int fd; 
  
  /* Contatori, variabili di appoggio */
  int i, valore;
  
  /* Genera cartella casuale */
  srandom(getpid());
  for (i=0; i < MAXNUM; i++)
  cartella[i] = 0;
  i=0;
  while (i < DIMCARTELLA) {
    valore = casuale(MAXNUM);
    if (!cartella[valore]) {
      cartella[valore] = 1;
      i++;
    }
  }
  mancanti = DIMCARTELLA;
  
  /* Apre il file con le giocate in lettura */
  fd = open(FILENAME, O_RDONLY);
  
  /* Inizializza signal handling */ 
  if (signal(SIGUSR1, sig_usr1) == SIG_ERR)
    errquit("CHILD: SIGNAL error");
  sigemptyset(&zeromask);
  sigemptyset(&newmask);
  sigaddset(&newmask, SIGUSR1);
  if (sigprocmask(SIG_BLOCK, &newmask, &oldmask) < 0)
    errquit("CHILD: SIG_BLOCK error");
  
  /* Main loop */
  while (mancanti > 0) {
    
    printf("Child[%d]: Aspetto una nuova giocata\n",getpid());
    nuovagiocata = 0;
    while (nuovagiocata == 0)
      sigsuspend(&zeromask);  /* aspetta una nuova giocata */
    
    if (lseek(fd, SEEK_SET, 0) < 0)
      errquit("CHILD: SEEK error");
    if (read(fd, &valore, sizeof(int))<0)
      errquit("CHILD: READ error");
    
    
    if (valore < 0) {
      /* Se il valore e' negativo, qualcuno ha fatto tombola */
      printf("Child[%d]: fine partita\n",getpid());
      exit(0);
    }
    
    printf("Child[%d]: e' stato estratto %d\n",getpid(), valore);
    if (cartella[valore]) {
      printf("Child[%d]: ok!, mancano %d\n",getpid(), mancanti);
      cartella[valore] == 0;
      mancanti--;
      if (mancanti == 0) {
        printf("Child[%d]: tombola\n",getpid());
        /* Ho fatto tombola; spedisco un segnale SIGUSR2 al parent */
        if (kill(getppid(), SIGUSR2) < 0)
          errquit("CHILD: KILL SIGUSR2");
      }
    }
    
    /* Spedisco un segnale SIGUSR1 al parent per confermare la lettura */
    printf("Child[%d]: Confermo giocata\n",getpid());
    if (kill(getppid(), SIGUSR1) < 0)
      errquit("CHILD: KILL SIGUSR1");
  }
  
}

int main(int argc, char* argv[])
{
  
  /* Process id ottenuto da fork */
  int pid;
  
  /* Process id dei giocatori */
  int giocatori[NGIOCATORI];

  /* Counter */
  int i;

  for (i=0; i < NGIOCATORI; i++) {
    pid = fork();
    if (pid < 0) 
      errquit("Problema con fork");
    else if (pid > 0) {
      /* Siamo nel Parent */
      giocatori[i] = pid;
    } else {
      /* Siamo nel child */
      child = 1;
      break;
    }
  }
  
  if (child) {
    /* Siamo nel figlio */
    dochild();
  } else {
    /* Siamo ancora nel parent */
    doparent(giocatori);
  }
  
  
}


