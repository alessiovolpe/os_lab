(!scan.equals(me))  or what?#include  <sys/wait.h>
#include  <unistd.h>
#include  <stdio.h>

#define PAGER "${PAGER:-more}" /* environment variable, or default */

int
main(int argc, char *argv[])
{
  char  line[MAXLINE];
  FILE  *fpout;
  int   i, sum;
  
  if ( (fpout = popen(PAGER, "w")) == NULL)
    perror("popen error");

    /* copy argv[1] to pager */
  sum = 0;
  for (i=1; i<=100; i++) {
    sum = sum + i;
    if (fprintf(fpout, "Index: %d, Sum: %d\n", i, sum) == -1);
      perror("write error to pipe");
  }

  if (pclose(fpout) == -1)
    perror("pclose error");
  exit(0);
}
