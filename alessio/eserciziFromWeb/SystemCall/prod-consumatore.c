#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>

/* use our name for signals */
#define SIGPRODUCE SIGUSR1
#define SIGCONSUME SIGUSR2

/* this file should not exist */
#define TMPFILE "tmpbuffer"

void printpidnum(int);
void initsync();
void wait_produce();
void wait_consume();
void tell_produce(pid_t);
void tell_consume(pid_t);

int main(void)
{
  int sfd;                        /* the shared file descriptor */
  pid_t pid;
  
  srandom(time(NULL));
  
  sfd = initfile(TMPFILE);
  
  initsync();
  
  if ((pid = fork()) < 0)
  perror("fork error");
  else if (pid == 0) {
    /* we are the child */
    int cnum;
    int ppid;
    ppid = getppid();
    while (1) {
      wait_produce();
      /* rewind the file and read */
      if ( lseek(sfd, 0, SEEK_SET) < 0)
      perror("Child seek error");
      if ( read(sfd, &cnum, sizeof(int)) < 0)
      perror("Read error");
      /* consume */
      sleep(1);
      printpidnum(cnum);
      tell_consume(ppid);
    }
  } else {
    int pnum;
    /* we are the parent */
    while (1) {
      /* produce a number */
      sleep(2);
      pnum = random() % 100;
      /* this function won't block us the first time */
      /* rewind the file and write */
      if ( write(sfd, &pnum, sizeof(int)) < 0)
      perror("Write error");
      printpidnum(pnum);
      tell_produce(pid);
      if( lseek(sfd, 0, SEEK_SET) < 0)
      perror("Parent seek error");
      wait_consume();
    }
  }
  exit(0);
}

void sig_usr() { }

void printpidnum(int c)
{
  printf("%d: %d\n", getpid(), c);
}

int initfile(char *name)
{
  int fd;
  /* let's open and create the shared file */
  if (fd = open(name, O_RDWR | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR) < 0)
  perror("Unable to create shared file");
  
  /* remove it, so only us and our child have access to it */
  /* unlink(name);*/
  
  return fd;
}

void initsync()
{
  sigset_t protectmask;
  
  /* initialize signal mask */
  sigemptyset(&protectmask);
  sigaddset(&protectmask,SIGUSR1);
  sigaddset(&protectmask,SIGUSR2);
  
  if ( sigprocmask(SIG_BLOCK,&protectmask,NULL) < 0 )
  
  /* set the signal handler */
  if ( signal(SIGPRODUCE,sig_usr) < 0)
  perror("sigal error");
  if ( signal(SIGCONSUME,sig_usr) < 0)
  perror("sigal error");
  /*      kill(getpid(),SIGCONSUME); */
  
}


void tell_produce(pid_t pid) {
  
  kill(pid,SIGPRODUCE);
  
}

void tell_consume(pid_t pid) {
  
  kill(pid,SIGCONSUME);
  
}

void wait_produce() {
  sigset_t zeromask;
  
  /* initialize signal mask */
  sigemptyset(&zeromask);
  
  sigsuspend(&zeromask);
}

void wait_consume() {
  sigset_t zeromask;
  
  /* initialize signal mask */
  sigemptyset(&zeromask);
  
  sigsuspend(&zeromask);
}