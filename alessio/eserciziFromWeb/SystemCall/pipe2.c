#include  <sys/wait.h>
#include  <unistd.h>

#define DEF_PAGER "/usr/bin/more"   /* default pager program */
#define MAXLINE 1024

int
main(int argc, char *argv[])
{
  int buf[MAXLINE];
  int   i, sum;
  int   n, fd[2];
  pid_t pid;
  char  *pager, *argv0;

  if (pipe(fd) < 0)
    perror("pipe error");

  if ( (pid = fork()) < 0)
    perror("fork error");
  else if (pid > 0) {               /* parent */
    close(fd[0]);   /* close read end */
    
    sum = 0;
    for (i=1; i<=100; i++) {
      sum = sum + i;
      sprintf(buffer, "Index: %d, Sum: %d\n", i, sum);
      n = strlen(line);
      if (write(fd[1], line, n) != n)
        perror("write error to pipe");
    }
    if (waitpid(pid, NULL, 0) < 0)
      perror("waitpid error");
    exit(0);

  } else {                    /* child */
    close(fd[1]); /* close write end */
    if (fd[0] != STDIN_FILENO) {
      if (dup2(fd[0], STDIN_FILENO) != STDIN_FILENO)
        perror("dup2 error to stdin");
      close(fd[0]); /* don't need this after dup2 */
    }

      /* get arguments for execl() */
    if ( (pager = getenv("PAGER")) == NULL)
      pager = DEF_PAGER;
    if ( (argv0 = strrchr(pager, '/')) != NULL)
      argv0++;    /* step past rightmost slash */
    else
      argv0 = pager;  /* no slash in pager */

    if (execl(pager, argv0, (char *) 0) < 0)
      perror("execl error for");
  }
}
