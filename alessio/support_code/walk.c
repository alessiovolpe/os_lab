#include <stdio.h>
#include <string.h>
#include <sys/stat.h>
#include <dirent.h>

int is_dir(const char *path){
	struct stat s;
	if (stat(path, &s) == -1){
		printf("\n\n%s\n\n" , path);
		perror("stat");
		return 0;
	}
	return S_ISDIR(s.st_mode);
}

void walk(const char *pathdir){
	if(!is_dir(pathdir)){
	   	printf(pathdir);
		return;
	}
	struct dirent **list;
	int count =  scandir(pathdir, &list, NULL, NULL);
	int i;
	for(i = 2; i < count; i++){
		char newpath[1024];
		char name[128];
		strcpy(name, list[i]->d_name);
		if(list[i]->d_type == DT_DIR){
			sprintf(newpath, "%s%s/", pathdir, name);
			walk(newpath);
			printf("%s\n" , newpath);
		}
		else{
			sprintf(newpath, "%s%s", pathdir, name);
			printf("%s\n" , newpath);
		}
	}
}

int main(int argc, char *argv[]){
	int i = 1;
	while(i < argc){
		walk(argv[i]);
		i++;
	}
}
