#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void run(const char *cmd, const char *arg){
	char *argv[3];
	asprintf(&argv[0],"%s", cmd);
	asprintf(&argv[1], "%s", arg);
	argv[2] = NULL;
	pid_t pid = fork();
	if(pid == 0){
		execvp(argv[0], (char * const *) argv);
		perror("error: ");
		exit(EXIT_FAILURE);
	}
	waitpid(pid, NULL, 0);
}

void scriptexec(FILE *fp){
	char *line = NULL, *cmd = NULL, *arg=NULL, *delim=" \n\t";
	size_t len = 0;
	while(getline(&line, &len, fp) != -1){
		line[strcspn(line, "\r\n")] = '\0';
		cmd = strtok_r(line, delim, &arg);
		if(cmd[0] != '#')
			run(cmd, arg);
	}
}
int main(int argc, char *argv[]){
	if(argc < 2) exit(EXIT_FAILURE);
	FILE *fp;
	if((fp = fopen(argv[1], "r" )) == NULL) exit(EXIT_FAILURE);
	scriptexec(fp);
	fclose(fp);
	return 0;
}
