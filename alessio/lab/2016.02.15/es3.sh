#!/bin/bash

if [ $# -eq 0 ]
then
	dir=. 
else
	dir=$1
fi
ar=($(find $dir -maxdepth 1 -type f -executable))

for ex in ${ar[@]}; do
	msec=$(echo $ex | grep -Eo '[0-9]+$')
	if [ $? -eq 0  ]; then
		sec=$(expr $msec / 1000)
		(sleep $sec && $ex > /dev/null) & 
	fi
done
