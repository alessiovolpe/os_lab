#include <sys/signalfd.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[]){
	sigset_t mask;
	int sfd;
	struct signalfd_siginfo fdrc;
	ssize_t s;

	sigemptyset(&mask);
	sigaddset(&mask, SIGUSR1);
	sigaddset(&mask, SIGUSR2);
	sigaddset(&mask, SIGQUIT);

	sigprocmask(SIG_BLOCK, &mask, NULL);
	sfd = signalfd(-1, &mask, 0);
	
	printf("my pid: %d\n", getpid());

	char b;
	char *str = "";
	int i = 0;
	for (;;i++) {
		if(i == 8){
			asprintf(&str,"%s%c", str, b);
			i = 0;
			b=0x0;
		}
		s = read(sfd, &fdrc, sizeof(struct signalfd_siginfo));
		if (fdrc.ssi_signo == SIGUSR1) {
			b = b<<1 | 1;
		} else if (fdrc.ssi_signo == SIGUSR2){
			b = b<<1 | 0;
		}  else if (fdrc.ssi_signo == SIGQUIT) {
			break;
		}
		kill(fdrc.ssi_pid, SIGUSR1);
	}
	kill(fdrc.ssi_pid, SIGUSR1);
}
