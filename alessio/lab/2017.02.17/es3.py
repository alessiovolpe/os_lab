#!/usr/bin/env python3
import os,  collections

dictionary={}

for dirpath,_,files in os.walk("./"):
	for f in files:
		path = os.path.abspath(os.path.join(dirpath, f))
		_ , ext = os.path.splitext(path)
		if ext not in dictionary:
			dictionary[ext] = []
		dictionary[ext].append(f)

dictionary = collections.OrderedDict(sorted(dictionary.items()))

for k in dictionary:
	if k != "":
		print(k, end=": ")
		for i in dictionary.get(k):
			print(i, end= " ")
		print("")

for i in dictionary.get(""):
	print(i, end= " ")
print("")

