#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>
#include <linux/limits.h>
#include <sys/types.h>
#include <sys/stat.h>
struct d{ 
	dev_t dev;
	ino_t ino;
	int link;
};

struct list{
	struct d el;
	struct list *next;
};

void sort(struct list **head){
	struct list *p = (*head);
	struct list *pn;
	struct d tmp;
	while(p->next != NULL){
		pn = p->next;
		while(pn != NULL){
			if(p->el.link > pn->el.link){
				tmp = p->el;
				p->el = pn->el;
				pn->el = tmp;
			}
			pn=pn->next;
		}
		p=p->next;
	}
}

void printlist(struct list **head,int lev, int i){
	if((*head) == NULL){
		printf("N file con %d ln: %d\n", lev, i);
		return;
	}else if((*head)->el.link != lev){
		printf("N file con %d ln: %d\n", lev, i);
		printlist(&((*head)->next),((*head)->el.link),1);
	}else{
		printlist(&((*head)->next),lev,i+1);
	}
	
}


int filter(const struct dirent *el){
	if( strcmp(el->d_name, ".") == 0 || strcmp(el->d_name, "..") == 0){
		return 0;
	} else {
		return 1;
	}
}

int add(struct list **head, dev_t dev,  ino_t ino){
	if((*head) == NULL){
		(*head) = malloc(sizeof(struct list));
		(*head)->el.dev = dev;
		(*head)->el.ino = ino;
		(*head)->el.link = 0;
		(*head)->next = NULL;
		return 1;
	} else if (ino == (*head)->el.ino && dev == (*head)->el.dev){
		(*head)->el.link++;
		return 0;
	} else {
		return add(&((*head)->next), dev, ino);
	}
}

int counter_f(const char *path, struct list **head){
	struct dirent **files;
	struct stat buf;
	char n_path[PATH_MAX];
	int count = 0;
	int n = scandir(path, &files, filter, alphasort);
	int i;
	for(i = 0; i < n; i++){
		n_path[0] = '\0';
		sprintf(n_path, "%s/%s", path, files[i]->d_name);
		stat(n_path, &buf);
		switch (buf.st_mode & S_IFMT) {
			case S_IFREG:
				count += add(head, buf.st_dev, buf.st_ino);
				break;
			case S_IFDIR:
				count += counter_f(n_path, head) 
					+ add(head, buf.st_dev, buf.st_ino);
				break;
		}
	}
	return count;
}

int main(int argc, char *argv[]){
	if(argc > 1)
		chdir(argv[1]);
	struct list *head = NULL;
	printf("n_file %d\n\n", counter_f(".", &head));
	printf("\n");
	sort(&head);
	printlist(&head, 0, 0);
}



