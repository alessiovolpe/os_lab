#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>

#define MAXNAME 255+2

struct list{
	char *name;
	struct list *next;
};

void add(struct list **head, const char *exn){
	if((*head) == NULL){
		(*head) = malloc(sizeof(struct list));
		(*head)->name = malloc(sizeof(MAXNAME));
		strcpy((*head)->name, exn);
		(*head)->next = NULL;
	}else{
		add(&(*head)->next, exn); 
	}
}

int pop(struct list **head, char *buf){
	if((*head) == NULL) return -1;
	strcpy(buf, (*head)->name);
	struct list *tmp = (*head);
	(*head) = (*head)->next;
	free(tmp);
	return 0;
}

int isexec( const char *name){
	struct stat sb;
	char buf[MAXNAME];
	sprintf(buf, "%s/%s", ".", name);
	if(stat(buf, &sb) == -1)
		exit(1);
	if(sb.st_mode & S_IXUSR)
		return 1;
	return 0;
}

int ispal(const char name[]){
	int n = 0;
	for(; name[n] != '\0'; n++)  ;
	int i=0,j=n-1;
	for (; i<j && !strncmp(&name[i], &name[j], 1); i++, j--);
	if (j <= i) return 1;
	else return 0;
}

int filter(const struct dirent *el){
	if( strcmp(el->d_name, ".") == 0
			|| strcmp(el->d_name, "..") == 0
			|| strcmp(el->d_name, "lanciaxp") == 0 ){
		return 0;
	}
	return 1;
}

void get_ex(const char* path, struct list **head){
	struct dirent **files;
	int n = scandir(path, &files, filter, alphasort);
	char buf[MAXNAME];
	int i;
	for( i = 0; i < n; i++){
		if(ispal(files[i]->d_name) && isexec(files[i]->d_name)){
			sprintf(buf, "./%s", files[i]->d_name);
			add(head, buf);
		}
	}
}

void lanciaex(struct list **head){
	char buf[MAXNAME];
	pid_t wpid, pid;
	int status;
	while(!pop(head, buf)){
		char *argv[3];
		asprintf(&argv[0], "%s%s", "./", buf);
		asprintf(&argv[1], "%s", buf);
		argv[2] = NULL;
		if((pid = fork ()) < 0)
			exit(1);
		if(pid == 0){
			execv(argv[0], (char * const*)argv);
			return;
			printf("ciao\n");
		}
	}
	while ((wpid = wait(&status)) > 0);
}


int main(int argc, char *argv[]){
	struct list *head = NULL;
	get_ex(".", &head);
	lanciaex(&head);
}
