#!/bin/bash

a=($(ps -e | awk '{ if (NR != 1) {print $1} }'))

for el in ${a[@]}; do

	path=$(ls -l /proc/$el/exe 2> /dev/null)
	if [ $? -eq 0 ]; then
		echo -n  "$el"$'\t'
		echo $path | awk '{ print $11 }'
	fi
done
