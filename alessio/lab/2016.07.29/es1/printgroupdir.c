#include "printgroupdir.h"


void printgroupdir(const char *dirp, const char *group){
	struct dirent **list = NULL;
	char *filepath;
	int i, n = scandir(dirp, &list, NULL, alphasort);
	struct stat buff_stat;
	struct group *grp;
	for(i = 0; i < n; i++){
		asprintf(&filepath, "%s/%s", dirp, list[i]->d_name);
		stat(filepath, &buff_stat);
		if(((buff_stat.st_mode & S_IFMT) != S_IFDIR) ){
			grp = getgrgid(buff_stat.st_gid);
			if(strcmp(grp->gr_name, group) == 0){
				printf("%s\n", filepath);
			}
		}
	}
}

