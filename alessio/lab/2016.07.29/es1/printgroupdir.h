#ifndef PRINTGROUPDIR_H
#define PRINTGROUPDIR_H 

#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <grp.h>
#include <uuid/uuid.h>
#include <string.h>

void printgroupdir(const char *dirp, const char *group);


#endif
