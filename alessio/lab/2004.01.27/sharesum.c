#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>

#define N 2

int main(){
	char buf[4];
	int d = 0;
	int pos = 0;
	pid_t p1;
	pid_t p2;
	int dx[2], sx[2];
	int indx, insx, outpar;	
	while(d < N){
		printf("pid: %d deep: %d,  %s\n", getpid(), d,(d)? ((pos)? "sx":"dx"):"" );
		pipe(dx);
		pipe(sx);
		pid_t p1 = fork();
		if(p1 != 0){
			indx = dup(dx[0]);
			insx = dup(sx[0]);
			close(dx[0]);
			close(dx[1]);
			close(sx[0]);
			close(sx[1]);
			pid_t p2 = fork();
			if(p2 != 0){
				break;
			} else { //child dx
				outpar = dup(dx[1]);
				close(dx[0]);
				close(dx[1]);
				close(sx[0]);
				close(sx[1]);
				pos = 1;
			}
		}else {
			outpar = dup(sx[1]);
			close(dx[0]);
			close(dx[1]);
			close(sx[0]);
			close(sx[1]);
			pos = 0;
		}
		d++;
	}
	if(d == N){
		sleep(3);
		printf("pid: %d deep: %d foglia:  %s, mando: %d\n", getpid(), d, (pos)? "sx":"dx", 1);
		int num = 1;
		*buf = num;
		write(outpar, buf, 4);
	} else {
		int nsx, ndx, sum;
		read(insx, buf, 4);
		nsx = (int) *buf;
		printf("pid: %d deep: %d, nodo:  %s, recv sx: %d\n", 
				getpid(), d,(d)? ((pos)? "sx":"dx"):"root", nsx );
		read(indx, buf, 4);
		ndx = (int) *buf;
		printf("pid: %d deep: %d, nodo:  %s, recv dx: %d\n", 
				getpid(), d,(d)? ((pos)? "sx":"dx"):"root", ndx );
		sum = nsx + ndx;
		if(d){
			*buf = sum;
			printf("pid: %d deep: %d, nodo:  %s, send par: %d\n", 
					getpid(), d, (pos)? "sx":"dx", sum );
			write(outpar,buf, 4);
		}else{
			printf("pid: %d deep: %d, root, tot: %d\n", getpid(), d, sum );
		}
		waitpid(p1, NULL, 0);
		waitpid(p2,NULL, 0);
	}
	printf("pid: %d deep: %d chiudo\n", getpid(), d);
}

