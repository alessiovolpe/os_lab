#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>

#define N 4


int main(){
	int d = 0;
	int pos = 0;
	pid_t p1;
	pid_t p2;
	
	while(d < N){
		printf("pid: %d deep: %d,  %s\n", getpid(), d,(d)? ((pos)? "sx":"dx"):"" );
		pid_t p1 = fork();
		if(p1 != 0){
			pid_t p2 = fork();
			if(p2 != 0){
				break;
			} else {
				pos = 1;
			}
		}else {
			pos = 0;
		}
		d++;
	}
	if(d == N){
		printf("pid: %d deep: %d Sono una foglia! %s\n", getpid(), d, (pos)? "sx":"dx");
	}
	sleep(32 - (d *10));
	printf("pid: %d deep: %d chiudo\n", getpid(), d);
}

